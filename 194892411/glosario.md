Control de versiones (VC): Se denomia a la gesti�n de los cambios realizados sobre un archivo o conjunto de estos.
Fuente:https://es.wikipedia.org/wiki/Control_de_versiones

Control de versiones distribuido (DVC): Permite a muchos desarrolladores de sofguar trabajar en un proyecto com�n sin necesidad de compartir una misma red.
Fuente:https://es.wikipedia.org/wiki/Control_de_versiones_distribuido

Repositorio remoto y Repositorio local:Repositorio remoto son versiones de un mismo proyecto alojados en internet.Repositorio local son las versiones un mismo proyecto alojados en nuestro computador. 
fuente:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

Copia de Trabajo: Es una copia de un repositorio, hecha con el comando clone.
Fuente:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

�rea de Preparaci�n/Staging Area:es un archivo contenido en el directorio de Git, que almacena la informaci�n de lo va a ir en tu la pr�xima confirmaci�n.
Fuente:https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

Preparar Cambios/Stage Changes:Presenta los cambios para los pr�ximos compromisos, Git sabe sobre el cambio, pero no es permanente en el repositorio.
Fuente:https://githowto.com/staging_changes

Confirmar cambios/Commit Changes:Cuando el �rea de preparaci�n est� lista, se pueden confirmar los cambios. No se incluir� ning�n archivo que no se haya ejecutado desde la �ltima edici�n. La forma m�s f�cil de confirmar es escribiendo "git commit"

Fuente:https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

Fork:Si en alg�n momento quisieras contribuir al proyecto de otra persona, o si deseas utilizar el proyecto de alguien como el punto de partida para tu propio proyecto. Esto se conoce como �fork�.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Clone:Una vez que decidiste hacer un fork , hasta ese momento s�lo existe en GitHub. Para poder trabajar en el proyecto, tendr�s que clonar el repositorio elegido a tu m�quina local.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Branch:Un branch o rama es una bifurcaci�n del proyecto que se est� realizando para a�adir una nueva funcionalidad o corregir un bug. Al menos deber�a haber una rama estable que fuera la que estuviera descargada en producci�n, esto es el producto en una versi�n X. Una buena pr�ctica ser�a, por ejemplo hacer una rama Y nueva de la rama X que estuviera en producci�n, hacer la mejora, corregir que no haya errores y, cuando estuviera todo hecho, reintegrar la rama a la rama X. Al reintegrarla a la rama X (o master) tendr�a esta nueva funcionalidad.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Master:La rama master es aquella rama donde se almacena la �ltima versi�n estable del proyecto que se est� realizando. En teor�a la rama master es la que est� en producci�n en cada momento o casi a cada momento y deber�a estar libre de bugs. As�, si esta rama est� en producci�n, sirve como referente para hacer nuevas funcionalidades y/o arreglar bugs de �ltima hora.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Commit: Acci�n en Git que confirma los cambios, almacenandolos en el repositorio.
https://git-scm.com/docs/git-commit

Push:Consiste en enviar todo lo que has confirmado con un commit al repositorio remoto. Aqu� s� que es donde se une tu trabajo con el de los dem�s.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Checkout:El checkout es la acci�n de descargarse una rama del repositorio GIT local (s�, GIT tiene su propio repositorio en local para poder ir haciendo commits) o remoto.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Fetch: La acci�n fetch actualiza el repositorio local baj�ndose los datos del repositorio remoto a tu repositorio local sin actualizarlo, es decir, se guarda una copia del repositorio remoto en tu local.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Merge: La acci�n de merge es la continuaci�n natural del fetch. El merge permite unir la copia del repositorio remoto con tu repositorio local, mezclando los diferentes c�digos.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

Pull:Un pull consiste en la uni�n del fetch y del merge, esto es, recoge la informaci�n del repositorio remoto y luego mezcla tu trabajo en local con esta.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

status:Se�ala los caminos que de los directorios que tienen diferencias entre el archivo �ndice y el commit HEAD actual
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/

log:Muestra las configuraciones que se han han realizado en los directorios. Muestra los registros de confirmacion.
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/ 

Checkout: Acci�n en Git que permite cambiar de rama o restaurar una de una versi�n anterior.
Fuente:https://git-scm.com/docs/git-checkout

Rama / Branch: Directorios o sub-directorios del �rea de trabajo (Punteros hacia los commits).
Fuente:https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell

Etiqueta / Tag: Es la identificaci�n dada usualmente a las versiones.
Fuente:https://git-scm.com/docs/git-tag
