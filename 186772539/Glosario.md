﻿##Glosario de Términos

 
a. **Control de versiones** (VC): El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante. 

[https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones ] 

b. **Control de versiones distribuido** (DVC): El control de versiones distribuido cambia el foco cliente-servidor a uno P2P, lo que quiere decir que se ya no se accede solo a un repositorio central, si no que en cada peer habrá un repositorio completo, sincronizados entre si. De esta forma los cambios realizados a un mismo archivo se actualizaran en todos los repositorios según corresponda.

[ https://es.wikipedia.org/wiki/Control_de_versiones_distribuido ]

c. **Repositorio remoto y Repositorio local**: 


d. **Copia de trabajo / Working Copy**: 

e. **Área de Preparación / Staging Area**: El staging area es el lugar previo por donde se prepara nuestra working copy para que en el siguiente commit los cambios se realicen en  versión "original" de nuestro trabajo.
[ https://www.git-tower.com/help/mac/working-copy/stage-changes ]

f. **Preparar Cambios / Stage Changes**:  Se proceden a revisar los cambios, para determinar que cambios estan en "stage"  y cuales no. Esto puede ser realizado de manera bastante prolija mirando los "checkbox" de cada archivo, el cual nos muestra su status actual.

 [https://www.git-tower.com/help/mac/working-copy/stage-changes ]

g. **Confirmar cambios / Commit Changes**: 

h. **Commit**:

i. **clone**: Es un comando de git utilizado para clonar o copiar un  repositorio previamente seleccionado.

[https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone ]

j.** pull**: Es un comando de git que tiene por funcion, incluir los cambios de un repositorio remoto en la branch actual.
 
[https://git-scm.com/docs/git-pull ] 

k. **push**: Es un comando de git que actualiza las tags junto con objetos asociados.
 
[https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push ]

l. **fetch**:  Es un comando de git que se utiliza para descargar objetos y tags desde otro repositorio.

[ https://git-scm.com/docs/git-fetch ]

m. **merge**: 

n. **status**: Es un comando de git que se utiliza para mostrar el estado del workiing copy y de la staging area.

[ https://www.atlassian.com/git/tutorials/inspecting-a-repository ]

o.** log**:  Es un comando de git que sirve para mostrar los "logs"(bitacora) de los commit realizados.

[ https://git-scm.com/docs/git-log ]

p. **checkout**:  


q. **Rama / Branch**:  Es un puntero o referencia hacia un commit

[ https://stackoverflow.com/questions/25068543/what-exactly-do-we-mean-by-branch ]

r. **Etiqueta / Tag**: 
