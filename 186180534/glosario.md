### Glosario

1. **Control de versiones**(VC)
   :  Es la gesti�n de los registros de cambios realizados en un archivo o m�s archivos, com�nmente utiliza un repositorio centralizado. [Fuente 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) [Fuente 2](https://es.slideshare.net/thegeekinside/control-de-versiones-distribuido) [Fuente 3](https://betterexplained.com/articles/a-visual-guide-to-version-control)

2. **Control de versiones distribuido** (DVC): 
   :  Es una extensi�n del control de versiones normal, que permite a otros usuarios replicar el repositorio central, lo que al mismo tiempo funciona como un respaldo extra. [Fuente 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones) [Fuente 2](https://betterexplained.com/articles/a-visual-guide-to-version-control) [Fuente 3](https://betterexplained.com/articles/intro-to-distributed-version-control-illustrated)


3. **Repositorio remoto y Repositorio local**:
   :  Carpeta perteneciente al sistema del VC, en donde se guarda los datos referentes a los archivos a los que se quiere registrar sus cambios. [Fuente 1](https://betterexplained.com/articles/a-visual-guide-to-version-control) [Fuente 2](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

   *  **Repositorio local**:
       Se llama local por pertenecer al mismo equipo en donde se encuentran los archivos a los que se le realiza un seguimiento de VC. [Fuente](https://betterexplained.com/articles/intro-to-distributed-version-control-illustrated) 

   *  **Repositorio remoto**: 
	  Se llama remoto por estar ubicado en un equipo diferente a donde se encuentran los archivos a los que se realiza un seguimiento VC. [Fuente](https://en.wikipedia.org/wiki/Repository_(version_control))

4. **Copia de trabajo / Working copy**: 
   : Es el directorio local (del usuario) en donde se encuentran los archivos a los que les aplica cambios. [Fuente](https://betterexplained.com/articles/a-visual-guide-to-version-control)

5. **�rea de preparaci�n / Staging Area**:
   : Es un archivo que representa el area intermedia entre el directorio que contiene los archivos a los que se aplica cambios, y el repositorio, guarda la informaci�n sobre lo que se ha cambiado antes de ser confirmado y enviado al repositorio. [Fuente 1](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics) [Fuente 2](https://githowto.com/staging_changes)

6. **Preparar cambios / Stage Change**:
   : Son las modificaciones hechas en los archivos pertenecientes al Working copy, que estan guardadas en el Staging Area, esperando un commit para ser enviado al repositorio. [Fuente 1](https://githowto.com/staging_changes) [Fuente 2](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

7. **Confirmar cambios / Commit Changes**:
   : Confirmaci�n de los cambios mantenidos en el Staging Area, lo que provoca un traslado de los nuevos cambios al repositorio. [Fuente](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

8. **commit**:
   : Es el comando que permnite confirmar y trasladar los cambios del Staging Area hacia el repositorio. Tambi�n puede ser utilizado, con los argumentos adecuados, para trasladar archivos directamente, ignorando el Staging Area. [Fuente 1](https://git-scm.com/docs/git-commit) [Fuente 2](https://www.atlassian.com/git/tutorials/saving-changes)

9. **clone**:
   : Comando para crear una copia de un repositorio en un directorio local. [Fuente](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone)
	
10. **pull**:
    : Comando que permite trasladar todos los cambios existentes en un repositorio remoto al Working copy. Utiliza merge para actualizar los archivos del Working copy. [Fuente](https://git-scm.com/docs/git-pull)

11. **push**:
    : Comando que permite actualizar un repositorio remoto (traslado de cambios desde un repositorio local). [Fuente](https://git-scm.com/docs/git-push)

12. **fetch**:
    : Comando que permite trasladar los cambios existentes en un repositorio remoto al repositorio local. [Fuente](https://git-scm.com/docs/git-fetch)

13. **merge**:
    : Comando para fusionar dos ramificaciones, es decir, actualiza los archivos de la rama actual con los cambios existentes en la ramificaci�n que se espec�fica. [Fuente](https://git-scm.com/docs/git-merge)

14. **status**:
    : Comando para mostrar los archivos que esten en el Staging Area esperando un commit, los que aun no son agregados a dicha area, y los archivos que no estan en el repositorio. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

15. **log**:
	: Comando que muestran los registros que explican los cambios realizados a un archivo, los cuales son creados al realizar un commit. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

16. **checkout**:
    : Comando que permite trasladar el Working copy a otra ramificaci�n, o bien, a alg�n estado anterior registrado en los commit. [Fuente](https://www.atlassian.com/git/tutorials/resetting-checking-out-and-reverting)

17. **Rama/Branch**:
    : Una rama b�sicamente consiste en una carpeta dentro del Working copy, que se utiliza para separar los trabajos que han de realizarse sobre un archivo. [Fuente](https://betterexplained.com/articles/a-visual-guide-to-version-control/)

18. **Etiqueta/Tag**:
    : La etiqueta es un nombre asignado a un cambio registrado en un archivo, utilizado para ser identificado. Usualmente sirve para representar la versi�n de un trabajo. [Fuente](https://git-scm.com/book/en/v2/Git-Basics-Tagging)