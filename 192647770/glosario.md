a) Control de versiones (VC): sistema que registra todos los cambios realizados sobre un archivo o conjunto de archivos en un periodo de tiempo, con la posibilidad de modo que recuperar versiones anteriores del trabajo realizado.
b. Control de versiones distribuido (DVC): Es un sistema donde los clientes pueden descargan del servidor la �ltima versi�n del trabajo realizado, replicando completamente el repertorio.
c) Repositorio remoto y repositorio local: Los repositorios remotos son versiones del proyecto en el que se trabaja, los cuales est�n alojados en Internet o en alg�n punto de la red. Mientras que los repositorios locales, se encuentran en el computador donde se trabaja.
d) Copia de trabajo / Working Copy: es la copia de todos los archivos de un proyecto (�ltima versi�n), los cuales se encuentran en un repositorio. 
e) �rea de Preparaci�n / Staging Area: El �rea de preparaci�n (staging area), o tambi�n llamado �ndice, es una ubicaci�n virtual en la que se contiene todos los archivos que se desean incluir en la pr�xima confirmaci�n (commit). 
f) Preparar Cambios / Stage Changes: Cuando se realiza el comando git add, prepara un cambio para pr�ximo en el proyecto. 
g) Confirmar cambios / Commit Changes: Cuando se realiza el comando git commit, se confirman los cambios realizados en el proyecto. 
h) commint: Este comando toma los archivos que se encuentren con seguimiento, los cuales ser�n registrados en la base de datos, luego avanza el puntero a la rama actual.
i) clone: permite copiar repositorios que se encuentren en otras carpetas del ordenador o de un sitio remoto
j) pull: Este comando se utiliza para descarga informaci�n de un repositorio remoto, para posteriormente intentar combinarla en la rama que se esta trabajando.
k) push: Este comando es utilizado para comunicarse con otro repositorio, viendo las diferencias entre nuestra base local con la remota, para posteriormente subir (push) las diferencia al otro repositorio.
l) fetch: Este comando es utilizado para comunicarse con un repositorio remoto, obteniendo la informaci�n que se encuentra en este, para posteriormente almacenarlo en la base de datos local de uno.
m) merge: este comnado se utiliza para fusionar uno o m�s ramas dentro de la rama que que se est� trabajando actualmente.
n) status: Comando que nos permite revisar en que estado se encuentran un archivo, mostrando en que rama se encuentra.
o.) log: Este comando nos permite revisar en el historial las modificaciones que se han llevado a cabo.
p) checkout: es un comando que permite saltar de una rama en otra, debido a esto, mueve el Head a la rama con la que trabajare.
q) rama: Una rama es simplemente el apuntador a la rama local en la que nos encontramos de momento. En este caso, apunta a la rama master. 
r) etiqueta/tag: las etiquetas se utilizan com�nmente para marcar un punto espec�fico del proyecto, con las cuales se pueden ir marcando las diferentes versiones realizadas.


https://github.com/susannalles/MinimalEditions/wiki/Lista-Comandos-Git
https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Obtener-y-Crear-Proyectos
https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Compartir-y-Actualizar-Proyectos
https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones
https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F
https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
