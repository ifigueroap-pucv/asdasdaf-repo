# &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Taller 0 - Ing Web

En el siguiente documento se definir�n conceptos relevantes para la asignatura "Ingenier�a Web".

&nbsp;
* **Control de versiones (VC)**: Se refiere al sistema que gestiona toda creaci�n, cambio o eliminaci�n de un archivo, manteniendo un registro de estos a trav�s del tiempo. El prop�sito de esto es poder ocupar una versi�n anterior del(los) archivo(s) modificado(s).

&nbsp;
[*Fuente*](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

&nbsp;
* **Control de versiones distribuido (DVC)**: Se refiere al sistema  de compartir y respaldar un repositorio, esto simplifica el trabajo de los desarrolladores al no estar todo centralizado, por lo que si muere el servidor donde alojaban los archivos todos los que estaban en �l poseen copias, por ende, pueden continuar el trabajo.
Softwares de ejemplo serian: Git,Bazaar y Darcs.

&nbsp;
[*Fuente*](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

&nbsp;
* **Repositorio remoto y repositorio local**: Para dar una idea general digamos que un repositorio es una "despensa" que contiene los archivos a desarrollar y existen dos tipos de *despensas* o "bodegas"
  * Repositorio remoto: Esta *bodega* se ubica, por lo general, en un servidor, por ello todo desarrollador tiene acceso a �l
  * Repositorio local: Comunmente ser�an los computadores personales de trabajo de los desarroladores o en el dispositivo personal que ocupen para guardar los archivos (discos duros, pendrives,etc).

&nbsp;
[*Fuente 1*](http://blogs.ua.es/jpm33/2013/07/03/git-repositorios-remotos-y-desarrollo-distribuido/), [*Fuente 2*](https://es.wikipedia.org/wiki/Repositorio)

&nbsp;
* **Copia de trabajo** ***/ Working Copy***: es lo que hace nuestro DVCS, realiza copias de los archivos o ficheros en los que estamos trabajando realizando copias cada vez que haya alguna modificaci�n.

&nbsp;
[*Fuente*](https://neliosoftware.com/es/blog/copias-de-seguridad-con-git/)

&nbsp;
* **Area de preparaci�n** ***/Staging Area***: Es donde se almacena informaci�n de lo que se ir� en el pr�ximo commit.


&nbsp;
[*Fuente*](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

&nbsp;
* **Preparar cambios** ***/Stage Changes***: Archivos que estan listos o "en  posicion" para subirlos a nuestro repositorio remoto.

&nbsp;
[*Fuente*](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Guardado-r%C3%A1pido-provisional)

&nbsp;
* **Confirmar cambios** ***/Commit Changes***:

&nbsp;
* ***Commit***:

&nbsp;
* ***Clone***:

&nbsp;
* ***Pull***:

&nbsp;
* ***Push***:

&nbsp;
* ***Fetch***:

&nbsp;
* ***Merge***:

&nbsp;
* ***Status***:

&nbsp;
* ***Log***:

&nbsp;
* ***Checkout***:

&nbsp;
* **Rama / Branch**:

&nbsp;
* **Etiqueta / Tag**: Es una marca que puede especificar un hito o modificaci�n importante en nuestro archivo

&nbsp;
[*Fuente*](https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git)