*Control de versiones: Es una combiancion de tecnologias y practicas para saber los cambios que se le han hecho al codigo fuente. 
[Fuente] (http://producingoss.com/es/vc.html)                    
 
*Control de versiones distribuido: Lo que hace es que cuando un cliente descarga la ultima actualizacion replica todo el repositorio, lo cual sirve por si se llegara a perder un servidor facilmente se puede copier del repositorio de los clientes.
[Fuente] (https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

*Repositorio remoto y Repositorio local: Uno remoto son donde se alojan las versiones de mi proyecto, pueden ser de solo lectura o lectura/escritura. El repositorio local se usa cuando no se tiene acceso a internet, un ejemplo puede ser guardar las versiones e un dvd. 
[Fuente 1] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) [Fuente 2] (https://soporte.itlinux.cl/hc/es/articles/200121598-Crear-repositorio-Local)

*Copia de trabajo/Working copy: Es una copia de los archivos del depositos, es esta en donde se trabaja y editan los archivos. 
[Fuente] (https://www.osmosislatina.com/cvs_info/wcopy.htm)

*Area de preparacion/Staging area: Es un archivo que contiene lo que ira en la proxima confirmacion. 
[Fuente] (https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

*Preparar cambios/Stage changes: Es el area en donde se ajustan los commits y se ven que archivos seran confirmados (a�adiendo y eliminando), puede ser un paso complejo por lo que se suele saltar con con algunos comandos. 
[Fuente] (https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

*Confirmar cambios/Commit changes: Se hace luego de la preparacion de cambios, la cual al confirmar crea un instantanea en el area de preparacion, tambien devuelve una salida que indica los archivos confirmados. 
[Fuente] (https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

*Commit: Es cuando se guardan archivos en el repositorio y git le dio el nombre de commit. 
[Fuente] (https://desarrolloweb.com/articulos/iniciar-repositorio-git-primer-commit.html)

*Clone: Es cuando se clona un repositorio. 
[Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

*Pull: El usar pull es para recibir o recuperar datos. 
[Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

*Push: El usar push es para mandar, enviar datos. 
[Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

*Fetch: Es utilizado para recuperar la informacion. 
[Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

*Merge: Es usado para fusionar ramas. 
[Fuente] (https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

*Status: Se encarga de mostrar los estados de los archivos, por ejemplo si tengo algun archivo preperado y aun no se confirman, con este comando se pueden ver cuales son. 
[Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

*Log: Se usa para visualisar de forma historia todas las confirmaciones que se han hecho. 
[Fuente] (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

*Checkout: Crea una nueva rama o la activa. 
[Fuente] (https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

*Rama/Branch: Se usan para desarrollar funcionalidades aisladas, esiste una rama por defecto que es la master, al crear una repositorio se cran nuevas ramas. 
[Fuente] (http://rogerdudler.github.io/git-guide/index.es.html)

*Etiqueta/tag: Se utilizan para diferenciar las versiones publicadas, las cuales deben de tener un numero unico. 
[Fuente] (http://rogerdudler.github.io/git-guide/index.es.html)