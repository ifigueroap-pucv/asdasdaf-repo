a) control de versiones (vc) : El control de versiones es un sistema que registra cambios realizados en uno o m�s archivos con el fin de poder recuperar versiones
			       m�s espec�ficas.
 fuente: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

b) control de versiones distribuido (dvc) : toma un enfoque entre iguales, es decir, existir� un repositorio global. Alg�n cambio realizado en un repositorio se efectuar� en todos los repositorios
 
 fuente: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones y https://es.wikipedia.org/wiki/Control_de_versiones_distribuido

c) repositorio remoto y repositorio local: Un repositorio es  donde se guardan todos los documentos relacionados con el proyecto, incluida la documentaci�n, ademas posee una copia del c�digo.
	                                   Repositorio local es simplemente un repositorio alojado en nuestro equipo de trabajo, en cambio, un repositorio remoto es aquel repositorio que su direcci�n se encuentra en otro sitio, pero que de igual forma desde el local se podr�a acceder a �l.

 fuente: https://help.github.com/articles/github-glossary/
 
d) copia de trabajo: Corresponde a una version duplicada del trabajo realizado.

 fuente: https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

e) �rea de Preparaci�n : Es un  archivo, generalmente contenido en tu directorio de Git, que almacena informaci�n acerca de lo que va a ir en la pr�xima confirmaci�n. 

 fuente: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

f) Preparar Cambios:  Preparar un archivo a modificar que estuviese bajo seguimiento.

 fuente: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git 
 
g) Confirmar cambios : Al confirmar, la versi�n del archivo en el momento de ejecutar git add ser� la que se incluya en la instant�nea.

 fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
 
i) clone: Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo remota.

 fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git]

j) pull: Es un comando que baja los cambios de la rama determinada y la actualiza contra tu repositorio local.

 fuente: http://rogerdudler.github.io/git-guide/index.es.html

k) push: Es un comando que permite que los cambios se vean reflejados en tu repositorio remoto.

 fuente: http://rogerdudler.github.io/git-guide/index.es.html

l) fetch: Es un comando que baja los cambios de la rama determinada y la coloca en una rama "espejo" que simplemente es una clase de rama "escondida" en la cual tu puedes mirar los cambios de dicha rama, para posterior hacer merge con tu rama local.

 fuente: https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git

m) merge: Es un comando que se utiliza para fusionar uno o m�s ramas dentro de la rama que tienes activa. A continuaci�n avanzar� la rama actual al resultado de la fusi�n.

 fuente: https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar

n) status: Es un comando que utiliza para determinar qu� archivos est�n en qu� estado.

 fuente: https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar

o) log: Es un comando que se utiliza para mostrar la historia registrada alcanzable de un proyecto desde la m�s reciente instant�nea confirmada hacia atr�s. 

 fuente: https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar

p) checkout: Es un comando que se usa para cambiar de rama y revisar el contenido de tu directorio de trabajo.

 fuente: https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar

q) Rama: Es un comando que se utiliza como una especie de herramienta de gesti�n de ramas. Puede listar las ramas que tienes, crear una nueva rama, eliminar ramas y cambiar el nombre de las ramas.

 fuente: https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]

r)Etiqueta: Es un comando que se utiliza para dar un marcador permanente a un punto espec�fico en el historial del c�digo fuente. Generalmente esto se utiliza para cosas como las liberaciones (releases).

fuente: https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar