a. Control de versiones (VC): Gesti�n de los diversos cambios que se realizan sobre los elementos de alg�n producto.
Fuente: https://es.wikipedia.org/wiki/Control_de_versiones

b. Control de versiones distribuido (DVC): Permite a muchos desarrolladores de software trabajar en un proyecto com�n sin necesidad de compartir una misma red.
Fuente: https://es.wikipedia.org/wiki/Control_de_versiones_distribuido

c. Repositorio remoto y Repositorio local: Los repositorios remotos son versiones de un proyecto que est�n hospedadas en Internet en cualquier otra red. Los repositorios locales al igual que los remotos, son versiones de un proyecto pero esta vez est�n hospedadas en la m�quina en que se est�n trabajando.
Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

d. Copia de trabajo / Working Copy: Es un clon de el trabajo principal.
Fuente: https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms

e. �rea de Preparaci�n / Staging Area: Es un archivo contenido en nuestro directorio Git que guarda informaci�n sobre qu� ir� en el siguiente commit.
Fuente: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

f. Preparar Cambios / Stage Changes: Compara cambios antes de hacer el commit.
Fuente: https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository

g. Confirmar cambios / Commit Changes: Guarda los cambios en el repositorio.
Fuente: https://git-scm.com/docs/git-commit

h. Commit: Guarda la informaci�n actual contenida del �ndice en un nuevo
Fuente: https://git-scm.com/docs/git-commit

i. clone: Se refiare a clonar o copiar un repositorio.
Fuente: https://git-scm.com/docs/git-clone

j. pull: Se refiere a incorporar cambios de un repositorio a la rama actual.
Fuente: https://git-scm.com/docs/git-pull

k. push: Se refiere a actualizar referencias remotas usando referencias locales.
Fuente: https://git-scm.com/docs/git-push

l. fetch: Se refiere a descargar referencias y objetos desde otro repositorio.
Fuente: https://git-scm.com/docs/git-fetch

m. merge: Incorpora cambios desde un commit nombrado en la rama actual.
Fuente: https://git-scm.com/docs/git-merge

n. status: Muestra las diferencias entre el "working tree" y el trabajo actual.
Fuente: https://git-scm.com/docs/git-status

o. log: Muestra los registros del commit.
Fuente: https://git-scm.com/docs/git-log

p. checkout: Actualiza archivos en el working tree para igualar la versi�n en el �ndice o el �rbol especificado.
Fuente: https://git-scm.com/docs/git-checkout

q. Rama / Branch: Es una copia donde se pueden trabajar cambios antes de aplicarlos al proyecto final.
Fuente: https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms

r. Etiqueta / Tag: Es la habilidad de etiquetar partes del trabajo, usualmente es usado para etiquetar versiones.
Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas