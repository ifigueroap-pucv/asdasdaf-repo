a. Control de Versiones (VC) : El control de versiones sirve para poseer un historial de los cambios que se han hecho.
b. Control de Versiones Distribuido (DVC) : El control de versiones distribuidos sirve para que varios desarrolladores a la vez modifiquen un mismo proyecto en com�n sin la necesidad de compartir la misma red.
c. Repositorio Remoto y Repositorio Local : El repositorio remoto es una version de nuestro proyecto subido a la red, en cambio el local es el que mantenemos en nuestros ficheros.
d. Copia de Trabajo / Working Copy : Es cuando copiamos con GIT nuestro proyecto.
e. �rea de Preparaci�n / Staging Area : Es donde se determina el proximo cambio en el proyecto.
f. Stage Changes / Preparar Cambios : Es basicamente cuando uno usa el comando git add, prepara un cambio proximo en el proyecto
g. Confirmar Cambios/Commit Changes : Es cuando uno usa el git commit y confirma los cambios realizados en el proyecto.
h. Commit : Sirve para confirmar los cambios hechos en el proyecto y a su vez comentar el porque fueron hechos estos cambios.
i. Clone : Sirve para "clonar" o "copiar" desde el repositorio remoto a un repositorio local.
j. Pull : Sirve para actualizar el repositorio local.
k. Push : Sirve para subir los cambios del repositorio local al remoto.
l. Fetch: Trae los cambios hechos pero los deja en otra rama.
m. Merge : Trae los cambios dejados en otra rama, a la rama principal.
n. Status : Sirve para ver los archivos listos para hacer commit.
o. Log : sirve para ver los ultimos commit hechos.
p. Checkout : es para ir de una rama a otra rama.
q. Rama / Branch : Es un tipo de apuntador a los cambios realizador por un usuario.
r. Etiqueta/Tag : Es como un tipo de rama que nunca cambia en el tiempo. Siempre apunta a lo mismo.