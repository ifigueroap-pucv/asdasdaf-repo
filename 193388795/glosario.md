Taller 0

===================

1. Control de versiones: Es una forma de organizar los cambios que se realizan sobre un producto de software para asi poder distribuir estos cambios 
como una version del producto.
2. Control de versiones distribuido: Es cuando no existe un �nico servidor que almacena el c�digo, si no que todos los desarrolladores tienen todo
el c�digo y pueden hacer cambios de forma local que despu�s pueden enviar a los otros desarrolladores.
3. Repositorio remoto y repositorio local: Un repositorio remoto es un lugar donde se guarda el c�digo que se encuentra en una ubicaci�n remota. Un repositorio
local es cuando el codigo esta almacenado en mi mismo equipo o en uno cercano.
4. Copia de trabajo: Es la copia en la que se realizan cambios locales.
5. Area de preparaci�n: Es un archivo que generalmente esta en el repositorio Git y que guarda informacion sobre QUE ira en el proximo commit. Se conoce
como el Index
6. Preparar cambios: Es cuando se marca un archivo en su version actual para que entre en el proximo commit
7. Confirmar cambios: Se toman los archivos como estan en el area de preparacion and guarda esa snapshot permanentemente al directorio git
8. Commit: Confirma los cambios hechos en los archivos que fueron incluidos con git add. Es necesario hacer git push para que los cambios se vean reflejados en el repositorio remoto.
9. Clone: Permite copiar un repositorio remoto en el equipo local.
10. pull: Baja los cambios del repositorio remota a la rama funsionandolas (merge)
11. push: peromite enviar la rama que en la que se esta trabajando a la rama de producci�n
12. fetch: Baja los cambios del repositorio remoto a la rama sin fusionarlas (merge) localmente
13. merge: Permite fusionar ramas
14. Status: El comando Status permite inquirir el estado de un archivo
15. Log: Log permite ver hacia atras las modificacionmes que se han llevado a cabo en un repositorio con un hist�rico de confirmaciones
16. chekout: Se puede utilizar para cambiarde rama, descartar cambios en la rama, crear rama, etc
17. Rama / Branch: Las ramas se utilizan para crear otra l�nea de desarrollo, un apuntador movil apuntando a confirmaciones. 
18. Etiqueta / TAg: permite etiquetar puntos de importancia especificos en la historia, se suele usar para marcar puntos donde se han lanzados nuevas versiones del programa.Es decir, la etiqueta es una rama, que nadie tiene la intenci�n de modificar.
