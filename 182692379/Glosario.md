1. **Control de versiones (VC)**: Es un sistema que permite registrar los cambios realizados sobre archivos de un proyecto a lo largo del tiempo, de modo que se puedan recuperar versiones específicas más adelante. [[1]]
2. **Control de versiones distribuido (DVC)**: Son aquellos que no hacen referencia a un repositorio central alojado en un servidor único, éste está distribuido, es decir cada uno de los desarrolladores tiene una copia local de la version con toda la información en sus computadoras. [[2]]
3. **Repositorio remoto y Repositorio local**: El repositorio remoto es una version online de un proyecto versionado con git. El repositorio local es la copia que se almacena en el computador de quien trabaja el proyecto. Estos se conectan a través de push y pull. [[3]]
4. **Copia de trabajo / Working Copy**: Es lo que se crea al clonar un repositorio remoto, el cual inicia las referencias de un proyecto en git y es una copia exacta que está en el servidor sobre el cual se trabajará. [[4]]
5. **Área de Preparación / Staging Area**: Es una de las 3 secciones de los estados git, en esta etapa se almacenan todos cambios que irán en el próximo commit. [[5]]
6. **Preparar Cambios / Stage Changes**: Es cuando los cambios se le informan a git, pero aún no se agregan a un commit, es decir, que aún no se agregan de forma definitiva al proyecto. [[6]]
7. **Confirmar cambios / Commit Changes**: Cuando todos los cambios que están en la etapa de preparación son confirmados. [[7]]
8. **Commit**: Guarda los cambios que se han hecho desde la último estado en el area de trabajo, se le asocia un mensaje definido por el usuario para resumir lo que contiene. [[8]]
9. **clone**: Clona un repositorio remoto en una carpeta vacía nueva o existente. [[9]]
10. **pull**: Agrega los cambios de un repositorio remoto al repositorio local. [[10]]
11. **push**: Actualiza el repositorio remoto enviando las copias de los cambios locales. [[11]]
12. **fetch**: Descarga los cambios del repositorio remoto y los almacena de forma local, sin aplicar cambios. [[12]]
13. **merge**: Agrega los cambios de una rama a otra, integra los cambios de forma tal que mantiene el orden temporal de los commits. [[13]]
14. **status**: Muestra los cambios actuales del area de trabajo en comparación a lo que apunta el último commit sobre el que se está trabajando. Pueden ser modificaciones, agregaciones o eliminaciones. [[14]]
15. **log**: Muestra el historial de los últimos commits. [[15]]
16. **checkout**: Sirve para cambiar la rama en la que se está trabajando o para reestablecer el estado de una rama. [[16]]
17. **Rama / Branch**: Las ramas son utilizadas para desarrollar funcionalidades aisladas unas de otras, asi el cliente puede tomar caminos independientes en el desarrollo de un software, generalmente se utiliza para resolver problemas o crear nuevas funcionalidades. [[17]]
18. **Etiqueta / Tag**: Sirve para marcar una referencia a un commit dentro del proyecto, de esta forma podemos etiquetar un punto específico en el historial del proyecto, se usa comunmente para el versionado de éstos. [[18]]

[1]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
[2]: http://blog.urcera.com/wordpress/?p=355
[3]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
[4]: https://git-scm.com/book/es-ni/v1/Git-Basics-Obteniendo-un-Repositorio-Git
[5]: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
[6]: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
[7]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
[8]: https://git-scm.com/docs/git-commit
[9]: https://git-scm.com/docs/git-clone
[10]: https://git-scm.com/docs/git-pull
[11]: https://git-scm.com/docs/git-push
[12]: https://git-scm.com/docs/git-fetch
[13]: https://git-scm.com/docs/git-merge
[14]: https://git-scm.com/docs/git-status
[15]: https://git-scm.com/docs/git-log
[16]: https://git-scm.com/docs/git-checkout
[17]: http://rogerdudler.github.io/git-guide/index.es.html
[18]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas