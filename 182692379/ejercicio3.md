## Ejercicio 3

#### Cantidad y nombre de los branches

Hay 5 ramas, son las siguientes:

- origin/HEAD -> origin/master
- origin/master
- origin/megaramita1
- origin/ramita1
- origin/ramita2

#### Cantidad y nombre de las etiquetas

Hay 9 etiquetas, son las siguientes:

- master
- megaramita1
- ramita1
- ramita2
- asdasdaf-tag
- final-final-ahora-si
- super-release
- tag1
- todas-las-tags-al-mismo-commit

#### Ùltimos 3 commits

commit 5a73831b72a19bff5ef7a9850af8cd365d635dd0
Merge: 70db4a7 438e7bb
Author: Esteban Gomez <estebangmontero@gmail.com>
Date:   Tue Aug 8 19:55:24 2017 -0400

    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

commit 70db4a77372d7f89f7045dd38ff8f8a14fc50976
Author: Esteban Gomez <estebangmontero@gmail.com>
Date:   Tue Aug 8 19:50:29 2017 -0400

    sdssadsaddsa

commit 0626fa154493d5721a8bb0a1187a3ecd2c640346
Author: Esteban Gomez <estebangmontero@gmail.com>
Date:   Tue Aug 8 19:45:37 2017 -0400

    sadasdasd