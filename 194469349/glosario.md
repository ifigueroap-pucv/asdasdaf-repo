**Glosario**

* Control de versiones (VC): Es la gestion del cambio sobre algun software o una configuracion.
* Control de versiones distribuido (DVC): Es lo que permite a los programadores de un proyecto trabajar en comun sin necesidad de estar en la misma red.
* Repositorio remoto y Repositorio local: Son versiones del proyecto que se encuentran en algun almacenamiento en la internet o una red y el repositorio local es lo mismo pero vista de la forma local.
* Copia de trabajo / Working Copy: Son directorios en el sistema local, que contienen una coleccion de archivos, se pueden editar como uno desee.
* �rea de Preparaci�n / Staging Area: es un sistema que se encuentra entre la fuente de datos y el almacen de datos el cual su objetivo es facilitar la extraccion de datos desde la fuente de origen, realizar limpieza de datos y ser usado como cache.
* Preparar Cambios / Stage Changes: Prepara los cambios de la proxima accion.
* Confirmar cambios / Commit Changes: Para confirmar los cambios a realizar.
* Commit: Comando para hacer registro de los cambios hechos en el repositorio 
* clone: Comando para obtener una copia de la ultima version del proyecto.
* pull: Comando para recibir la informacion de un proyecto clonado de un git.
* push: Comando que sirve para enviar tu trabajo al repositorio remoto.
* fetch: Comando para recuperar la informacion de alguien que no esta en el repositorio remoto.
* merge: se utiliza para fusionar 2 o mas ramas dentro de la rama activa.
* status: Se�ala de direccion de los directorios que contienen diferencia entre el archivo indice y el commit head.
* log: Muestra todos los registros de los commit
* checkout: Instruccion para cambiar rama.
* Rama / Branch: Es un apuntador que apunta a las confirmaciones.
* Etiqueta / Tag: Deja registro de los cambios realizados