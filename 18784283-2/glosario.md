a. Control de versiones (VC)

Es un sistema que registra los cambios que se realizan sobre un archivo o conjunto de archivos a lo largo del tiempo,
esto evita tener muchos archivos para cada versi�n ya que el sistema permite recuperar las versiones anteriores.
Fuente: https://git-scm.com

b. Control de versiones distribuido (DVC)

Es un sistema en el que cada cliente descarga la ultima versi�n del o los archivos, generando as� tantas copias de 
seguridad como descargas se hayan hecho. Adem�s de evitar multiples archivos distintos con versiones distintas permite
colaborar con distintos grupos de gente simultaneamente aunque no esten en la misma red.
Fuente: https://git-scm.com

c. Repositorio remoto y Repositorio local

Los repositorios remotos son archivos que se encuentran alojados en internet o en algun punto de red, Mientras que 
los repositorios locales es aquel en el que realizamos cambios locales, es decir se encuentra en nuestro computador.
Fuente: https://git-scm.com

d. Copia de trabajo / Working Copy

son los archivos locales modificados que a�n no hemos a�adido para preparar el commit.
Fuente: https://mydefinitionofdone.com

e. �rea de Preparaci�n / Staging Area

Es un archivo, generalmente contenido en su directorio de Git, que almacena informaci�n sobre lo que va a entrar en su pr�ximo commit.
Fuente:https://git-scm.com

f. Preparar Cambios / Stage Changes

Es un archivo que esta en el "area de preparaci�n" pero aun no se ha confirmado los cambios.
Fuente:https://git-scm.com


g. Confirmar cambios / Commit Changes

Es la confirmacion de un archivo que esta en "stage changes" para que sea montada en la versi�n principal.
Fuente:https://git-scm.com

h. Commit

git Commit identifica los cambios hechos en dicho ambiente de trabajo. Trabaja en el repositorio local.
Fuente: https://es.stackoverflow.com

i. clone

es un comando para obtener una copia de un repositorio Git existente.
Fuente:https://git-scm.com

j. pull

git pull es una abreviatura para git fetch seguido por git merge FETCH_HEAD.
Fuente:https://git-scm.com

k. push

Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo remota.
Fuente: https://es.stackoverflow.com

l. fetch

Comando que descargar objetos y referencias desde otro repositorio
Fuente: https://git-scm.com

m. merge
Comando que incorpora cambios desde un repositorio remoto a la sucursal actual.
Fuente:https://git-scm.com

n. status

Comando que muestra el estado del arbol de trabajo.
Fuente:https://git-scm.com

o. log

Comando que muestra el registro de los "commits"
Fuete:https://git-scm.com

p. checkout

Comando que cambia ramas o restaura archivos del �rbol de trabajo
Fuete:https://git-scm.com

q. Rama / Branch

Es un apuntador m�vil apuntando a un "commit" especifico.
Fuente:https://git-scm.com

r. Etiqueta / Tag

Puntos espec�ficos en la historia como importantes. Generalmente 
la gente usa esta funcionalidad para marcar puntos donde se ha lanzado alguna versi�n.
Fuente:https://git-scm.com
