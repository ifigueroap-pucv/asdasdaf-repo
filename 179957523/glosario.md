##Glosario de terminos

a. **Control de versiones** (VC): Es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones espec�ficas m�s adelante. 

[https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones]

b. **Control de versiones distribuido** (DVC): Es un sistema donde los clientes no s�lo descargan la �ltima instant�nea de los archivos: replican completamente el repositorio. 

[https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones]

c. **Repositorio remoto y Repositorio local**: El repositorio remoto es donde las versiones de un proyecto que se encuentran alojados en Internet o en alg�n punto de la red. Puedes tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que tengas. El repositorio local esta compuesto por tres "�rboles" administrados por git. El primero es tu Directorio de trabajo que contiene los archivos, el segundo es el Index que actua como una zona intermedia, y el �ltimo es el HEAD que apunta al �ltimo commit realizado.

[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git]

d. **Copia de trabajo / Working Copy**: Corresponde a una version duplicada del trabajo realizado.

[https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git]

e. **�rea de Preparaci�n / Staging Area**: Es un sencillo archivo, generalmente contenido en tu directorio de Git, que almacena informaci�n acerca de lo que va a ir en tu pr�xima confirmaci�n. 

[https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git]

f. **Preparar Cambios / Stage Changes**:  Preparar un archivo a modificar que estuviese bajo seguimiento.

[https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git]

g. **Confirmar cambios / Commit Changes**: Al confirmar, la versi�n del archivo en el momento de ejecutar git add ser� la que se incluya en la instant�nea.

[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio]

h. **Commit**: Es un comando donde se guardan los cambios que se han hecho localmente.

[http://rogerdudler.github.io/git-guide/index.es.html]

i. **clone**: Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo remota.

[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git]

j. **pull**: Es un comando que baja los cambios de la rama determinada y la actualiza contra tu repositorio local.

[http://rogerdudler.github.io/git-guide/index.es.html]

k. **push**: Es un comando que permite que los cambios se vean reflejados en tu repositorio remoto.

[http://rogerdudler.github.io/git-guide/index.es.html]

l. **fetch**: Es un comando que baja los cambios de la rama determinada y la coloca en una rama "espejo" que simplemente es una clase de rama "escondida" en la cual tu puedes mirar los cambios de dicha rama, para posterior hacer merge con tu rama local.

[https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git]

m. **merge**: Es un comando que se utiliza para fusionar uno o m�s ramas dentro de la rama que tienes activa. A continuaci�n avanzar� la rama actual al resultado de la fusi�n.

[https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]

n. **status**: Es un comando que utiliza para determinar qu� archivos est�n en qu� estado.

[https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]

o. **log**: Es un comando que se utiliza para mostrar la historia registrada alcanzable de un proyecto desde la m�s reciente instant�nea confirmada hacia atr�s. 

[https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]

p. **checkout**: Es un comando que se usa para cambiar de rama y revisar el contenido de tu directorio de trabajo.

[https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]

q. **Rama / Branch**: Es un comando que se utiliza como una especie de herramienta de gesti�n de ramas. Puede listar las ramas que tienes, crear una nueva rama, eliminar ramas y cambiar el nombre de las ramas.

[https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]

r. **Etiqueta / Tag**: Es un comando que se utiliza para dar un marcador permanente a un punto espec�fico en el historial del c�digo fuente. Generalmente esto se utiliza para cosas como las liberaciones (releases).

[https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Ramificar-y-Fusionar]