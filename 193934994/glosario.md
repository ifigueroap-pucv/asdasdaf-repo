# GLOSARIO


1. Control de versiones (VC): Es el sistema que gestiona los cambios realizados en archivos, documentos, codigos fuentes, etc.
En general se usa para seguir las modificaciones de un proyecto, ya que permite la recuperacion de versiones anteriores.
Se suele identificar una version con numeros o codigos de letras seguidos de la palabra "revision", por ejemplo, revision 1. 

2. Control de versiones distribuido (DVC): Es una forma del control de versiones que permite trabajar en un proyecto 
a varios usuarios a la vez, sin la necesidad de estar conectados a la misma red. Usualmente se utiliza un servidor para
que los clientes puedan replicar el repositorio completamente, asi cualquiera de los clientes puede conectarse al servidor
y restaurarlo en caso de que este falle.

3. Repositorio remoto y Repositorio local: Repositorio remoto, es donde esta almacenado el proyecto en Internet o en un punto de red.
Repositorio local es la copia del repositorio remoto o el  comienzo de un reposiotorio almacenado almacenado de forma loca, permitiendo modificarlo
sin la necesidad de estar conectado a Internet.

4. Copia de trabajo / ​Working Copy: copia local de archivos administrada por el sistema de control de versiones, 
de un repositorio. En otras palabras el almacen local en el que se esta trabajando actualmente.

5. Área de Preparación /Staging Area: Es un archivo en el que se almacenan  los cambios que se enviarán en el proximo commit.

6. Preparar Cambios / Stage Changes: El estado de los cambios realizados en un repositorio antes de hacer commit.

7. Confirmar cambios / Commit Changes: Es la accion de de confirmar los cambios realizados en el repositorio, para que este disponible a otros
usuarios.

8. Commit: Es un comando que se usa para cuando se juntan o escriben los cambios realizados en la copia de trabajo al repositorio. Es como 
"guardar" los cambios, pero  usualmente tiene un mensaje commit con una descripcion de los cambios realizados.

9. clone: Es una copia de un repositorio generalmente remoto, que sirve para utilizarde forma local.
 Tambien se crea un respositorio en base a las revisiones de otro respositorio existente. 

10. pull: Es la operacion para buscar cambios y obtener esa copia. Se hace para mantener actualizada la copia en la
que se esta trabajando

11. push: Es la operacion de enviar los cambios realizados a un repositorio remoto, se hace para que los cambios
realizados de forma local esten disponible para otros usuarios.

12. fetch: Cuando se obtiene los ultimos cambios de un repositorio online pero sin juntarlos

13. merge: Es la operacion de fusionar los cambios realizados en branches independientes, para obtener una coleccion unica
de archivos que contiene el conjunto de cambios

14. status: Es un comando para determinar el estado de algunos archivos

15. log: Es un comando de Git para ver el historial de commits que se han hecho en un repositorio.

16. checkout: Es crear un working copy local desde un repositorio, pudiendo especificar una revision especifica u 
obtener la ultima revision. Ademas se puede utilizar este termino para referirse al working copy.

17. Rama / Branch: Es una version (copia) paralela del respositorio, para que pueda ser modificado paralelamente sin entorpecer
los trabajos. Cuando ya sea hayan realizado las modificaciones pueden volver a fusionarse con el repositorio (rama) principal.

18. Etiqueta / Tag: Se utiliza un Tag para etiquetar en un punto particular  en la cadena commit a un conjunto de archivos 
determinados para poder identificarlos mediante un nombre o numero de revision.

## FUENTES
1. http://producingoss.com/es/vc.html
 https://en.wikipedia.org/wiki/Version_control
 
 2. https://en.wikipedia.org/wiki/Distributed_version_control
 https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
 
 3. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
 
 4. https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
 http://gitready.com/beginner/2009/01/18/the-staging-area.html
 
 5. http://gitready.com/beginner/2009/01/18/the-staging-area.html
 https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
 
 6. https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
 
 7. https://en.wikipedia.org/wiki/Commit_(version_control)
    http://www.ecodeup.com/instala-y-crea-tu-primer-repositorio-local-con-git-en-windows/
 
 8. https://en.wikipedia.org/wiki/Version_control
 https://help.github.com/articles/github-glossary/
 
 9. https://help.github.com/articles/github-glossary/
	https://en.wikipedia.org/wiki/Version_control

10.	https://help.github.com/articles/github-glossary/
11. https://help.github.com/articles/github-glossary/


12. https://help.github.com/articles/github-glossary/
	https://en.wikipedia.org/wiki/Version_control

13. https://en.wikipedia.org/wiki/Merge_(version_control)

14. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

15. https://www.desarrolloweb.com/articulos/git-log.html
	
16. https://en.wikipedia.org/wiki/Version_control

17. https://help.github.com/articles/github-glossary/
	https://en.wikipedia.org/wiki/Version_control
	
18.	https://www.atlassian.com/git/glossary/terminology
	https://en.wikipedia.org/wiki/Version_control

