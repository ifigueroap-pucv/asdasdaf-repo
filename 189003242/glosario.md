-Control de Versiones (VC): Es un sistema de guardara los cambios realizados en un proyecto con el objetivo de poder tener una traza de aquellos cambios por si su uso es necesario.

-Control de Versiones distribuido (DVC): En este sistema, cada vez que se hace una nueva actualizacion del projecto, es guardado por todos los servidores que esten distribuyendo estos cambios, de esta manera, si un servidor cae, los otros tendran guardada toda la informacion ya editada.

-Repositorio remoto y Repositorio Local: El repositorio remoto es una version de un proyecto que esta guardado en un punto de la red o en el internet. Este repositorio puede ser editado, enviado o recibido libremente. El repositorio Local se encuentra alojado en el disco duro, pero no se encuentra en internet.

-Copia de Trabajo / Working Copy: Hacer una copia total de tu proyecto, clonando el repositorio final en todos los servidores principales.

-�rea de Preparaci�n / Staging Area: Es un estado temporal el cual se enviaran los futuros cambios del proyecto en forma de commit.

-Preparar Cambios / Stage Changes: es un estado en el que git sabe que hay cambios pero no los ha a�adido permanentemente en el repositorio hasta que estos entren en estado de confirmacion (commit).

-Confirmar cambios / Commit Changes: En este punto, todos los cambios han sido a�adidos en el repositorio al cambiarlos en estado commit.

-Commit: graba todos los cambios al repositorio

-clone: copia completamente un repositorio hacia una nueva carpeta

-pull: obtiene o agrega desde otro repositorio o alguna rama local

-push: actualiza los cambios hechos en el repositorio local, a los demas repositorios al cual esta conectado

-fetch: Descarga objetos y referencias desde otros repositorios

-merge: fusiona dos o mas ramas del repositorio

-status: muestra como van todas las ramas de trabajo

-log: muestra todos los datos y cambios realizados ultimamente.

-checkout: crea una nueva rama y cambias inmediatamente hacia ella.

-Rama / Branch: con este comando puedes listar, crear y borrar otras ramas

-Etiqueta / Tag: con este comando puedes agregar, listar, borrar o verificar etiquetas marcadas por cada actualizacion.
