## Datos ejercicio3

**a. Cantidad y nombre de los branches**: Tiene 2 ramas y son: 

- master y sebastianS


**b. Cantidad y nombre de las etiquetas**: Tiene 5 etiquetas y son: 

- asdasdaf-tag
- final-final-ahora-si
- super-release
- tag1
- todas-las-tags-al-mismo-commit

**c. Nombre, mensaje y c�digo hash de los �ltimos 3 commits**:

    commit 5f8e13c78ffab33f5554a798e59ce310a1bf9f01 
    (HEAD -> sebastianS, origin/mast
    er, origin/HEAD, master)
    Merge: 678cc44 b0dd37b
    Author: unknown <pablo.zuniga.vale@gmail.com>
    Date:   Wed Aug 9 10:36:37 2017 -0400

    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

    commit b0dd37b4a8cc72babf75f2d0701cc760d1e5b470
    Merge: 216ef67 e40efe6
    Author: Diego Rodriguez 
    <diego.rodriguez.q@mail.pucv.cl>
    Date:   Wed Aug 9 10:34:15 2017 -0400

    Merge branch 'master' of 
    https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

    commit e40efe65c25665327ad4ce8e54132c5a485192c3
    Merge: 3ac903a 2ba4238
    Author: cristian rivera <cart_1997@hotmail.com>
    Date:   Wed Aug 9 10:50:33 2017 -0400

    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo
