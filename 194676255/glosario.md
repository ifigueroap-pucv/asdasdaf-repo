## Datos glosario

- **Control de versiones (VC)**
: Es un sistema que registra los cambios realizados sobre uno o mas archivos, los cuales pueden ser recuperados si el usuario lo requiere.	[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- **Control de versiones distribuido (DVC)**: Permite trabajar en un proyecto com�n, sin tener que compartir la misma red.	[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

- **Repositorio remoto y Repositorio local**
: Los repositorios remotos corresponden a las versiones del proyecto que estan almacenados en la Internet, en cambio los repositorios locales se encuentran almacenados dentro del equipo.	[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

- **Copia de trabajo / Working Copy**: Son los archivos locales modificados que a�n no han sido a�adidos para preparar el commit.	[Fuente](https://mydefinitionofdone.com/2017/04/12/git/) 

- **�rea de Preparaci�n / Staging Area**: Es un archivo que contiene la informacion, sobre que ira en el siguiente commit.	[Fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

- **Preparar Cambios / Stage Changes**:  Es un archivo que est� en el "�rea de preparaci�n" pero aun no se han confirmado los cambios.	[Fuente](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

- **Confirmar cambios / Commit Changes**: El git commit identifica y confirma todos los cambios hechos en dicho ambiente de trabajo. Trabaja con el repositorio local	[Fuente](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

- **Commit**:	Corresponde al almacenamiento de los cambios realizados en el archivo, dentro de los repositorios locales.	[Fuente](https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push)

- **Clone**: Copia un repositorio hacia una nueva direccion ya existente.	[Fuente](https://git-scm.com/docs/git-clone)

- **Pull**: Incorpora los cambios realizados, desde un repositorio remoto hacia un branch actual.	[Fuente](https://git-scm.com/docs/git-pull)

- **Push**
: Actualiza las referencias remotas, mediante la utilizacion de las referencias locales, mientras se envian los objetos para completar el proceso.	 [Fuente](https://git-scm.com/docs/git-push)
- **Fetch**: Descarga los objetos y referencias de otro repositorio	[Fuente](https://git-scm.com/docs/git-fetch)

- **Merge**: Incorpora cambios del commit utilizado hacia el branch actual.	[Fuente](https://git-scm.com/docs/git-merge)

- **Status**:	Muestra las rutas que tienen diferencias entre el archivo index y la cabeza del commit actual.	[Fuente](https://git-scm.com/docs/git-status)

- **Log**
: Muestra los registros de confirmacion.	[Fuente](https://git-scm.com/docs/git-log)

- **Checkout**
: Actualiza los archivos del �rbol actual, para que coincidan con la versi�n del arbol especificado.	[Fuente](https://www.kernel.org/pub/software/scm/git/docs/git-checkout.html)

- **Rama / Branch**
: Es un puntero ligero y dinamico, el cual contiene las direcciones de los commits.	[Fuente](https://git-scm.com/book/en/v1/Git-Branching-What-a-Branch-Is)

- **Etiqueta / Tag**: Crea, enumera, elimina o verifica un objeto de etiqueta, hecho con GPG.		[Fuente](https://git-scm.com/docs/git-tag)