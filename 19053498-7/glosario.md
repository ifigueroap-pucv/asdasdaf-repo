1. Control de versiones

...Sistema de registro de cambios realizados sobre un archivo o conjunto de archivos a trav�s del tiempo, de manera que sea posible recuperar versiones anteriores del archivo cuando se necesite. Adem�s se puede utilizar para comparar cambios a trav�s del tiempo y ver quien realiz� cambios. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Git - Acerca del control de versiones")

2. Control de versiones distribuido

...Es una configuraci�n del control de versiones que permite que cada miembro de un proyecto pueda acceder a todos los cambios que se han hecho a�n si el servidor no est� disponible. Esto es posible ya que cada miembro del proyecto descarga el repositorio completo del proyecto, de manera de que si el servidor que almacena dicho proyecto se cae, se puede cualquier copia para restaurar el servidor. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Git - Acerca del control de versiones")

3. Repositorio remoto y repositorio local

...Un repositorio local es un almacen de gestion y almacenaci�n de archivos e informaci�n, ubicado en el mismo equipo. Un repositorio remoto es lo mismo, pero ubicado en un equipo que puede estar en la misma red local, o a trav�s de internet. [Fuente](https://www.bibliotecapiloto.gov.co/categoria-extension-cultural/145-sabes-que-son-y-para-que-sirve-los-repositorios-digitales "�Sabes qu� son y para qu� sirve los repositorios digitales?")

4. Copia de trabajo

...Copia del repositorio en la que el desarrollador agrega, modifica o elimina trabajo antes de implementarlo en el repositorio. [Fuente](https://tortoisesvn.net/docs/release/TortoiseSVN_es/tsvn-qs-basics.html "Conceptos b�sicos del control de versiones")

5. �rea de preparaci�n

...Es el �rea del directorio de trabajo en la que se almacenan los cambios realizados por el usuario a la copia de trabajo, y es desde donde se selecciona que cambios se van a enviar al repositorio. [Fuente](http://gitready.com/beginner/2009/01/18/the-staging-area.html "git ready>>the staging area")

6. Preparar cambios

...Se refiere a cuando se crea un cambio al trabajo pero se implementa a la copia de trabajo, o a una rama del repositorio principal [Fuente](https://githowto.com/staging_changes "6. Staging the changes")

7. Confirmar cambios

...Se refiere a cuando se aplican cambios al repositorio principal, adem�s de indicar quien y cuando realiz� dicho cambio [Fuente](https://git-scm.com/docs/git-commit)

8. Commit

...Comando de git que se usa para confirmar los cambios realizados a un repositorio [Fuente](https://git-scm.com/docs/git-commit)

9. Clone

...Comando de git que se usa para crear una copia de trabajo local [Fuente](https://githowto.com/cloningrepositories)

10. Pull

...Comando de git que permite recibir la version mas actualizada del repositorio sin tener que crear una copia de trabajo nueva solo para eso [Fuente](https://git-scm.com/docs/git-pull)

11. Push

...Comando de git que permite actualizar un repositorio (que no es el principal) con los cambios realizados de manera local [Fuente](https://git-scm.com/docs/git-push)

12. Fetch

...Comando de git que se utiliza para obtener una rama del repositorio sin aplicar los cambios de esta en la copia de la rama principal [Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

13. Merge

...Comando de git utilizado para unir cambios realizados en una rama con el repositorio principal [Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

14. Status

...Comando de git usado para observar el estado del repositorio principal, sus ramas, y del �rea de preparaci�n [Fuente1](https://git-scm.com/docs/git-status) [Fuente2](https://githowto.com/checking_status)

15. Log

...Comando de git que permite ver la lista de cambios realizados en un repositorio, junto con su hash identificador, descripci�n, quien lo realiz� y cuando lo hizo [Fuente](https://git-scm.com/docs/git-log)

16. Checkout

...Comando que permite cambiar la rama del repositorio en donde se trabaja [Fuente](https://git-scm.com/docs/git-checkout)

17. Rama

...Copia del repositorio principal a la que se le aplicaron cambios mediante commit [Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

18. Etiqueta

...Es un nombre que se le asigna a un log para diferenciarlo, destacarlo, y para hacerlo mas f�cil de encontrar ya que reemplaza al hash identificador [Fuente](https://git-scm.com/book/en/v2/Git-Basics-Tagging)