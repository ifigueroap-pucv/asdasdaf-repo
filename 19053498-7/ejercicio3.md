1. Cantidad y nombre de los branches

...$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/megaramita1
  remotes/origin/ramita1
  remotes/origin/ramita2

2. Cantidad y nombre de las etiquetas

...$ git tag -l
asdasdaf-tag
final-final-ahora-si
super-release
tag1
todas-las-tags-al-mismo-commit

3. Nombre, mensaje y c�digo hash de los �ltimos 3 commits

...$ git log
commit 920f2f2e52c7cf5f7753043bcb927403aa29e7e1 (HEAD -> master)
Author: Oscar Sepulveda Jorquera <oscar.sepulveda.jorquera@gmail.com>
Date:   Wed Aug 9 01:57:40 2017 -0400

    Se ha a�adido el glosario

commit 9ca8dc7e3ee22a0a95191a44d63bf5d6842f69a4 (origin/master, origin/HEAD)
Author: CarlosVelasquezAyan <carlos.velasquez.a@mail.pucv.cl>
Date:   Wed Aug 9 00:02:03 2017 -0400

    Modificacion archivo ejercicio3.md

commit 5925d9148daccc458daefa55f7a7ad938ec71802
Author: CarlosVelasquezAyan <carlos.velasquez.a@mail.pucv.cl>
Date:   Tue Aug 8 23:08:52 2017 -0400

    Archivos faltantes
