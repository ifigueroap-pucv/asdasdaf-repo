Glosario de Términos
===================
a. **Control de versiones (VC)**: Un sistema que registra los cambios realizados, y los deja guardados por si existe la necesidad de volver atrás.
> https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

b. **Control de versiones distribuido (DVC)**: Todos los clientes pueden trabajar en un mismo proyecto, por lo que se pueden registrar los cambios de cualquiera de estos clientes.
>https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

c. **Repositorio remoto y Repositorio local**: Repositorio remoto es una copia de seguridad guardada en la nube, mientras que el local en un equipo o medio de guardado. 
>https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
>https://soporte.itlinux.cl/hc/es/articles/200121598-Crear-repositorio-Local

d. **Copia de trabajo / Working Copy**: Documento que posee información de corto plazo o efímera. Estos documentos pueden ser una copia de referencia yel hecho de ser sólo una copia no altera la forma de trabajo.
>http://www.expertglossary.com/records/definition/working-copy

e. **Área de Preparación / Staging Area**: Es la zona de espera, por donde debe pasar los cambios antes de realizar un commit.
>https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

f. **Preparar Cambios / Stage Changes**: Etapas por las que debe pasar un archivo previo a ser subido.
>https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository

g. **Confirmar cambios / Commit Changes**: Guardar definitivamente los cambios.
>https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository

h.**Commit**: Commit es cuando se guarda un proyecto o archivo, el cual lleva además un mensaje dentro (Por ejemplo, hablando de los cambios o con algún recordatorio).
>https://help.github.com/articles/github-glossary/

i. **clone**: Es una copia de seguridad dentro de un computador en vez de en un sitio web.
>https://help.github.com/articles/github-glossary/

j.  **pull**: Pull hace referencia a subir cambios y que estos se sincronicen con los demás, sobr escribiendo por ejemplo un código.
>https://help.github.com/articles/github-glossary/

k. **push**: Push es subir los cambios, por ejemplo a un repositorio.
>https://help.github.com/articles/github-glossary/

l. **fetch**: Obtener los últimos cambios del repositorio sin fusionarlos.
>https://help.github.com/articles/github-glossary/

m. **merge**: Es la fusión de los cambios, unirlos, esto sucede comúnmente  con una solicitud de extracción o mediante una línea de comandos.
>https://help.github.com/articles/github-glossary/

n. **status**: Estado del archivo.
>https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository

o. **log**:  Son los registros de confirmación. 
>https://git-scm.com/docs/git-log

p. **checkout**: Actualiza los archivos para que coincidan con la versión del índice.
>https://git-scm.com/docs/git-checkout

q. **Rama / Branch**:  Sistemas de control de versiones. Existe la rama principal que es el master y las ramas que nacen de esta, donde se puede trabajar por separado.
>https://git-scm.com/book/es/v1/Ramificaciones-en-Git

r. **Etiqueta / Tag**: Referencia para marcar un punto en particular.
>https://www.atlassian.com/git/glossary/terminology


