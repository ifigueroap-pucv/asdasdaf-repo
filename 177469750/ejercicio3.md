a)**Branches**
    Mediante el comando branch -a podemos apreciar la existencia de 5 ramas en el repositorio.
    
    git branch -a
    * master
        remotes/origin/HEAD -> origin/master
        remotes/origin/master
        remotes/origin/megaramita1
        remotes/origin/ramita1
        remotes/origin/ramita2
        remotes/origin/taller1

b) **Tags**

    Mediante el comando tag -i podemos apreciar la existencia de 5 tags en el repositorio.
    
    $ git tag -i
    asdasdaf-tag
    final-final-ahora-si
    super-release
    tag1
    todas-las-tags-al-mismo-commit

c)  **Últimos 3 commits**

1.  entrega ejercico3 final Bastian Tobar Mori

    commit 622f2508777887bf1aaeacad50a8fc600ca2ed16
    Author: Cristian <cristianhu96@gmail.com>
    Date:   Wed Aug 9 11:04:42 2017 -0400

2.  Mis Datos

    commit 88e7e6ae216a4a3c312dd12c8431cc1d1d162274
    Author: bastiantobar.94@gmail <bastiantobar.94@gmail.com>
    Date:   Wed Aug 9 11:04:01 2017 -0400

3.  entrega glosario final Bastian Tobar Mori

    commit 33aaa62a6819da2c30ca2eadbdf1a127159adc65
    Author: bastiantobar.94@gmail <bastiantobar.94@gmail.com>
    Date:   Wed Aug 9 11:01:35 2017 -0400
    



