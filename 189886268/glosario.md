* Control de versiones (VC): Es la gesti�n de los diversos cambios que se realizan sobre los elementos de
alg�n producto o una configuraci�n del mismo.
[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)

* Control de versiones distribuido (VCD): Permite a muchos desarrolladores de sofguar trabajar en un proyecto com�n sin necesidad de compartir
una misma red.
[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

* Repositorio remoto y repositorio local: Los repositorios remotos son versiones de tu proyecto que se encuentran alojados
 en Internet o en alg�n punto de la red. El repositorio local son los archivos del proyecto alojados en el computador propio.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[Fuente2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* Working copy: Es una copia de un repositorio, hecha con el comando clone.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* Staging area: El �rea de preparaci�n es un archivo, generalmente ubicado en el directorio Git, 
que almacena informaci�n acerca de lo que ir� en la pr�xima versi�n.
[Fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

*Stage changes: Consiste en realizar cambios no permanentes en el repositorio, hasta hacer el commit.
[Fuente](https://githowto.com/staging_changes)

* Commit changes: Hacer que los cambios sean permanentes en el repositorio.
[Fuente](https://githowto.com/staging_changes)

* Commit: Almacena los cambios en el repositorio.
[Fuente](https://git-scm.com/docs/git-commit)

* Clone: Clona un repositorio en un nuevo directorio.
[Fuente](https://git-scm.com/docs/git-clone)

* Pull: Sacar desde e integrar con otro repositorio o una rama local.
[Fuente](https://git-scm.com/docs/git-pull)

* Push: Actualizar referencias remotas y sus objetos asociados.
[Fuente](https://git-scm.com/docs/git-push)

* Fetch: Descargar objetos y referencias desde otro repositorio.
[Fuente](https://git-scm.com/docs/git-fetch)

* Merge: Unir dos o m�s historias de desarrollo.
[Fuente](https://git-scm.com/docs/git-merge)

* Status: Mostrar el estado del directorio y del �rea de preparaci�n en los que se est� trabajando.
[Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

* Log: Mostrar los registros de los commit.
[Fuente](https://git-scm.com/docs/git-log)

* Checkout: Cambiar de rama o restaurar archivos del �rbol de trabajo.
[Fuente](https://git-scm.com/docs/git-checkout)

* Rama/Branch: Es un puntero movible de bajo peso hacia un commit.
[Fuente](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

* Etiqueta/Tag: Usualmente se usa para etiquetar versiones.
[Fuente](https://git-scm.com/book/en/v2/Git-Basics-Tagging)