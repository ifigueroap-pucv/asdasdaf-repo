# Cantidad y nombre de los branches.

  * master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/megaramita1
  remotes/origin/ramita1
  remotes/origin/ramita2

* hay 5 branches cuyos nombres son: 
* HEAD, master, megaramita1, ramita1 y ramita2

# Cantidad y nombre de las etiquetas.


* asdasdaf-tag
final-final-ahora-si
super-release
tag1
todas-las-tags-al-mismo-commit

* Hay 5 tags

# Nombre, mensaje y c�digo hash de los �ltimos 3 commits. Use el comando �git log� para obtener esta informaci�n.

* commit f0dc0a3a2bd838daad7af4f1849013a4aa27d5f4 (origin/master, origin/HEAD)
Author: 18916791-1 <michael.fernandez.o@mail.pucv.cl>
    Hola!

* commit c09f7571047220ac37b431184f818316b1d60785
Merge: 95a55b1 68bbe51
Author: Jose Toro <jose.toro.p@mail.pucv.cl>
    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

* commit 68bbe518dfb472c0107d7789fab044a25773f0c6
Author: Benjamin Cruz <benja.cruz3@gmail.com>
    A<C3><B1>adido datos.md