
#Glosario
* Control de versiones (VC):
Es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo,
de tal manera que se puedan recuperar las versiones anteriores especificas sin tener una cantidad voluminosa de archivos.
>https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

* Control de versiones distribuido (DVC):
Es un sistema en el que todos los desarroladores replican completamente el repositorio, si por algun motivo el servidor 
donde estaban almacenados los archicos muere cualquiera que haya estado colaborando con el repositorio puede restaurarlo.
Cada vez que se descargan los datos se hace una copia completa de los datos.
>https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

* Repositorio remoto y Repositorio local:
El repositorio remoto es aquel en que los datos estan alojados en un servidor
El repositorio local es aquel en el que los datos estan alojados en un computador en especifico 
>https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos
>https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

* Copia de trabajo / Working Copy:
Son los archivos locales modificados que a�n no hemos a�adido para preparar el commit.
>https://mydefinitionofdone.com

* �rea de Preparaci�n / Staging Area
Es un area de prueba en el cual se eligen uno a uno los archivos que luego se le haran commit.
>https://www.youtube.com/watch?v=t6GMcIoCD9Q

* Preparar Cambios / Stage Changes
Es un comando para a�adir un contenido a la proxima confirmacion.
>https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

* Confirmar cambios / Commit Changes
Es un comando que realiza los cambios que se hallan realizado en el "Area de Preparacion" y los traspasa generando un repositorio
al momento de ejecucion.
>https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio

* Commit
Es el registrar cambios en el repositorio local.
>https://www.quora.com/What-is-the-meaning-of-commit-and-stage-in-git

* clone
Clona un respoositorio en un directorio diferente.
>https://git-scm.com/docs/git-clone

* pull
Recupera todos los datos del proyecto remoto que no tengas todavia. Despues de esto se podr�n tener referencias a todas las ramas 
del repositorio remoto, que puedes unir o inspeccionar en cualquier momento. 
>https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

* push
Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo remota.
>https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git

* fetch
Hace que tu rama local este actualizada con la versi�n remota, mientras tambi�n actualiza tus otras ramas.
>https://git-scm.com/docs/git-pull
 
* merge
Es la fusion de dos o mas ramas.
>https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

* status
Muestra el estado del �rbol de trabajo.
>https://git-scm.com/docs/git-status

* log
Sirve para examinar la secuencia de commits (confirmaciones) que hemos realizado en la historia de un proyecto, 
para saber d�nde estamos y qu� estados hemos tenido a lo largo de la vida del repositorio. 
>https://www.desarrolloweb.com/articulos/git-log.html

* checkout
Copia archivos desde el stage al directorio de trabajo. Us� esto para descartar los cambios locales.
>https://marklodato.github.io/visual-git-guide/index-es.html

* Rama / Branch
Es la forma que tiene git de almacenar los datos mostrando la conectividad que existe entre un cambio y otro, mostrando de donde 
provienen.
>https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F

* Etiqueta / Tag
Usado para etiquetar puntos importantes en la hisotria, por ejemplo cuando se lanzo una versi�n.
>https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas