*Control de versiones(VC): Es un sistema que se guarda los registros de los cambios que se hicieron durante el tiempo, de modo que se pueda recuperar una version anterior. [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

*Control de versiones distribuido(DVC): Es un dvc, en donde los clientes descargan la ultima version de los archivos, como un respaldo de seguridad del servidor, entonces en caso de que se muera el servidor, cualquier repositorio de los clientes puede copiarse del servidor para restaurarlo [Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

*Repositorio remoto y Repositorio local: En el remoto, se guardan las versiones de tu proyecto en algun lugar de la red.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

*Copia de trabajo/Working Copy: Es una copia de un repositorio, hecha con el comando clone.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

*�rea de Preparaci�n/Staging: Area El �rea de preparaci�n es un archivo, generalmente ubicado en el directorio Git, que almacena informaci�n acerca de lo que ir� en la pr�xima versi�n.[Fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

*Preparar Cambios/Stage Changes: Consiste en realizar cambios no permanentes en el repositorio, hasta hacer el commit.[Fuente](https://githowto.com/staging_changes)

*Confirmar cambios/Commit Changes: Hacerlos permanentes en el repositorio.[Fuente](https://githowto.com/staging_changes)

*Commit: se refiere a la idea de hacer un conjunto de cambios, hacer que sean permantentes.[Fuente](https://es.wikipedia.org/wiki/Commit)

*clone: Se usa para obtener una copia de un repositorio existente.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

*pull: incorpora cambios desde el repositorio remoto dentro de la banda actual.[Fuente](https://git-scm.com/docs/git-pull)

*push: Actualiza los repositorios remotos usando los repositorios locales, enviando los objejot para conpletar los repositorios.[Fuente](https://git-scm.com/docs/git-push)

*fetch: Trae los cambios, pero los deja en un branch.[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

*merge: Trae las cosas de un brach a el branch local.[Fuente](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)

*status: Sirve para determinar el estado de tus archivos, para ver en que estado estan los archivos.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

*log: Sirve para aver las modificaciones que se han hecho.[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

*checkout: sirve para crear una nueva rama y poder saltar a ella automaticamente.[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

*Rama/Branch: una rama apunta a cada una de las confirmaciones de cambio.[Fuente](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

*Etiqueta/Tag: sirve como una rama firmada que siempre se mantiene igual.[Fuente](https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git)