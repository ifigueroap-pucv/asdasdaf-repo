# Ejercicio 3

* Cantidad y nombre de las branches: hay 6 ramas, las cuales son: 
 * *master
 * remotes/origin/HEAD -> origin/master
 * remotes/origin/master
 * remotes/origin/megaramita1
 * remotes/origin/ramita1
 * remotes/origin/ramita2

* Cantidad y nombre de las etiquetas: hay 5 etiquetas:
 * asdasdaf-tag
 * final-final-ahora-si
 * super-release
 * tag1
 * todas-las-tags-al-mismo-commit

* Los ultimos 3 commits son:

  - commit 95e13824570a6e0574d6365191005f0fc8d2a666 (HEAD -> master)
  - Author: Marcelo Chavez <marc3l0_0493@hotmail.com>
  - Date:   Fri Aug 4 22:46:25 2017 -0400

    >Glosario Marcelo Chavez

  - commit 20703d76a197916f46d67e7a88e63c83944c471d (origin/master, origin/HEAD)
  - Author: NADIA SOLEDAD TORRES LEYTON <nadiasoledadtorresleyton@MacBook-Pro-de-NAD         IA.local>
  - Date:   Fri Aug 4 14:17:34 2017 -0400

    >agregando datos

  - commit 9c97f6153d4892825f661a085fa5c4a55d865e78
  - Author: NADIA SOLEDAD TORRES LEYTON <nadiasoledadtorresleyton@MacBook-Pro-de-NAD         IA.local>
  - Date:   Fri Aug 4 13:58:15 2017 -0400

    >agregando ejercicio3

