>#Glosario

* __Control de versiones (VC):__ _Es un sistema que gestiona y registra los diversos cambios que se realizan en uno o m�s archivos con el pasar del tiempo. As� poder reutilizar o bien recuperar las versiones guardadas con anterioridad._

>[Referencia](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

* **Control de versiones distribuido (DVC):** _Son herramientas de control de versiones que toman un enfoque de peer-to-peer. Los clientes replican el c�digo base(repositorio). Si muere un servidor, cualquier cliente puede copiar el repositorio en el servidor para su restauraci�n._

>[Referencia 1][1], [Referencia 2][2]

 [1]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
 [2]: https://es.wikipedia.org/wiki/Control_de_versiones_distribuido

* **Repositorio remoto y Repositorio local:** _Los repositorios remotos son las versiones de nuestro proyecto, almacenado en Internet o en alg�n sitio de la red. Seg�n los permisos estos pueden ser s�lo de lectura o bien de lectura/escritura. Mientras el repositorio local es aquel que se encuentra en nuestro ordenador donde no es necesario la conexi�n a internet._

>[Referencia 1][3], [Referencia 2][4]

 [3]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
 [4]: https://blog.desdelinux.net/usar-repositorio-local-ubuntu/

* **Copia de trabajo/Working Copy:** _Es una copia de los archivos del proyecto (versi�n), almacenadas en un repositorio._

>[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* **�rea de Preparaci�n/Staging Area:** _O �ndice, es un archivo contenido en el directorio de Git, que almacena la informaci�n de lo va a ir en tu la pr�xima confirmaci�n._

>#Glosario

* __Control de versiones (VC):__ _Es un sistema que gestiona y registra los diversos cambios que se realizan en uno o m�s archivos con el pasar del tiempo. As� poder reutilizar o bien recuperar las versiones guardadas con anterioridad._

>[Referencia](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

* **Control de versiones distribuido (DVC):** _Son herramientas de control de versiones que toman un enfoque de peer-to-peer. Los clientes replican el c�digo base(repositorio). Si muere un servidor, cualquier cliente puede copiar el repositorio en el servidor para su restauraci�n._

>[Referencia 1][1], [Referencia 2][2]

 [1]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
 [2]: https://es.wikipedia.org/wiki/Control_de_versiones_distribuido

* **Repositorio remoto y Repositorio local:** _Los repositorios remotos son las versiones de nuestro proyecto, almacenado en Internet o en alg�n sitio de la red. Seg�n los permisos estos pueden ser s�lo de lectura o bien de lectura/escritura. Mientras el repositorio local es aquel que se encuentra en nuestro ordenador donde no es necesario la conexi�n a internet._

>[Referencia 1][3], [Referencia 2][4]

 [3]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
 [4]: https://blog.desdelinux.net/usar-repositorio-local-ubuntu/

* **Copia de trabajo/Working Copy:** _Es una copia de los archivos del proyecto (versi�n), almacenadas en un repositorio._

>[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* **�rea de Preparaci�n/Staging Area:** _O �ndice, es un archivo contenido en el directorio de Git, que almacena la informaci�n de lo va a ir en tu la pr�xima confirmaci�n._

>[Referencia](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

* **Preparar Cambios/Stage Changes:** _Presenta los cambios para los pr�ximos compromisos, Git sabe sobre el cambio, pero no es permanente en el repositorio._

>[Referencia](https://githowto.com/staging_changes)

* **Confirmar cambios/Commit Changes:** _Cuando el �rea de preparaci�n est� lista, se pueden confirmar los cambios. No se incluir� ning�n archivo que no se haya ejecutado desde la �ltima edici�n. La forma m�s f�cil de confirmar es escribiendo **git commit**_

>[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)

* **Commit:** _Registra los cambios en el repositorio. Almacena el contenido actual del �ndice en un nuevo **commit**, junto con un mensaje de registro del usuario que describe los cambios._

>[Referencia](https://git-scm.com/docs/git-commit)

* **Clone:** _Clona un repositorio en un nuevo directorio. Crea ramas de seguimiento remoto para cada rama en el repositorio clonado, adem�s crea y extrae una rama que bifurca de la rama actualmente activa del repositorio clonado._

>[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* **Pull:** _Es una combinaci�n de **fetch** y **merge** . Ya que, toma la informaci�n de la sucursal de un repositorio en l�nea y la fusiona con su copia local._

>[Referencia](https://github.com/GarageGames/Torque2D/wiki/Cloning-the-repo-and-working-with-Git)

* **Push:** _Toma todos sus cambios locales y los sube a la sucursal de un repositorio remoto._

>[Referencia](https://github.com/GarageGames/Torque2D/wiki/Cloning-the-repo-and-working-with-Git)

* **Fetch:** _Descarga el estado actual de un repositorio en l�nea sin modificar su repositorio local._

>[Referencia](https://github.com/GarageGames/Torque2D/wiki/Cloning-the-repo-and-working-with-Git)

* **Merge:** _Fusiona las modificaciones de otra rama en la rama de trabajo actual._

>[Referencia](https://github.com/GarageGames/Torque2D/wiki/Cloning-the-repo-and-working-with-Git)

* **Status:** _Muestra las rutas que tienen diferencias entre el archivo de �ndice y el **commit HEAD** actual._

>[Referencia](https://git-scm.com/docs/git-status)

* **Log:** _Muestra los registros de confirmaci�n. O sea de modificaci�n que se han llevado a cabo._

>[Referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)

* **Checkout:** _Cambia ramas o bien restaura y actualiza los archivos del �rbol de trabajo con el fin que coincidan con la versi�n del �ndice o del �rbol especificado._ 

>[Referencia](https://git-scm.com/docs/git-checkout)

* **Rama/Branch:** _Es un apuntador m�vil que apunta a una confirmaci�n.  Mediante la primera confirmaci�n se crea la rama principal. Esta rama va avanzando a medida de cada confirmaci�n._ 

>[Referencia](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

* **Etiqueta/Tag:** _Marca un estados importantes en la vida de los repositorios._ 

>[Referencia](https://desarrolloweb.com/articulos/especificar-versiones-git-tag.html)
