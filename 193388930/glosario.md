1. Control de Versiones (VC) : El control de versiones es para tener un historial de los cambios que se han hecho.

2. Control de Versiones Distribuido (DVC) : El control de versiones distribuidos es para que varios desarrolladores a la vez modifiquen un mismo proyecto en com�n.

3. Repositorio Remoto y Repositorio Local : El repositorio remoto es una version de nuestro proyecto subido a la red, el local es el que tenemos nuestros ficheros.

4. Copia de Trabajo / Working Copy : Es cuando copiamos con GIT nuestro proyecto.

5. �rea de Preparaci�n / Staging Area : Es donde se determina el proximo cambio en el proyecto.

6. Stage Changes / Preparar Cambios : Es basicamente cuando uno usa el comando git add, prepara un cambio proximo en el proyecto.

7. Confirmar Cambios/Commit Changes : Es cuando uno usa el git commit y confirma los cambios realizados en el proyecto.

8. Commit : Sirve para confirmar los cambios hechos en el proyecto y a su vez comentar el porque fueron hechos estos cambios.

9. Clone : Sirve para "clonar" desde el repositorio remoto a un repositorio local.

10. Pull : Sirve para actualizar el repositorio local.

11. Push : Sirve para subir los cambios del repositorio local al remoto.

12. Fetch: Trae los cambios hechos pero los deja en otra rama.

13. Merge : Trae los cambios dejados en otra rama, a la rama principal.

14. Status : Sirve para ver los archivos listos para hacer commit.

15. Log : sirve para ver los ultimos commit hechos.

16. Checkout : es para ir de una rama a otra rama.

17. Rama / Branch : Es un tipo de apuntador a los cambios realizador por un usuario.

18. Etiqueta/Tag : Es como un tipo de rama que nunca cambia en el tiempo. Siempre apunta a lo mismo.

LINKS

https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
http://michelletorres.mx/como-conectar-un-repositorio-local-con-un-repositorio-remoto-de-github/
https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area
https://github.com/susannalles/MinimalEditions/wiki/Lista-Comandos-Git
https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Obtener-y-Crear-Proyectos
https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Compartir-y-Actualizar-Proyectos