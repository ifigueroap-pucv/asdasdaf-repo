a. Control de versiones (VC)=Es una combinacion de tecnologias y practicas que permiten seguir y controlar los cambios realizados en los ficheros del proyecto, es decir en la documentacion , en el codigo fuente y en las paginas web.
	http://producingoss.com/es/vc.html	
b. Control de versiones distribuido (DVC)=Arquitectura de almacenamiento del control de versiones en el cual cada usuario tiene su propio repositorio.
	https://es.wikipedia.org/wiki/Control_de_versiones

c. Repositorio remoto y Repositorio local=
	-Los repositorios remotos son versiones de tu proyecto que se encuentran en internet o en tu red. Puedes tener varios , los cuales pueden ser solo de lectura , o de lectura/escritura , esto segun los permisos correspondientes.
	https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
        -Repositorio local es donde se almacena el VC en un equipo local.
	https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
d. Copia de trabajo / Working Copy=Es la copia de un repositorio existente.
	https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

e. �rea de Preparaci�n / Staging Area= Es un archivo, generalmente ubicado en el directorio Git, 
	que almacena informaci�n acerca de lo que ir� en la pr�xima versi�n.
	https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
f. Preparar Cambios / Stage Changes=Es la preparaci�n de los cambios realizacios en un archivo para confirmalos.
	https://git-scm.com/about/staging-area
g. Confirmar cambios / Commit Changes=Es la confirmaci�n de cambios previamente realizados, para que estos sean enviados al repositorio.
	https://git-scm.com/docs/git-commit
h. Commit=El comando commit es utilizado para cambiar a la cabecera. 
	https://www.hostinger.es/tutoriales/comandos-de-git
i. clone=Permite obtener una copia de un repositorio Git existente , cada version de cada archivo de la historia del proyecto se descarga cuando ejecutamos el git clone.
	https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git
j. pull=Acci�n en Git que obtione archivos desde un repositorio remoto y los traslada al repositorio actual para actualizarlo.
 	https://git-scm.com/docs/git-pull
k. push=Envia los cambios que se han realizado en la rama principal de los repertorios remotos  que estan enlazado con el directorio que se trabaja.
	https://www.hostinger.es/tutoriales/comandos-de-git
l. fetch=Acci�n en Git para descargar archivos desde un repositorio.
	https://git-scm.com/docs/git-fetch
m. merge=Este comando es utilizado para fusionar una rama con otra rama activa.
	https://www.hostinger.es/tutoriales/comandos-de-git
n. status=Acci�n en Git que muestra el estado del directorio actual.
	https://git-scm.com/docs/git-status

o. log=Ejecutar este comando muestra una lista de commits en una rama junto con todos los detalles.
	https://www.hostinger.es/tutoriales/comandos-de-git
p. checkout=Es utilizado para cambiar ramas o para crear ramas .
	https://www.hostinger.es/tutoriales/comandos-de-git
q. Rama / Branch=Este comando es utilizado para listar , crear o borrar ramas.
	https://www.hostinger.es/tutoriales/comandos-de-git
r. Etiqueta / Tag=Es la identificaci�n dada usualmente a las versiones.
	https://git-scm.com/docs/git-tag
