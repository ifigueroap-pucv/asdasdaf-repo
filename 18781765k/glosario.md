a. Control de versiones (VC): es una combinaci�n de tecnolog�as y practicas para seguir y controlar los cambios realizados en los ficheros del proyecto, 
en particular en el c�digo fuente, en la documentaci�n y en las p�ginas web. (http://producingoss.com/es/vc.html).

b. Control de versiones distribuido (DVC): En programaci�n inform�tica, el control de versiones distribuido permite a muchos desarrolladores 
de software trabajar en un proyecto com�n sin necesidad de compartir una misma red.Las revisiones son almacenadas en un sistema de control 
de versiones distribuido (https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

c. Repositorio remoto y Repositorio local: Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en 
alg�n punto de la red. Puedes tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que tengas.

d. Copia de trabajo / Working Copy: es el branch seleccionado por el para trabajar en el, el cual es llamado copia actual.

e. �rea de Preparaci�n / Staging Area: es un archivo, generalmente contenido en el directorio de Git, que almacena informaci�n sobre 
lo que pasar� al pr�ximo commit. A veces se le conoce como el "�ndice", pero tambi�n es com�n referirse a �l como el �rea de clasificaci�n

f. Preparar Cambios / Stage Changes:  Esto significa que git sabe sobre el cambio, pero no es permanente en el repositorio. 
El pr�ximo commit incluir� los cambios realizados. Si decide no confirmar el cambio, el comando de estado le recordar� que puede usar 
el git resetcomando para deshabilitar estos cambios.

g. Confirmar cambios / Commit Changes:  El cambio confirmado est� ahora en su repositorio local, pero a�n as� debe ser enviado al repositorio remoto.

h. Commit:  Subida de archivos a tu repositorio local, a diferencia de en otros sistemas como SVN con el commit no se propaga el cambio.

i. clone: Para clonar el repositorio de GitHub.

j. pull: baja los cambios del repositorio remoteo a nuestra copia en 

k. push: Envia los cambios locales comiteados al resto de nodos

l. fetch: Descarga el contenido del servidor.

m. merge Mezcla la rama actual con otra.

n. status: Comprueba el estado de repositorio actual

o. log: Puedes ver el historial de cambios

p. checkout: Para cambiar de un branch a otro. As� por ejemplo, si estamos en "master" y queremos cambiarnos a un branch llamado "classwork",
 haremos 'git checkout classwork'.

q. Rama / Branch: Las ramas simplememte son punteros a distintos snapshots.

r. Etiqueta / Tag: Un tag (una etiqueta) apunta a un cierto punto en el tiempo en un branch espec�fico. Con un tag, es posible tener un punto con un tiempo 
al cual siempre sea posible revertir el c�digo, por ejemplo, ponerle el tag "testing" al c�digo 25.01.2009. Crear, listar, eliminar o verificar un objeto 
de etiqueta firmado con GPG.