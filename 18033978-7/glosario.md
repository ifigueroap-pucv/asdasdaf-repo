##Glosario Ing. Web 

1. **Control de versiones:**
Un sistema de control de versiones es un sistema que nos permite guardar �copias� del estado de tu proyecto en ese momento, 
otorgando la capacidad de recuperar ese estado en el momento que se estime conveniente. 
Trata principalmente de poder guardar estados de un proyecto mientras trabajas en �l, y si en lg�n momento se necesita volver a un estado
anterior, por ejemplo por un error, se puede volver a utilizar una versi�n anterior.
[link de referencia](https://hipertextual.com)

2. **Control de versiones distribuido:**
El control de versiones distribuido brinda la facilidad  de trabajar a  muchos desarrolladores de software  
en un proyecto com�n sin necesidad de compartir una misma red.
Las revisiones son guardadas en un sistema de control de versiones distribuido.
[link de referencia](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

3. **Repositorio remoto y Repositorio local:**
Los repositorios remotos son versiones de un proyecto que se encuentran alojados en Internet o en alg�n punto de la red. 
Se pueden tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que se otorgen.
Colaborar con otros implica gestionar estos repositorios remotos, y mandar (push) y recibir (pull) datos de ellos cuando necesites compartir cosas
Los repositorios locales, son versiones de un proyecto, al igual que los repositorios remotos pero que se encuentran el equipo local, por ejemplo cuando
descargas la versi�n mas actual y empiezas a modificarla sin subirla a la red.
[link de referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

4. **Copia de trabajo:**
Una copia de trabajo es una representaci�n de un archivo original, pero que al realizarle cambios, no fecta el archivo original
a menos que se le indique explicitamente. Tiene varias ventajas, ya que varia personas pueden estar trabajando en las copias
pero sin modificar el original hasta tener una version final.
[link de referencia](https://wiki.eclipse.org/FAQ_What_is_a_working_copy%3F)

5. **Area de preparacion:**
Area de preparaci�n o staging area es un archivo generalmente contenido en el directorio de git que guarda informaci�n sobre que
ir� en nuestro pr�ximo cambio. A veces es referida como el �ndice, pero es mas com�n referirse a ella como �rea de preparaci�n o
staging area
[link de referencia](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

6. **Preparar cambios:**
Arreglar un archivo es simplemente prepararlo para un cambio. 
Git, con su indice permite al usuario cambiar solo algunas caracter�sticas de los cambios que uno ha realizado desde el �ltimo cambio 
Por ejemplo, si uno est� trabajando en un proyecto y tiene unas caracter�sticas terminadas y otras que aun le falta para terminar,
gir permite guardar las caracter�sticas terminadas y dejar las otras listas para ser modificadas cuando se desee continuar sin afectar las
que ya estan terminadas.
[link de referencia](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

7. **Confirmar cambios:**
Cuando se tiene un repositorio git y una copia de los archivos de ese repositorio, y se desea guardar el estado actual de las copias de los archivos
en el repositorio, git toma los archivos marcados como modificados y los guarda.
[link de referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

8. **Commit:**
Almacena el contenido actual del �ndice junto con un mensaje que describe los cambios.
[link de referencia](https://git-scm.com/docs/git-commit)

9. **Clone:**
Clona un repositorio en un directorio creado recientemente, crea ramas de seguimiento remoto para 
cada rama en el repositorio clonado y crea y extrae una 
rama inicial que se bifurca de la rama actualmente activa del repositorio clonado.
[link de referencia](https://git-scm.com/docs/git-clone)

10. **Pull:**
Incorpora cambios desde un repositorio remoto a la rama actual.
[link de referencia](https://git-scm.com/docs/git-pull)

11. **Push:**
Actualiza las referencias remotas usando referencias locales, mientras env�a objetos necesarios para completar las referencias dadas
[link de referencia](https://git-scm.com/docs/git-push)

12. **Fetch:**
Descarga objetos y refs desde otro repositorio
[link de referencia](https://git-scm.com/docs/git-fetch)

13. **Merge:**
Une 2 o mas historias de desarrollo,
incorpora cambios en la rama actual.
[link de referencia](https://git-scm.com/docs/git-merge)

14. **Status:**
Muestra el estado del �rbol de trabajo
Muestra las rutas que tienen diferencias entre el archivo de �ndice y el commit HEAD actual, las rutas que tienen diferencias
entre el �rbol de trabajo y el archivo de �ndice y las rutas en el �rbol de trabajo que no son trackeadas por git.
[link de referencia](https://git-scm.com/docs/git-status)

15. **Log:**
Muestra los registros.
[link de referencia](https://git-scm.com/docs/git-log)

16. **Checkout:**
Cambia ramas o restaura archivos de �rbol de trabajo
Actualiza los archivos del �rbol de trabajo para que coincidan con la versi�n del �ndice o del �rbol especificado. 
Si no se da ninguna ruta, git checkout tambi�n actualizar� HEAD para establecer la rama especificada como la rama actual.
[link de referencia](https://git-scm.com/docs/git-checkout)

17. **Branch:**
Lista, crea o elimina ramas
[link de referencia](https://git-scm.com/docs/git-branch)

18. **Tag:**
Se utiliza para referenciar un objeto con un nombre asignado.
[link de referencia](https://git-scm.com/docs/gitglossary#def_ref)