##### Glosario ######

** Control de versiones (VC) : es una revision a un producto o proyecto que esta siendo desarrollado el cual necesita llevar una gestion de su avance respecto a las modificaciones realizadas en el. Ayuda a gestionar distintas versiones de los archivos que tiene un proyecto.
** Control de versiones distribuido (DVC) : Las versiones del proyecto pueden ser trabajadas en un espacio en comun el mismo proyecto, teniendo una actualizacion constante sobre los cambios realizados por otros usuarios que se encuentren dearrollando un producto.
** Repositorio remoto y Repositorio localCopia de trabajo / Working Copy : Es el repositorio que se encuentra "en linea" desde el cual otros usuarios que tengan acceso a este repositorio podran agregar y/o modificar el cotenido de este.
** Área de Preparación / Staging Area :  es una lista de archivos en un directorio de Git en el cual se almacena la información de lo va a ir en la próxima confirmación que el usuario local determine.
** Preparar Cambios /Stage Changes : Presenta los cambios para los próximos compromisos, Git sabe sobre el cambio, pero no es permanente en el repositorio.
** Confirmar cambios / Commit Changes : Se realiza cuando el area de preparacion esta lista para confirmar los cambios realizados. Con esto no se incluiran los archivos que hayan sido ejecutados desde la ultima edicion.
** Commit : Se usa para hacer un registro de los cambios realizados en el repositorio. Junto a esto se añaden datos de los archivos y comentarios.
** clone : Clona un directorio o repositorio a uno nuevo. Junto con esto crea las ramas y divisioes necesarias para la integracion de futuras modificciones.
** pull : Toma parte de la informacion de un repositorio que se encuentra en linea y luego lo fusioona con el repositorio local.
** push : Toma los cambios locaes realizados y estos los sube al repositotio en linea o al repositorio remoto.
** fetch : Descarga el estado actual de un determinado repositotio remoto sin modificar los datos del repositorio local.
** merge : Fusiona las modificaciones de otra rama con la rama de trabajo actual del repositorio.
** status : Señala las rutas de los directorios que tienen diferencias entre el archivo indice y el commit HEAD Actual
** log : Muestra la modificaciones que se han han realizado en los directorios. Muestra los registros de confirmacion.
** checkout : Cambia parte de los branches en caso que no coincida con la version indice o un determinado arbol.
** Rama / Branch :  Es una forma de definir una confirmacion a un directorio especifico. Con lo anterior es posible poder acceder a directorios secundarios llegando a una organizacion y estructura de los directorios.
** Etiqueta / Tag : deja registro de los estados de cambios importantes realizados en los repositorios.


** Bibliografias
** http://michelletorres.mx/como-conectar-un-repositorio-local-con-un-repositorio-remoto-de-github/
** https://github.com/susannalles/MinimalEditions/wiki/Lista-Comandos-Git
** https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Obtener-y-Crear-Proyectos
** https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
** https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area
** https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Compartir-y-Actualizar-Proyectos
** Videos proporcionado por el profesor como usar Git