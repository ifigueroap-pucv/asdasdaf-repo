Parte 1:

a. Control de versiones (VC): Es un sistema que sirve para controlar, administrar, modificar, actualizar, archivos que sirven para desarrollar un software.

b. Control de versiones distribuido (DVC): Este es un sistema destribuido, que permite a un grupo de proyecto trabajar, sin necesidad de compartir la misma red.
c. Repositorio remoto y Repositorio local: Los repositorios remotos son versiones de tu proyecto que estan alojados en cualquier red, cada uno de ellos tendra permisos
					   de solo lectura o escritura y lectura, asi pueden colaborar todos los integrantes que forman parte del proyecto. Y el repositorio
				           local opera con archivos y recursos que estan ubicados localmente, que sirven para la mayoria de operaciones de git y no necesita
					   de otro dispositivo conectado a la misma red.
d. Copia de trabajo / Working Copy: La Copia de trabajo sirve para actualizar los archivos que estan siendo utilizados para el proyecto, usando el repositorio remoto y local.
e. �rea de Preparaci�n / Staging Area: Git contiene comandos que se utilizan para realizar una serie de cambios en los archivos, asi facilitando el trabajo para el desarrollo del software.
f. Preparar Cambios / Stage Changes: Hay una preparaci�n interactiva amigable, que es un script que muestra los archivos que pueden pasar al area de preparacion y tambien los
			             que se estan preparando.
g. Confirmar cambios / Commit Changes: A medida que vas avanzando en el proyecto, vas confirmando cambios y git inmediatamente lo va actualizando.
h. Commit: Cuando se confirman los cambios, se puede enviar un mensaje, para aclarar la modificaci�n, asi siendo m�s f�cil para todas las personas que participan en el proyecto.
i. clone: Hace una copia exacta de todos los archivos en el repositorio remoto.
j. pull: actualiza el repositorio local copiando todo lo del repositorio remoto.
k. push: Permite enviar los cambios realizados localmente a un repositorio remoto para 
	que otros puedan acceder a estos.
l. fetch: Descarga el estado actual de un repositorio en l�nea sin modificar su repositorio local.
m. merge:Fusiona las modificaciones de otra rama en la rama de trabajo actual.
n. status: Sirve para comprobar el estado de tus archivos.
o. log: REvisa el historial de confirmaciones, ver modificaciones que se hallan llevado a cabo.
p. checkout: Cambia ramas o bien restaura y actualiza los archivos del �rbol de trabajo con el fin 
	que coincidan con la versi�n del �ndice o del �rbol especificado.
q. Branch: Una rama es una versi�n paralela de un repositorio pero contenida dentro de este 
	la cual nos permite realizar cambios sin alterar la rama principal, al terminar nuestras modificaciones 
	es posible realizar un merge con la rama principal.
r. Tag: Marca un estados importantes en la vida de los repositorios.
	



















