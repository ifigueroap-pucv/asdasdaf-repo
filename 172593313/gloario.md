{\rtf1\ansi\ansicpg1252\cocoartf1504\cocoasubrtf830
{\fonttbl\f0\froman\fcharset0 Times-Roman;\f1\fswiss\fcharset0 ArialMT;}
{\colortbl;\red255\green255\blue255;\red0\green0\blue0;\red25\green25\blue25;\red255\green255\blue255;
\red255\green255\blue0;}
{\*\expandedcolortbl;;\cssrgb\c0\c0\c0;\cssrgb\c12941\c12941\c12941;\cssrgb\c100000\c100000\c100000;
\csgenericrgb\c100000\c100000\c0;}
\margl1440\margr1440\vieww15240\viewh9980\viewkind0
\deftab720
\pard\pardeftab720\sl280\partightenfactor0

\f0\fs24 \cf2 \expnd0\expndtw0\kerning0
\
a. Control de versiones (VC):  Es un sistema que registra los cambios realizados en un archivo guardando sus versiones a lo largo del tiempo, de modo que sea m\'e1s sencillo recuperar alguna versi\'f3n. *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones"}}{\fldrslt https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones}}*\
\
b. Control de versiones distribuido (DVC):  Es sistema que permite una copia completa del repositorio si es que un servidor est\'e1 colaborado a trav\'e9s de el. *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones"}}{\fldrslt https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones}}*\
 \
c. Repositorio remoto y Repositorio local:  El repositorio Remoto son versiones de proyecto que se encuentran alojados en internet o alg\'fan punto en la red. Repositorio Local, es aquel que est\'e1n almacenadas sus versiones en el mismo equipo que se cre\'f3. *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos"}}{\fldrslt https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos}}* \
\
d. Copia de trabajo / Working Copy:  Es una copia actual del trabajo que se est\'e1 realizando. *{\field{\*\fldinst{HYPERLINK "http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html#creando-el-repositorio"}}{\fldrslt http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html#creando-el-repositorio}}*\
\
e. \'c1rea de Preparaci\'f3n / Staging Area:  \cf3 \cb4 El \'e1rea trabajo es un archivo, generalmente contenido en el directorio de Git, que almacena informaci\'f3n sobre lo que pasar\'e1 al pr\'f3ximo commit. A veces se le conoce como el "\'edndice", pero tambi\'e9n es com\'fan referirse a \'e9l como el \'e1rea de clasificaci\'f3n.  *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/book/en/v2/Getting-Started-Git-Basics"}}{\fldrslt https://git-scm.com/book/en/v2/Getting-Started-Git-Basics}}*
\f1\fs32 \

\f0\fs24 \cf2 \cb1  \
\cb5 f. Preparar Cambios / Stage Changes: \cb1 \
 \
\cb5 g. Confirmar cambios / Commit Changes:  \cb1 \
 \
h. Commit:  Designar un conjuntos de cambios \'93tentativos\'94 de una forma permanente. *{\field{\*\fldinst{HYPERLINK "https://es.wikipedia.org/wiki/Commit"}}{\fldrslt https://es.wikipedia.org/wiki/Commit}}*\
 \
i. clone:  Clona un repositorio en un directorio creado recientemente. *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-clone"}}{\fldrslt https://git-scm.com/docs/git-clone}}*\
 \
j. pull:  Incorpora cambios desde un repositorio remoto a uno local o actual *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-pull"}}{\fldrslt https://git-scm.com/docs/git-pull}}*\
 \
\pard\tx10208\pardeftab720\sl280\partightenfactor0
\cf2 k. push:  actualiza los sistemas de archivos resientes(refs) de forma remota usando refs locales, mientras env\'eda objetos necesarios para las refs dadas *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-push"}}{\fldrslt https://git-scm.com/docs/git-push}}*\
\pard\pardeftab720\sl280\partightenfactor0
\cf2  \
l. Fetch:  Descarga objetos y sistemas de archivos resientes desde otros repositorios *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-fetch"}}{\fldrslt https://git-scm.com/docs/git-fetch}}*\
 \
m. merge: Une dos o m\'e1s historias de desarrollo *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-merge"}}{\fldrslt https://git-scm.com/docs/git-merge}}*\
 \
n. status: Muestra el estado del \'e1rbol de trabajo. *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-status"}}{\fldrslt https://git-scm.com/docs/git-status}}*\
 \
o. log:  Muestra los registros de confirmaci\'f3n *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-log"}}{\fldrslt https://git-scm.com/docs/git-log}}*\
 \
p. checkout:  Cambia ramas o restaura archivos del \'e1rbol de trabajo *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-checkout"}}{\fldrslt https://git-scm.com/docs/git-checkout}}* \
 \
q. Rama / Branch:  Sirve para enumerar, crear o eliminar ramas *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-branch"}}{\fldrslt https://git-scm.com/docs/git-branch}}*\
 \
r. Etiqueta / Tag:  Sirve para crear, enumerar, eliminar o verificar un objeto designado como GPG *{\field{\*\fldinst{HYPERLINK "https://git-scm.com/docs/git-tag"}}{\fldrslt https://git-scm.com/docs/git-tag}}*\
\
\
\
}