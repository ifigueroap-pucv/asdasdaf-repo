a. Cantidad y nombre de los branches:

	* master
 	 remotes/origin/HEAD -> origin/master
 	 remotes/origin/master
 	 remotes/origin/megaramita1
 	 remotes/origin/ramita1
 	 remotes/origin/ramita2

	* Se observan que exsten 5 branches


b. Cantidad y nombre de las etiquetas:

	asdasdaf-tag
	final-final-ahora-si
	super-release
	tag1
	todas-las-tags-al-mismo-commit
	
	Se observa que existen 5 etiquetas

c. Nombre, mensaje y código hash de los últimos 3 commits:
	commit 44f21d56565eb4a634387140b78cc42cc3a4038b
	Author: Gonzalo Serrano <gserranosalas@gmail.com>
	Date:   Wed Aug 9 11:24:41 2017 -0400

	    Subo mi archivo Glosario.

	commit cb5bd9c8c5a353ab3d66cbc1a70cc6f0c35fc3dc
	Merge: 325f5cb 660aa2f
	Author: bastiantobar.94@gmail <bastiantobar.94@gmail.com>
	Date:   Wed Aug 9 11:09:30 2017 -0400

	    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

	commit 325f5cb178004b3ed94d36cf6d9bbb8912c83a79
	Merge: 4c0079e 087eed7
	Author: bastiantobar.94@gmail <bastiantobar.94@gmail.com>
	Date:   Wed Aug 9 11:06:18 2017 -0400

	    Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

