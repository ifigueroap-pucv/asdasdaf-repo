##Glosario

+ **a. Control de versiones (VC):** Es un software que ayuda a los desarrolladores a trabajar de forma simultánea sin sobreescribir los cambios del otro y mantener un historial de todas las versiones. [Fuente](http://www.w3ii.com/es/git/git_basic_concepts.html)

+ **b. Control de versiones distribuido (DVC):** Resuelve el problema de los sistemas centralizados. Cada cliente refleja totalmente el repositorio, si el server se cae cualquier cliente puede copiar denuevo al servidor para restaurarlo. Cada salida es un respaldo del repositorio. [Fuente](http://www.w3ii.com/es/git/git_basic_concepts.html)

+ **c. Repositorio remoto y Repositorio local:** Los repositorios remotos son lugares en internet donde se almacenan varios versiones de un proyecto para colaborar con otros. El repositorio local también contiene versiones de tu proyecto, pero se almacenan en tu propio pc. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

+ **d. Copia de trabajo / ​Working Copy:** Es una copia total de un repositorio clonado pero localizada en mi equipo, en la que puedo trabajar para luego hacer "push" a un repositorio remoto. [Fuente](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)

+ **e. Área de Preparación / ​Staging Area:** Es una de las secciones principales de un proyecto Git donde van los archivos que han sido modificados y que quiero incluir en mi próximo "committ". [Fuente](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

+ **f. Preparar Cambios / ​Stage Changes:** Es cuando se le deja saber a git sobre los cambio, pero aún no son permanente en el repositorio, serán incluidos en el próximo "committ". [Fuente](https://githowto.com/staging_changes)

+ **g. Confirmar cambios / ​Commit Changes:** Es la acción de confirmar los cambios que se prepararon en el Stage Changes preparandolos para el área de preparación. [Fuente](http://gitready.com/beginner/2009/01/18/the-staging-area.html)

+ **h. Commit:** Es el comando que permnite confirmar y trasladar los cambios del Staging Area hacia el repositorio. También puede ser utilizado, con los argumentos adecuados, para trasladar archivos directamente, ignorando el Staging Area. [Fuente](https://git-scm.com/docs/git-commit)

+ **i. Clone:** Comando para crear una copia de un repositorio en un directorio local. [Fuente](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone)

+ **j. Pull:** Comando que permite trasladar todos los cambios existentes en un repositorio remoto al Working copy. Utiliza merge para actualizar los archivos del Working copy. [Fuente](https://git-scm.com/docs/git-pull)

+ **k. Push:** Comando que permite actualizar un repositorio remoto (traslado de cambios desde un repositorio local). [Fuente](https://git-scm.com/docs/git-push)

+ **l. Fetch:** Comando que permite trasladar los cambios existentes en un repositorio remoto al repositorio local. [Fuente](https://git-scm.com/docs/git-fetch)

+ **m. Merge:** Comando para fusionar dos ramificaciones, es decir, actualiza los archivos de la rama actual con los cambios existentes en la ramificación que se específica. [Fuente](https://git-scm.com/docs/git-merge)

+ **n. Status:** Comando para mostrar los archivos que esten en el Staging Area esperando un commit, los que aun no son agregados a dicha area, y los archivos que no estan en el repositorio. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

+ **o. Log:** Comando que muestran los registros que explican los cambios realizados a un archivo, los cuales son creados al realizar un commit. [Fuente](https://www.atlassian.com/git/tutorials/inspecting-a-repository)

+ **p. Checkout:** Comando que permite trasladar el Working copy a otra ramificación, o bien, a algún estado anterior registrado en los commit. [Fuente](https://www.atlassian.com/git/tutorials/resetting-checking-out-and-reverting)

+ **q. Rama / Branch:** Una rama básicamente consiste en una carpeta dentro del Working copy, que se utiliza para separar los trabajos que han de realizarse sobre un archivo. [Fuente](https://betterexplained.com/articles/a-visual-guide-to-version-control/)

+ **r. Etiqueta / Tag:** La etiqueta es un nombre asignado a un cambio registrado en un archivo, utilizado para ser identificado. Usualmente sirve para representar la versión de un trabajo. [Fuente](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
