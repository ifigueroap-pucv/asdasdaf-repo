##Glosario:

* Control de versiones (VC): Se denomia a la gesti�n de los cambios realizados sobre un archivo o conjunto de estos.
[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)

* Control de versiones distribuido (DVC): Arquitectura de almacenamiento del control de versiones en el cual cada usuario tiene su propio repositorio.
[Fuente](https://es.wikipedia.org/wiki/Control_de_versiones)

* Repositorio remoto y Repositorio local: Un repositorio local es donde se almacena el VC en un equipo local, en cambio el repositorio remoto reside en alg�n
ludar de internet.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

* Copia de trabajo / Working Copy: Es la copia de un repositorio existente.
[Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

* �rea de Preparaci�n / Staging Area: Es un archivo, generalmente ubicado en el directorio Git, 
que almacena informaci�n acerca de lo que ir� en la pr�xima versi�n, en el flujo de trabajo este se encuentra entre el directorio de trabajo y el repositorio.
[Fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

* Preparar Cambios / Stage Changes:Es la preparaci�n de los cambios realizacios en un archivo para confirmalos.
[Fuente](https://git-scm.com/about/staging-area)

* Confirmar cambios / Commit Changes: Es la confirmaci�n de cambios previamente realizados, para que estos sean enviados al repositorio.
[Fuente](https://git-scm.com/docs/git-commit)

* Commit: Acci�n en Git que confirma los cambios, almacenandolos en el repositorio.
[Fuente](https://git-scm.com/docs/git-commit)

* Clone: Acci�n en Git la cual copia un repositorio en un nuevo directorio.
[Fuente](https://git-scm.com/docs/git-clone)

* Pull: Acci�n en Git que obtione archivos desde un repositorio remoto y los traslada al repositorio actual para actualizarlo.
[Fuente](https://git-scm.com/docs/git-pull)

* Push: Acci�n en Git que actualiza referencias remotas con respecto a las del repositorio local.
[Fuente](https://git-scm.com/docs/git-push)

* Fetch: Acci�n en Git para descargar archivos desde un repositorio.
[Fuente](https://git-scm.com/docs/git-fetch)

* Merge: Acci�n en Git para unir 2 o m�s versiones de un archivo.
[Fuente](https://git-scm.com/docs/git-merge)

* Status: Acci�n en Git que muestra el estado del directorio actual.
[Fuente](https://git-scm.com/docs/git-status)

* log: Acci�n en Git que muestra el registro de commits.
[Fuente](https://git-scm.com/docs/git-log)

* Checkout: Acci�n en Git que permite cambiar de rama o restaurar una de una versi�n anterior.
[Fuente](https://git-scm.com/docs/git-checkout)

* Rama / Branch: Directorios o sub-directorios del �rea de trabajo (Punteros hacia los commits).
[Fuente](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

* Etiqueta / Tag: Es la identificaci�n dada usualmente a las versiones.
[Fuente](https://git-scm.com/docs/git-tag)
