

#Glosario

* Control de versiones (VC): El control de versiones es un sistema el cual permite el almacenamiento de cambios a un archivo o un grupo de estos, con esto se logra un registro de versiones para el futuro.

> https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control

* Control de versiones distribuido (DVC): En este tipo de control los clientes no solo utilizan el ultimo lote de archivos, sino que reflejan el repositorio copletamente.

> https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control

* Repositorio remoto y repositorio local:  El repositorio local guarda la version del proyecto en el ordenador, el remoto guarda la version en la red

> https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes

* Copia de trabajo / Working Copy: Es un clon del repositoria, en el cual cada usuario trabaja local mente.

> https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms

* Area de Preparacion / Staging Area: Es un archivo el cual contiene informacion de los cambios realizados.

> https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

* Preparar Cambios / Stage Changes: Es la accion de enviar los cambios a la Area de preparacion, utilizando el comando add.

>https://www.atlassian.com/git/tutorials/saving-changes

* Confirmar cambios / Commit Changes: Confirma el almacenaje de los cambios en la Area de Preparacion y los envia al repositorio.

>https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository

* Commit: Toma los archivos en el Area de preparacion y los almacena en el repositorio.

> https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

* clone: Crea una copia exacta de un repositorio existente.

>https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository

* pull: Incorpora los cambios de un repositorio remoto a la rama actual.

> https://git-scm.com/docs/git-pull

* push: Envia el proyecto a un repositorio remoto.

> https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes

* fetch: Obtiene los datos del proyecto desde un repositiorio remoto.

>https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes

* merge: Permite fusionar distintas versiones de cambios a la rama actual.

>https://git-scm.com/docs/git-merge

* status: Permite ver el estado del directorio de trabajo y la area de preparacion.

>https://www.atlassian.com/git/tutorials/inspecting-a-repository

* log: Permite ver el historia de cambios del proyecto.

> https://www.atlassian.com/git/tutorials/inspecting-a-repository

* checkout: Actualiza el arbol de trabajo para que este concida con el indice.

>https://git-scm.com/docs/git-checkout

* Rama / Branch: Representan distintas lineas de desarrollo en un proyecto.

>https://www.atlassian.com/git/tutorials/using-branches

* Etiqueta / Tag: Permite equiquetar puntos especificos en el desarrollo,

>https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas


