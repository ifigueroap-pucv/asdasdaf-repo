Taller 0
===================
1. Control de versiones: Es un sistema que permite registrar los cambios hechos sobre el c�digo fuente de una aplicaci�n.
2. Control de versiones distribuido: Es uno de los distintos tipos de sistemas de control de versiones, no requiere un servidor central que almacene el c�digo, 
aunque tampoco es excluyente. Al ser distribuido, cada desarrollador tiene una copia completa del proyecto en su equipo y al hacer alg�n cambio este se propaga en los equipos 
de los desarrolladores cuando estos hagan un git pull.
3. Repositorio remoto y repositorio local: Los repositorios remotos son versiones del proyectos que est�n almacenados de forma remota, como por ejemplo en un servidor web.
Los repositorios locales son los repositorios que est�n almacenados en el equipo del desarrollador.
4. Copia de trabajo: Es la copia local del repositorio, donde se hacen cambios para despu�s hacer un push que permita que los dem�s desarrolladores tengan acceso a los cambios hechos.
5. Area de preparaci�n: Es un archivo almacenado en el repositorio GIT que guarda la informaci�n sobre que cambios se agregar�n al commit, junto con informaci�n sobre quien hizo cada cambio y cuando.
6. Preparar cambios: Es cuando se marca un archivo en su version actual para que entre en el proximo commit.
7. Confirmar cambios: Se toman los archivos como estan en el area de preparacion y se guardan permanentemente en el directorio git.
8. Commit: Confirma los cambios hechos en los archivos que fueron incluidos con git add. Es necesario hacer git push para que los cambios se vean reflejados en el repositorio remoto.
9. Clone: Copia un repositorio existente y crea un clon o copia de dicho repositorio.
10. pull: Hace una fusi�n o merge de un repositorio remoto
11. push: Es la forma en que se transfieren los commits desde la copia de trabajo local hasta un repositorio remoto.
12. fetch: Baja los cambios del repositorio remoto a la rama sin fusionarlas (merge) localmente.
13. merge: Permite fusionar ramas
14. Status: Muestra el estado actual del directorio de trabajo y el �rea de preparaci�n. Permite ver que cambios est�n preparados y que archivos git no esta siguiendo.
15. Log: Log permite ver hacia atras las modificacionmes que se han llevado a cabo en un repositorio con un hist�rico de confirmaciones
16. checkout: Este comando permite navegar entre las ramas creadas por git branch.
17. Rama / Branch: Las ramas se utilizan para crear otra l�nea de desarrollo, un apuntador movil apuntando a confirmaciones. 
18. Etiqueta / TAg: permite etiquetar puntos de importancia especificos en la historia, se suele usar para marcar puntos donde se han lanzados nuevas versiones del programa.Es decir, la etiqueta es una rama, que nadie tiene la intenci�n de modificar.


[Fuente 1](https://es.wikipedia.org/wiki/Control_de_versiones)
[Fuente 2](http://www.tuprogramacion.com/programacion/git-el-control-de-versiones-distribuido/)
[Fuente 3](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
[Fuente 4]( https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms)
[Fuente 5, 6, 7](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)
[Fuente 8](https://githowto.com/commiting_changes)
[Fuente 9](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone)
[Fuente 10](https://www.atlassian.com/git/tutorials/syncing)
[Fuente 11](https://www.atlassian.com/git/tutorials/syncing#git-push)
[Fuente 12](https://www.atlassian.com/git/tutorials/syncing#git-fetch)
[Fuente 13](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
[Fuente 14](https://www.atlassian.com/git/tutorials/inspecting-a-repository)
[Fuente 15](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)
[Fuente 16](https://www.atlassian.com/git/tutorials/using-branches#git-checkout)
[Fuente 17](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
[Fuente 18](https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git)