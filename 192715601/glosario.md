**Glosario:** 

1. Control de versiones (VC): Un sistema VC *(Version Control)* se encarga de llevar un registro actualizado de las versiones de un archivo o conjunto de archivos de manera que se pueda tener acceso a cualquier versión de estos archivos desde sus orígenes a su última actuaizacion.  

    ***fuente(s):***   
    * [#1](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)
    
2. Control de versiones distribuido (VCD): Los sistemas VCD *(Version control distributed)* perimten además de llevar un registro actualizados de un archivo en una ruta específica, compartir el directorio de estos archivos con otros usuarios que tengan acceso al sistema, de esta manera se pueden realizar cambios a los archivos desde distintas locaciones y dispositivos, por lo tanto el repositorio de archivo, se mantiene en conjunto al equipo de trabajo que lo utiliza.  

    ***fuente(s):*** 
    * [#1](https://www.ecured.cu/Sistemas_de_control_de_versiones).
    * [#2](http://altenwald.org/2009/01/12/sistemas-de-control-de-versiones-centralizados-o-distribuidos)  
    
    
3. Repositorio remoto y repositorio local: Un repositorio es un directorio donde se encuentran los archivos asociados a nuestra cuenta. En git los usuarios manejan una copia completa del repositorio, el que puede ser local o remoto. Los repositorios remotos se alojan en servidores de la wed, mientras que los repositorios locales, trabajan en un servidor local.  

    ***fuente(s):***
    * [#1](https://carloscarcamo.gitbooks.io/git-intro/content/repositorios_remotos.html)
    * [#2](http://raulavila.com/2017/04/como-funciona-git-4/)
    
4. Copia de trabajo / Working Copy:  Los usuarios pertenecientes a un proyecto git, manejan una copia de su propio trabajo, para así editarlo y trabajar en él sin afectar el original, ya que la copia modificada sólo reemplazará al original una vez sea aprobado por el usuario.

  ***fuente(s):***

    * [#1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos).
    

5. Área de Preparación / Staging Area: También conocido como índice, se trata de un archivo que esta generalmente en el directorio de git, se utiliza para registrar los cambios a realizar en la confirmación posterior.
    
    ***fuente(s):***
    * [#1](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics).

6. Preparar Cambios / Stage Changes: 
 Representa el estado en el que se encuentran los archivos que esperan la confirmación para ser guardados.

    ***fuente(s):***
    * [#1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio).

7. Confirmar cambios / Commit Changes: Una vez terminado los cambios realizados por el usuario este se dispone a confirmar dichos camio para respaldarlos en el repositorio.

    ***fuente(s):***
    * [#1](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository).
    

8. Commit: Sentencia que permite adjuntar un comentario de usario al realizar alguna modificación como por ejemplo aplicar un comando add 

    ***fuente(s):***
    * [#1](https://git-scm.com/docs/git-commit). 
    
9. Clone: Comando que permite clonar un repositorio en algun directorio específico este repositorio puede ser local o remoto 

    ***fuente(s):***
    *[#1](https://git-scm.com/docs/git-clone).

10. Pull: Comando que permite descargar los cambios de una rama y de esta manera actualizarla.

    ***fuente(s):***
    * [#1](https://es.wikipedia.org/wiki/Git).
    

11. Push: Comando que permite compartir cambios realizados localmente en un repositorio remoto.

    ***fuente(s):***
    *[#1](https://git-scm.com/docs/git-push).
    
12. fetch: Comando que permite obtener datos de un repositorio remoto, para ubicarlos en un repositorio local.

    ***fuente(s):***
    * [#1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos#Inspeccionando-un-repositorio-remoto). 

13. merge: Comando que permite al usuario fusionar dos ramas distintas

    ***fuente(s):***
    * [#1](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar).

14. status: Comando git que permite conocer el estado actual de los archivos de un repositorio

    ***fuente(s):***
    *[#1](https://git-scm.com/docs/git-status).
    
15. log: Es un comando que permite revisar el historial de las modificaciones realizadas en el repositorio.
    
    ***fuente(s):***
    * [#1](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones).

16. checkout: Comando git el cual permite crear una nueva rama en un solo paso y además acceder directamente a ella.
    
    ***fuente(s):***
    * [#1](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

17. Rama / Branch: Funciona como un índice que representa lineas de desarrollo independientes, pero pertenecientes a un mismo repositorio . Estas ramas se pueden crear, listar, renombrar y eliminar. ramas.

    ***fuente(s):***
    * [#1](hhttps://www.atlassian.com/git/tutorials/using-branches)

18. Etiqueta / Tag: Son marcadores de puntos especificos en el historial de cambios de un proyecto, gracias a "Git tag" podemos listar estas etiquetas en orden alfabetico.

    ***fuente(s):***
    * [#1](https://filisantillan.com/creando-etiquetas-con-git/)
    
.