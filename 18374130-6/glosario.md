### Glosario

 - ##### Control de versiones (VC)

Cuando se trabaja a diario con múltiples archivos es necesario registrar cada uno de los cambios que se han realizado a los mismos, ya sea para recuperar versiones anteriores o recordar cuales fueron específicamente los cambios realizados.  Lo anterior se conoce como control de versiones y es facilitado mediante sistemas de control de versiones (VSC).

[https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- ##### Control de versiones distribuido (DVC)

Se trata del control de versiones realizado desde varios computadores clientes hacia un servidor que aloja un determinado repositorio del cual es posible que cada cliente obtenga copias de seguridad.

[https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

- ##### Repositorio remoto y Repositorio local

Un repositorio local es el conjunto de archivos y carpetas almacenados en un computador particular que sirven como base para efectuar un control de versiones. Un repositorio remoto es un repositorio alojado en la nube que puede ser leido o escrito por varios individuos.

[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

[http://www.ecodeup.com/instala-y-crea-tu-primer-repositorio-local-con-git-en-windows/](http://www.ecodeup.com/instala-y-crea-tu-primer-repositorio-local-con-git-en-windows/)

- ##### Copia de trabajo / Working Copy

Corresponde a la carpeta que se utilizará para trabajar con git, esta puede crearse a partir de un proyecto existente localmente o de uno alojado en la nube.

[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

- ##### Área de Preparación / Staging Area

Se trata de la ubicación temporal donde se encuentran los archivos que han sido editados o añadidos pero no confirmados.

[https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

- ##### Preparar Cambios / Stage Changes

Es la acción de añadir archivos a la Staging Area. Se utiliza el comando git add nombre\_archivo.

[https://githowto.com/staging\_changes](https://githowto.com/staging_changes)

- ##### Confirmar cambios / Commit Changes

Los archivos de la Staging Area pasan de manera definitiva al directorio git.

[https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

- ##### Commit

Comando utilizado para confirmar cambios (Los archivos de la Staging Area pasan de manera definitiva al directorio git).

[https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

- ##### clone

Comando para copiar un repositorio local o importar uno de la nube.

[https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

- ##### pull

Comando para recuperar cambios de un repositorio remoto a nuestro repositorio local.

[https://git-scm.com/docs/git-pull](https://git-scm.com/docs/git-pull)

- ##### push

Comando que permite exportar cambios desde nuestro repositorio local a un repositorio remoto.

[https://git-scm.com/docs/git-push](https://git-scm.com/docs/git-push)

- ##### fetch

Comando para recuperar cambios de un repositorio remoto a nuestro repositorio local.

[https://git-scm.com/docs/git-fetch](https://git-scm.com/docs/git-fetch)

- ##### merge

Comando para fusionar ramas.

[https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

- ##### status

Comando para ver los cambios recientes (archivos en el Staging Index) desde la rama que se está ubicado.

[https://git-scm.com/docs/git-status](https://git-scm.com/docs/git-status)

- ##### log

Comando para ver el historial de commit.

[https://git-scm.com/docs/git-log](https://git-scm.com/docs/git-log)

- ##### checkout

Comando para cambiar la rama en la que estamos ubicados.

[https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)

- ##### Rama / Branch

Una rama es una especie de &quot;puntero&quot; o punto de control desde donde podemos efectuar cambios a nuestros archivos (add y posterior commit). Este &quot;puntero&quot; permite identificar en cuál bifurcación del arbol se realizaron ciertos cambios.

[https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

- ##### Etiqueta / Tag

Una etiqueta se añade mediante el comando tag y permite crear anotaciones en ciertos momentos importantes de la edición del proyecto.

[https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
