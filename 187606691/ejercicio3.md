#Ejercicio 3
---
- Cantidad y nombre de las Branches: hay 4 branches sus nombres son Master, Megaramita, ramita1, ramita2
- Cantidad y nombre de las Tags: hay 5 etiquetas y sus nombres son todas-las-tags-al-mismo-tiempo, tag1, super-release, final-final-ahora-si, asdasdaf-tag.
- Los últimos 3 commits son:
- - Andres Kroll, Aqui van los archivos, __016c86e2896710cd9ab220326d1cb10910644af5__
- - Jose Toro, Agregado Glosario con contenido completado., __f82ad2c3b947c7b81a290cdc6350b6447f2145bb__
- - Jose Toro, Agregado archivo ejercicio3.md, __1f627097c96af54e25cfe4bdecc50b61e7193b3d__

