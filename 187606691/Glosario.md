# Glosario:
---
A continuación se presenta un glosario de definiciones sobre el control de versiones y el uso de la herramienta [GIT][] para esta.

- __Control de Versiones(CV)__: Sistema de gestión referente a los cambios hechos sobre un producto. Dentro de esta existen revisiones o versiones que indican el estado del producto en un momento dado.
Un sistema de gestión de versiones permite revertir el producto a un estado anterior, tener un registros de las personas que han modificado que iteración del producto entre otros. Esto facibilita la detección y solución de posibles errores dentro del producto, así como recuperar los datos de una versión previa en caso de perdida del producto final.
Existen 3 tipos de sistemas de versión de control: locales, centralizados y distribuidos.

- __Control de versiones Distribuido (DVC)__: Los sistemas de control de versiones distribuidos son clientes que, además de descargar la ultima instancia del producto, lo replican (o _clonan_) completamente. De esta manera se evita la perdida de datos ante la posible eventualidad de un error del servidor si es que ya se ha replicado el repositorio en algun cliente, del cual se puede restaurar mediante a la copia de seguridad de los datos.
Este tipo de sistemas son especialmente utiles para trabajos colaborativos mediante varios flujos de trabajo.

![Figura 1: Sistema de control de versiones Distribuido][imagen1]
__Figura 1:__ Sistema de control de versiones Distribuido. [Fuente][imagen1]

- __Repositorio remoto y Repositorio local__: Cuando nos referimos a un **repositorio remoto** nos referimos a un repositorio cuyas versiones están alojadas en _internet_. Estos pueden ser de sólo lectura o lectura/escritura dependiendo de los permisos. Este tipo de repositorios permite el trabajo colaborativo mediante la gestion de este.
Un **respositorio local**, por otro lado, nos permite mantener un control de versiones dentro nuestro computador, sin necesidad de una conexión a _internet_. Esto permite mantener un entorno de desarrollo de prueba hasta que el producto llegue a tal punto que sea recomendable compartirlo dentro del repositorio remoto.
Es necesario el uso de ambos tipos de repositorios para la gestión ordenada del producto dentro del sistema de control de versiones.

![Figura 2- Repositorio remoto y local][imagen2]
__Figura 2:__ Ciclo de interacción Repositorio remoto y local. [Fuente][imagen2]

- __Copia de trabajo/Working Copy__: El concepto de copia de trabajo se refiere que, al momento de retirar el producto del repositorio, no retiramos el repositorio completo, sino que sólo la parte o rama en la que vamos a trabajar.
Esto crea un diferencia fundamental entre los sistemas de control _distribuidos_, en referencia a los sistemas _locales_ y _centralizados_(Como por ejemplo, [SVN][]), pues los sistemas _distribuidos_ (como [GIT][]) no hacen diferencia entre la copia de trabajo y el repositorio local.

- __Área de preparación/Stagin area__: Se refiere al archivo contenedor en el cual se _clonará_ el repositorio y el cual guardará la información de los próximos cambios registrados.

- __Preparar Cambios/State Changes__: En la etapa de preparación de cambios se mantendrá un seguimiento de los datos que fueron modificados para así generar una instancia que será replicada en el repositorio remoto.
El estado seguimiento de un archivo define si este será considerado en la modificación del respositorio remoto una vez esta sea confirmada y enviada. Un archivo en seguimiento no tiene que estar necesariamente modificado, puede no haber sufrido ningún cambio y aún así ser enviado al repositorio remoto al momento de modificarlo.

![Figura 3: Ciclo de vida del estado de un archivo][imagen3]
__Figura 3:__ Ciclo de vida del estado de un archivo. [Fuente][imagen3]

- __Confirmar Cambios/Commit Changes__: Refierase a la acción de enviar los cambios realizados dentro del repositorio local al repositorio remoto (_veáse figura 3_), modificando este último. Los cambios sólo se harán sobre los archivos que han sido marcados para estos cambios (con seguimiento).

- __commit__: Comando git para señalar la confirmación de los cambios de los archivos modificados para su envio al repositorio remoto. Usualmente es precedido por un comando add.
```bash
git commit [-a | --interactive | --patch] [-s] [-v] [-u<mode>] [--amend]
	   [--dry-run] [(-c | -C | --fixup | --squash) <commit>]
	   [-F <file> | -m <msg>] [--reset-author] [--allow-empty]
	   [--allow-empty-message] [--no-verify] [-e] [--author=<author>]
	   [--date=<date>] [--cleanup=<mode>] [--[no-]status]
	   [-i | -o] [-S[<keyid>]] [--] [<file>…​]
```
```bash
$git add .
$git commit -m "Esto es un ejemplo"
```

- __clone__: Comando git que permite _clonar_ el repositorio remoto entregado dentro del directorio actual.
```bash
git clone [--template=<template_directory>]
	  [-l] [-s] [--no-hardlinks] [-q] [-n] [--bare] [--mirror]
	  [-o <name>] [-b <name>] [-u <upload-pack>] [--reference <repository>]
	  [--dissociate] [--separate-git-dir <git dir>]
	  [--depth <depth>] [--[no-]single-branch]
	  [--recurse-submodules] [--[no-]shallow-submodules]
	  [--jobs <n>] [--] <repository> [<directory>]
```
```bash
$git clone https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo
```

- __pull__: Comando git para obtener la versión actual del repositorio remoto para integrarla al repositorio local.
```bash
$git pull [options] [<repository> [<refspec>…​]]
```

- __push__: comando git para enviar todos los cambios confirmados en archivos con seguimiento en el respositorio local al respositorio remoto.
```bash
git push [--all | --mirror | --tags] [--follow-tags] [--atomic] [-n | --dry-run] [--receive-pack=<git-receive-pack>]
	   [--repo=<repository>] [-f | --force] [-d | --delete] 
	   [--prune] [-v | --verbose]
	   [-u | --set-upstream] [--push-option=<string>]
	   [--[no-]signed|--sign=(true|false|if-asked)]
	   [--force-with-lease[=<refname>[:<expect>]]]
	   [--no-verify] [<repository> [<refspec>…]]
```
```bash
$git add .
$git commit -m "Esto es un ejemplo"
$git push
```

- __fetch__: Comando git que permite obtener datos de una rama o etiqueta especifica dentro del repositorio local. Este comando es util para obtener datos que aún no existen dentro del repositorio local pero no lo intenta unir con los datos existentes de tu respositorio.
```bash
git fetch [<options>] [<repository> [<refspec>…​]]
git fetch [<options>] <group>
git fetch --multiple [<options>] [(<repository> | <group>)…​]
git fetch --all [<options>]
```

```bash
$git fetch origin
```

- __merge__: Comando git para unir dos o más historias de desarrollo. Es común que este comando genere conflicto en proyectos grandes. 
```bash
git merge [-n] [--stat] [--no-commit] [--squash] [--[no-]edit]
	[-s <strategy>] [-X <strategy-option>] [-S[<keyid>]]
	[--[no-]allow-unrelated-histories]
	[--[no-]rerere-autoupdate] [-m <msg>] [<commit>…​]
git merge --abort
git merge --continue
```
```bash
$git merge origin
```
![Git merge satirización][imagen4]

__Figura 4:__ Satirización de los conflictos del comando merge. [Fuente][imagen4].

- __status__: Comando git que permite revisar el estado del repositorio local, referente a los archivos modificados que presentan o no seguimiento.
```bash
git status [<options>…​] [--] [<pathspec>…​]
```

- __log__: Comando git que muestra por pantalla los registros de las confirmaciones de cambios (_commit_).
```bash
git log [<options>] [<revision range>] [[\--] <path>…​]
```

- __checkout__:Comando git para cambiar de rama de desarrollo en la que se está trabajando.
```
git checkout [-q] [-f] [-m] [<branch>]
git checkout [-q] [-f] [-m] --detach [<branch>]
git checkout [-q] [-f] [-m] [--detach] <commit>
git checkout [-q] [-f] [-m] [[-b|-B|--orphan] <new_branch>] [<start_point>]
git checkout [-f|--ours|--theirs|-m|--conflict=<style>] [<tree-ish>] [--] <paths>…​
git checkout [-p|--patch] [<tree-ish>] [--] [<paths>…​]
```

- __Rama/Branch__: Las ramas permiten mantener un ambiente de desarrollo aislado para mantener un orden dentro del desarrollo del producto. Estas, al ser completadas deben ser fucionadas con la rama principal.
```bash
git branch [--color[=<when>] | --no-color] [-r | -a]
	[--list] [-v [--abbrev=<length> | --no-abbrev]]
	[--column[=<options>] | --no-column] [--sort=<key>]
	[(--merged | --no-merged) [<commit>]]
	[--contains [<commit]] [--no-contains [<commit>]]
	[--points-at <object>] [--format=<format>] [<pattern>…​]
git branch [--set-upstream | --track | --no-track] [-l] [-f] <branchname> [<start-point>]
git branch (--set-upstream-to=<upstream> | -u <upstream>) [<branchname>]
git branch --unset-upstream [<branchname>]
git branch (-m | -M) [<oldbranch>] <newbranch>
git branch (-d | -D) [-r] <branchname>…​
git branch --edit-description [<branchname>]
```

- __Etiqueta/Tag__: comando git que permite _etiquetar_ estados del proyecto, como por ejemplo versiones. Tambien podemos verificar o eliminar etiquetas con esta.

```bash
git tag [-a | -s | -u <keyid>] [-f] [-m <msg> | -F <file>]
	<tagname> [<commit> | <object>]
git tag -d <tagname>…​
git tag [-n[<num>]] -l [--contains <commit>] [--no-contains <commit>]
	[--points-at <object>] [--column[=<options>] | --no-column]
	[--create-reflog] [--sort=<key>] [--format=<format>]
	[--[no-]merged [<commit>]] [<pattern>…​]
git tag -v [--format=<format>] <tagname>…​
```
```bash
$git tag 3.2.0 1b2e1d63ff
```

# Referencias:
---
[Git - Empezando - Acerca del control de versiones][rf1]
[Stack Overflow - Gits local repository and remote repository confusion][rf2]
[SVNbook - Creating a Working Copy][rf3]
[Git - Getting Started Git Basics][rf4]
[Git - Git Basics - Recording Changes to the Repository][rf5]
[Git - git-commit][rf6]
[Git - git-clone][rf7]
[Git - git-pull][rf8]
[Git - git-push][rf9]
[Git - git-fetch][rf10]
[Git - git-status][rf11]
[Git - git-log][rf12]
[Git - git-checkout][rf13]
[Git - git-branch][rf14]

[//]:#(ATAJOS-CUERPO)
[GIT]: https://github.com/
[SVN]: https://subversion.apache.org/

[//]:#(ATAJOS-REFERENCIAS)
[rf1]:https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
[rf2]:https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts
[rf3]:http://svnbook.red-bean.com/en/1.6/svn.tour.initial.html
[rf4]:https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
[rf5]:https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository
[rf6]:https://git-scm.com/docs/git-commit
[rf7]:https://git-scm.com/docs/git-clone
[rf8]:https://git-scm.com/docs/git-pull
[rf9]:https://git-scm.com/docs/git-push
[rf10]:https://git-scm.com/docs/git-fetch
[rf11]:https://git-scm.com/docs/git-status
[rf12]:https://git-scm.com/docs/git-log
[rf13]:https://git-scm.com/docs/git-checkout
[rf14]:https://git-scm.com/docs/git-branch
[rf15]:https://git-scm.com/docs/git-tag



[//]:#(IMAGENES)
[imagen1]:https://git-scm.com/figures/18333fig0103-tn.png
[imagen2]:https://greenido.files.wordpress.com/2013/07/git-local-remote.png?w=696&h=570
[imagen3]:https://git-scm.com/book/en/v2/images/lifecycle.png
[imagen4]:https://i.imgur.com/X9zNSkM.gif
