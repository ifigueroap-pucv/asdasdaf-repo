#Glosario

* **Control de versiones:** Es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante.

* **Control de versiones distribuidos:** permite a muchos desarrolladores de software trabajar en un proyecto común sin necesidad de compartir una misma red. Las revisiones son almacenadas en un sistema de control de versiones distribuido
 
* **Repositorio remoto y repositorio local:** son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red. Puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas

* **Copia de trabajo / Working Copy:** Copia local donde el desarrollador trabaja.

* **Área de Preparación / Staging area:** Es la zona donde se añaden los cambios que se van a hacer commit. Promociona una buena práctica de Git.

* **Preparar Cambios / Stage Changes:** hacer algunos cambios, y confirmar instantáneas de esos cambios a tu repositorio cada vez que el proyecto alcance un estado que desees grabar

* **Confirmar cambios / Commit Changes :** realizar algunos cambios y confirmar instantáneas de esos cambios en el repositorio cada vez que el proyecto alcance un estado que quieras conservar.

* **Commit:** Envío de cambios locales al repositorio • Como resultado cambia la versión del archivo(s)/repositorio. Como resultado cambia la versión del archivo(s)/repositorio

* **Clone:** para orientar un repositorio existente y crear un clon o una copia del repositorio de destino

* **Pull:** Obtener la copia del remitente especificado de la rama actual e inmediatamente fusionarla en la copia local

* **Push:** Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo remota.

* **Fetch:** Este comando recupera todos los datos del proyecto remoto que no tengas todavía

* **Merge:** Intentará fusionar automáticamente los cambios. Aplica los cambios de un archivo a otro

* **Status:** Cambios pendientes de commit

* **Long:** Permite visualizar/revisar la lista de cambios de un archivo/repositorio

* **Checkout:** Creación de una copia de trabajo que rastrea un repositorio

* **Branch/ rama:** Permite crear una copia de un archivo/carpeta rastreada. Permite desarrollar en paralelo en otra “rama” pero dejando constancia de la relación que existe con la rama original.

* **Tag:** Listar las etiquetas disponibles en Git es sencillo





+ [Fuente aquí](https://git-scm.com)

+ **Ignacio Vegas Fernandez 18921971-7**
