*Control de versiones (VC):			Es el registro de los cambios realizados sobre un archivo o un conjunto de estos, de modo que se puedan recuperar versiones. 
*Control de versiones distribuido (DVC):	Los repositorios estan copiados en otros computadores, si hay alguna perdida se puede restaurar de otro computador.
*Repositorio remoto y Repositorio local:	Los repositorios remoto son versiones de un proyecto que se encuentran alojados en internet. El repositorio local es similar al remoto solo que se almacenan los repositorios en un computador.       
*Copia de trabajo / Working Copy:		Es una copia del trabajo hecho con su ultima version en el git.
*�rea de Preparaci�n / Staging Area:		Contiene los pasos a seguir respecto a la proxima confirmacion a realizar.
*Preparar Cambios / Stage Changes:		Se prepara el archivo para luego ser cambiado en el repositorio remoto.
*Confirmar cambios / Commit Changes:		Confirmar el cambio preparado, se guardan los cambios realizados.
*Commit:					Sirve para hacer cambios en el repositorio
*clone: 					Sirve para hacer una copia del repositorio remoto hacia el reposotorio local.
*pull: 						Sirve para combinar todos los cambios que se han hecho en el repositorio local.
*push:						Sirve para enviar los cambios que se han hecho a la rama principal de los repositorios remotos.
*fetch:						Sirve para buscar todos los objetos de un repositorio remoto, para que se pueda revisar que cambios hubieron comparado a lo que se tiene.
*merge: 					Sirve para fusionar una rama con otra rama.
*status: 					Muestra la lista de archivos que se han cambiado y los que seran a�adidos.
*log: 						Muestra los ultimos commit hechos
*checkout:					Actualiza los archivos en la rama o para cambiar las ramas entre ellas.
*Rama / Branch:					Es un puntero a la copia puntial de los contenidos preparados, ademas es un puntero hacia la confirmacion de un cambio.
*Etiqueta / Tag:				Sirve para marcar commits dentro del repositorio


	

https://git-scm.com/book/es/v1
https://es.wikipedia.org/wiki/Git