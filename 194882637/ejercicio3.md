
####**a. Cantidad y nombre de los branches**
git branch -a
  
master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/megaramita1
  remotes/origin/ramita1
  remotes/origin/ramita2


* Se puede observar que existen 5 branches cuyos nombres son: *HEAD, master, megaramita1, ramita1* y *ramita2*

####**b. Cantidad y nombre de las etiquetas**

$ git tag
asdasdaf-tag
final-final-ahora-si
super-release
tag1
todas-las-tags-al-mismo-commit


* Se puede observar que la cantidad de tag son 5 cuyos nombres son: *asdasdaf-tag, final-final-ahora-si, super-release, tag1* y *todas-las-tags-al-mismo-commit*

####**c. Nombre, mensaje y c�digo hash de los �ltimos 3 commits. Use el comando �git log� para obtener esta informaci�n.**


commit 204b55c3217479305ed7f3971550a9170247eef9 (HEAD -> master, origin/master, origin/HEAD)
Author: Israel Ogas <ogasvega.israel@gmail.com>
Date:   Tue Aug 8 19:54:06 2017 -0300

    enviando archivos

commit 2e9e63c52032fa4b6cab2e3fb77292d77ed3ff21
Author: bastiantobar.94@gmail <bastiantobar.94@gmail.com>
Date:   Tue Aug 8 17:15:21 2017 -0400

       ahora

commit 98904bfe1d9bacd1cffb9d02f30649bb35c05d9c
Merge: cefd04e 6a5f65b
Author: bastiantobar.94@gmail <bastiantobar.94@gmail.com>
Date:   Tue Aug 8 17:05:39 2017 -0400

    Merge branch 'master' of http://bitbucket.org/ifigueroap-pucv/asdasdaf-repo

    Ejercicio 3 realizado a las 7:15pm, 08/08/17 (Hrs).


