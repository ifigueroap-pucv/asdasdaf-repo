
#**Glosario**
 

**a. Control de versiones (VC) :** 
: Se hizo el control de versiones con el fin de que los cambios realizados en el documento se guarden en todas las versiones pasadas, agregando a esto la hora y fecha en que se realizo y quien realizo la modificacion con el fin de poder acceder a estas versiones en alg�n punto. [(1)] 


**b. Control de versiones distribuido (DVC) :**
: Implica que aunque existe un repositorio central para todos que va guardando cada uno de los cambios realizados por los diferentes usuario, cada uno de ellos podr� descargar su propia copia de trabajo. As� mismo todos pueden acceder a la versi�n de los otros usuarios. [(2)]

**c. Repositorio remoto y Repositorio local :**
: Los repositorios remotos son versiones de tu proyecto que se encuentra en un lugar distinto al de tu computador, generalmente se dice que est� en la Internet o en otra red. En cambio el repositorio local, es el lugar donde se almacena el proyecto dentro del computador del usuario [(3)]

**d. Copia de trabajo / Working Copy :**
: Es una "rama" donde se puede hacer clonando desde un repositorio que ya existente o creando uno nuevo de forma local para que otros usuarios puedan modificar los distintos documentos. [(4)]

**e. �rea de Preparaci�n / Staging Area**
: Es un archivo en que generalmente se encuentra en el directorio, que almacena informaci�n el pr�ximo "commit". Tambi�n se le conoce como "Index".  B�sicamente se puede definir como el lugar en donde se elige cuales cambios se enviaran.[(5)]

**f. Preparar Cambios / Stage Changes :**
: ]Es la preparaci�n de los cambios que se desean  realizar para luego confirmar estos cambios en el Staging Area (�rea de Preparaci�n). [(6)]

**g. Confirmar cambios / Commit Changes :**
:  Esla acci�n de confirmar los cambios que se prepararon en el Stage Changes preparandolos para el Staging Area (�rea de Preparaci�n). [(7)]

**h. Commit :**
: Significa que los cambios tentativos que se realizaron pasan a ser permanentes. [(8)]

**i. Clone :**
: Es copiar el repositorio remoto en el local para luego sincronizar entre ambas. [(9)]

**j. Pull :**
: Es la suma del merge y Fetch que b�sicamente trata de la baja de los cambios de una rama determinada y la actualiza en el repositorio local. [[10]]

**k. Push :**
: Subir los cambios hechos en tu ambiente de trabajo a una rama de trabajo que sea tuya. [(11)]

**l. Fetch :**
: recoje los cambios desde un repositorio remoto, pero los deja en otro rama. [(12)]

**m. Merge : **
: Se dedica a fusionar las ramas. [(13)]

**n. Status :**
: Muestra el estado del �rbol de trabajo, esto quiere decir que se muestran las rutas que tienen diferencias entre el HEAD y archivo �ndice. [(14)]

**o. Log :**
: Muestra los registro de commit. [(15)]

**p. Checkout :**
: Actualiza los archivos del �rbol en el que se trabajo para que estos puedan coincidir con los de la versi�n del �ndice o a los del �rbol. [(16)]

**q. Rama / Branch :**
: Cuando se trabajo con el control de versiones se puede acceder a los distintos cambios que se han realizado como si se tratara de un arbol. Es por esto que tenemos la rama principal o tronco llamada master y las que se van abriendo llamadas branch. [(17)]

**r. Etiqueta / Tag :**
: Se utiliza generalmente para enumerar las versiones que se van lanzando. [(17)] 

  1: https://producingoss.com/es/vc.html
  2: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
  3: https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos
  4: https://programarivm.com/como-crear-un-repositorio-en-github-y-luego-clonarlo-con-git-for-windows/
  5: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
  6: http://gitready.com/beginner/2009/01/18/the-staging-area.html
  7: http://gitready.com/beginner/2009/01/18/the-staging-area.html
  8: https://es.wikipedia.org/wiki/Commit
  9: https://help.github.com/articles/cloning-a-repository/
  10: https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git
  11: https://www.gestiopolis.com/que-son-los-sistemas-de-jalar-pull-y-empujar-push/
  12: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
  13: https://git-scm.com/docs/git-status
  14: https://git-scm.com/docs/git-log
  15: https://git-scm.com/docs/git-checkout
  16: https://cursoweb20.net/2013/04/24/que-son-las-etiquetas-o-tags-para-que-sirven/
  17: http://web.uservers.net/ayuda/soluciones/paginas-y-html/-que-es-un-tag_Mjg.html
  
  
  
  