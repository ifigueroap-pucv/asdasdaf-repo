
####**a. Cantidad y nombre de los branches**
git branch -a

>  (*  master)
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/megaramita1
  remotes/origin/ramita1
  remotes/origin/ramita2

* Se puede observar que existen 5 branches cuyos nombres son: *HEAD, master, megaramita1, ramita1* y *ramita2*

####**b. Cantidad y nombre de las etiquetas**

git tag

>asdasdaf-tag
final-final-ahora-si
super-release
tag1
todas-las-tags-al-mismo-commit


* Se puede observar que la cantidad de tag son 5 cuyos nombres son: *asdasdaf-tag, final-final-ahora-si, super-release, tag1* y *todas-las-tags-al-mismo-commit*

####**c. Nombre, mensaje y c�digo hash de los �ltimos 3 commits. Use el comando �git log� para obtener esta informaci�n.**



>commit 5356cb0aa8e58a93b8d28f9a6da139e0a3ed0462 (origin/master, origin/HEAD)
Author: Andres Kroll <andreskroll17@gmail.com>
Date:   Mon Aug 7 20:16:08 2017 -0400

	aqui va el ejercicio 3


	
>commit 5001d03d075d6dc91ee5696df682d788e633f75c
Author: Patricio Gutierrez <patricio.gutierrez4000@gmail.com>
Date:   Mon Aug 7 17:30:43 2017 -0400

    taller completado

>commit df9676859afc7452d8e0dc69b862d1c82b96d594
Author: Patricio Gutierrez <patricio.gutierrez4000@gmail.com>
Date:   Mon Aug 7 15:55:29 2017 -0400

    ejercio3_Terminado



