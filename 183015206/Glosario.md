
#**Glosario**
 

**a. Control de versiones (VC) :** 
: El control de versiones se hizo con el fin de que los cambios realizados en el documento o archivos se guarden todas las versiones anteriores, a�adiendo a esto la fecha y hora en que se hizo y quien realizo la modificacion con el prop�sito de poder acceder a estas versiones en alg�n punto. [[1]] 


**b. Control de versiones distribuido (DVC) :**
: Un control de versiones distribuidos implica que aunque existe un repositorio central para todos que va guardando cada uno de los cambios realizados por los diferentes usuario, cada uno de ellos podr� descargar su propia copia de trabajo. As� mismo todos pueden acceder a la versi�n de los otros usuarios. [[2]]

**c. Repositorio remoto y Repositorio local :**
: Los repositorios remotos son versiones de tu proyecto que se encuentra en un lugar distinto al de tu computador, generalmente se dice que est� en la Internet o en otra red. En cambio el repositorio local, es el lugar donde se almacena el proyecto dentro del computador del usuario. [[3]]

**d. Copia de trabajo / Working Copy :**
: Es una "rama" o una copia donde se puede hacer clonando desde un repositorio ya existente o creando uno nuevo localmente para que otros usuarios puedan modificar los distintos documentos o archivos. [[4]]

**e. �rea de Preparaci�n / Staging Area**
: Es un archivo en que generalmente se encuentra en el directorio, que almacena informaci�n el pr�ximo "commit". Tambi�n se le conoce como "Index".  B�sicamente se puede definir como el lugar en donde se elige cuales cambios se enviaran. [[5]][[6]]

**f. Preparar Cambios / Stage Changes :**
: Se refiere a la acci�n para la preparaci�n de los cambios que se quieren realizar para luego confirmar estos cambios en el Staging Area (�rea de Preparaci�n). [[7]]

**g. Confirmar cambios / Commit Changes :**
:  Se refiere a la acci�n de confirmar los cambios que se prepararon en el Stage Changes preparandolos para el Staging Area (�rea de Preparaci�n). [[8]]

**h. Commit :**
: En simples palabras commit significa que los cambios tentativos que se realizaron pasan a ser permanentes. [[9]]

**i. Clone :**
: Es copiar el repositorio remoto en el local para luego sincronizar entre ambas. [[10]]

**j. Pull :**
: Es la suma del fetch y merge que b�sicamente trata de la baja de los cambios de una rama determinada y la actualiza en el repositorio local. [[11]]

**k. Push :**
: Lo que hace es subir los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya. [[12]]

**l. Fetch :**
: Trae los cambios desde un repositorio remoto, pero los deja en otro rama. [[12]]

**m. Merge : **
: B�sicamente su funci�n es el de fusionar las ramas. [[13]]

**n. Status :**
: Muestra el estado del �rbol de trabajo, esto quiere decir que se muestran las rutas que tienen diferencias entre el archivo �ndice y el HEAD. [[14]]

**o. Log :**
: Muestra los registro de commit. [[15]]

**p. Checkout :**
: Actualiza los archivos del �rbol de trabajo para que estos puedan coincidir con los de la versi�n del �ndice o a los del �rbol. [[16]]

**q. Rama / Branch :**
: Cuando se trabajo con el control de versiones podemos acceder a los distintos cambios que se han realizado tal como si de un �rbol se tratara. Es por esto que tenemos el tronco o rama principal llamada master y las que se van abriendo llamadas branch. [[17]]

**r. Etiqueta / Tag :**
: En una funci�n que se utiliza generalmente para enumerar las versiones que se lancen. [[18]] 

  [1]: https://www.1and1.es/digitalguide/paginas-web/desarrollo-web/git-vs-svn-una-comparativa-del-control-de-versiones/
  [2]: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
  [3]: https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos
  [4]: https://programarivm.com/como-crear-un-repositorio-en-github-y-luego-clonarlo-con-git-for-windows/
  [5]: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
  [6]: http://gitready.com/beginner/2009/01/18/the-staging-area.html
  [7]: http://gitready.com/beginner/2009/01/18/the-staging-area.html
  [8]: http://gitready.com/beginner/2009/01/18/the-staging-area.html
  [9]: https://es.wikipedia.org/wiki/Commit
  [10]: https://help.github.com/articles/cloning-a-repository/
  [11]: https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git
  [12]: https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git
  [13]: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
  [14]: https://git-scm.com/docs/git-status
  [15]: https://git-scm.com/docs/git-log
  [16]: https://git-scm.com/docs/git-checkout
  [17]: https://www.adictosaltrabajo.com/tutoriales/git-branch-bash/
  [18]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
  
  
  