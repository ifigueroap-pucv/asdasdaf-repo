# Glosario
1. Control de versiones:
Es un software que registra los cambios realizados a un conjunto de archivos dentro de un periodo de tiempo.
[Link 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
[LInk 2](https://www.tutorialspoint.com/git/git_basic_concepts.htm)

2. Control de versiones distribuido: 
Es una característica del control de versiones que puede trabajar en el mismo proyecto diferentes personas de forma remota y este replican a todo el equipo de trabajo
[Link 1](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

3. Repositorio remoto y Repositorio local:
Son los archivos del proyecto que esta alojado fuera del equipo, dentro de algún repositorio en linea, mientras que el repositorio local es lo que esta guardado en el propio equipo.
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
4. Copia de trabajo /  Working Copy:
Se refiere a todos los archivos involucrados.Estos son almacenados dentro de la base de datos comprimida.Lista para modificar.
[Link](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

5. Área de Preparación /  Staging Area
El área de preparación es un sencillo archivo, generalmente contenido en tu directorio de Git, que almacena información acerca de lo que va a ir en tu próxima confirmación. 
[Link](https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)

6. Preparar Cambios /  Stage Changes
7. Confirmar cambios /  Commit Changes
8. Commit
Confirma los cambios realizados  del proyecto
[Link](https://learnxinyminutes.com/docs/es-es/git-es/)
9. clone
Comando para descargar la información de un repositorio para trabajar en él.
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
10. pull
Permite incorporar los cambios desde un repositorio de trabajo a uno remoto antes de verificar los cambios con el repositorio externo.
11. push
Comando que me permite enviar los cambios a un repositorio remoto.
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
12. fetch
Permite recuperar datos de tus repositorios remotos
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
13. merge
realiza una fusión a tres bandas entre las dos últimas instantáneas de cada rama y el ancestro común a ambas, creando un nuevo commit con los cambios mezclados.
[Link](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Reorganizando-el-trabajo-realizado)
[Link2](http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html#merging)
14. status
Comando que me muestra el estado de los archivos de la carpeta del proyecto. Me muestra los archivos agregados, modificados, con seguimiendo y no.
[Link](https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Seguimiento-Básico)
15. log
Comando que me permite mostrar todos los commit que se realizen en nuestro proyecto
[Link](https://www.desarrolloweb.com/articulos/git-log.html)
16. checkout
Comando que me permite moverme entre las ramas del proyecto para realizar add y luego hacer commit de la rama selecionada.
[Link](https://www.atlassian.com/git/tutorials/using-branches#git-checkout)
17. Rama / Branch
Comando que me crea una nueva rama que me permite agregar nuevas funcionalidades al proyecto y gestionar las versiones de esta.
[Link](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-¿Qué-es-una-rama%3F)
[Link2](https://www.adictosaltrabajo.com/tutoriales/git-branch-bash/)
18. Etiqueta / Tag:
Permite destacar hitos importantes dentro de la linea de trabajo.Por ejemplo una nueva versión del software.Esta no genera una nueva ramificación
[Link](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
[Link](https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git)