1. Cantidad y nombre de los branches
4 branches. 
`origin/master`, `origin/megaramita1`, `origin/ramita1` y `origin/ramita2`.
2. Cantidad y nombre de las etiquetas
5 tag. 
`asdasdaf-tag`, `final-final-ahora-si` ,`super-release`,`tag1,todas-las-tags-al-mismo-commit`.
3. Nombre, mensaje y c�digo hash de los �ltimos 3 commits
```
commit 6354649029aa38445127a530d934fb2b01f4abdb
Author: Claudio Andr�s Quiroz Navia <claudioandre94@gmail.com>
Date:   Wed Aug 9 01:59:26 2017 -0400
Se a�ade el glosario del taller 0
```

```
commit f0dc0a3a2bd838daad7af4f1849013a4aa27d5f4
Author: 18916791-1 <michael.fernandez.o@mail.pucv.cl>
Date:   Tue Aug 8 12:41:29 2017 -0400
Hola!
```

```
commit c09f7571047220ac37b431184f818316b1d60785
Merge: 95a55b1 68bbe51
Author: Jose Toro <jose.toro.p@mail.pucv.cl>
Date:   Tue Aug 8 11:24:49 2017 -0400
Merge branch 'master' of https://bitbucket.org/ifigueroap-pucv/asdasdaf-repo
```
