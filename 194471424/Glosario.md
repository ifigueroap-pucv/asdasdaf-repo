# Glosario  

A continuaci�n, se presentan definiciones de conceptos relacionados al uso de la herramienta git.  

+ **Control de versiones (VC)**  
   :   Seguimiento, control y almacenamiento de los cambios que se producen en un archivo o conjunto de archivos (proyecto), para as� tener posterior acceso a la informaci�n de las m�ltiples versiones de este.  
   Esto �ltimo permite que se pueda volver a versiones anteriores de un archivo, comparar versiones, conocer los autores de los cambios, tener puntos de recuperaci�n de informaci�n, entre otros beneficios.  

+ **Control de versiones distribuido (DVC)**  
   :   Es una arquitectura de almacenamiento en el control de versiones que consiste en la r�plica completa del repositorio local, que es almacenada en otro repositorio, uno por cada usuario.  

+ **Repositorio remoto y Repositorio local**  
   :   Un repositorio local es la residencia completa del VC de un archivo o proyecto, en un equipo o servidor local.  
   Un repositorio remoto es cualquier versi�n de este archivo, que reside en alg�n lugar de internet.

+ **Copia de trabajo / Working Copy**  
   :   Es una carpeta o documento que es copia del archivo con el que se est� trabajando en el VCS, donde se trabajan cambios.

+ **�rea de preparaci�n / Staging Area**
   :   En Git, es la secci�n donde se encuentra el archivo en estado Preparado, es decir, que est� marcado para que los cambios sean confirmados.  
   Es el �rea que media entre el directorio de trabajo, donde se modifica el archivo y el repositorio donde se almacenan los cambios confirmados.  

+ **Preparar Cambios / Stage Changes**  
   :   En Git, se refiere a la acci�n, en la �rea de preparaci�n, de preparar los cambios hechos en un archivo para confirmarlos.  

+ **Confirmar Cambios / Commit Changes**  
   :   En Git, hace referencia a la acci�n de confirmar los cambios previamente preparados, envi�ndolos al repositorio.  

+ **Commit**  
   :   Instrucci�n en Git para confirmar los cambios preparados, ocasionando el registro de estos en el repositorio.  

+ **Clone**  
   :   Instrucci�n en Git para clonar o crear una copia local de un repositorio en un nuevo directorio.  

+ **Pull**  
   :   Instrucci�n en Git para obtener los archivos con cambios confirmados desde un repositorio remoto y trasladarlos al repositorio local para actualizarlo.  

+ **Push**  
   :   Instrucci�n en Git para obtener los archivos con cambios confirmados desde el repositorio local y enviarlos a un repositorio remoto para actualizar los archivos.  

+ **Fetch**  
   :   Instrucci�n en Git para la acci�n de obtener los archivos con cambios confirmados desde un repositorio.  

+ **Merge**  
   :   Instrucci�n en Git para unir 2 o m�s versiones de un archivo en un �nico archivo.  

+ **Status**  
   :   Instrucci�n en Git que muestra los archivos que se encuentran en el directorio actual que no tienen un seguimiento de cambios.  
   Adem�s, muestra los archivos modificados pero que no han recibido un commit.  

+ **Log**  
   :   Instrucci�n en Git que muestra todos los registros de los commit hechos.  

+ **Checkout**  
   :   Instrucci�n en Git para cambiar entre branches, y tambi�n permite acceder a un branch de una versi�n anterior (existente).  

+ **Branch**  
   :   Son directorios o sub-directorios que se encuentran dentro del �rea de trabajo.  

+ **Etiqueta / Tag**  
   :   Instrucci�n en Git que permite darle una etiqueta a los registros.  


# Fuentes  

Cabe destacar las fuentes desde las cuales obtuve informaci�n para comprender los conceptos, aunque de igual modo hay algunos que quiz�s, m�s que basarme en las definiciones escritas, me bas� en lo que entend� del concepto al momento de 'aplicarlo'.

* VC  y DVC  
   + https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control  
   + https://es.wikipedia.org/wiki/Control_de_versiones  

* Repositorio remoto y local  
   + https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes  
   + http://www.ecodeup.com/instala-y-crea-tu-primer-repositorio-local-con-git-en-windows  
   + https://git-scm.com/book/en/v2/Getting-Started-Git-Basics  

* Working Copy  
   + https://betterexplained.com/articles/a-visual-guide-to-version-control/  
   + https://git-scm.com/book/en/v2/Getting-Started-Git-Basics  

* Staging Area  y Stage Changes
   + https://git-scm.com/book/en/v2/Getting-Started-Git-Basics  
   + https://git-scm.com/about/staging-area  

* Commit Changes  y Commit
   + https://git-scm.com/book/en/v2/Getting-Started-Git-Basics  
   + https://git-scm.com/docs/git-commit  

* Clone  
   + https://git-scm.com/docs/git-clone  

* Pull  
   + https://git-scm.com/docs/git-pull

* Push  
   + https://git-scm.com/docs/git-push

* Fetch  
   + https://git-scm.com/docs/git-fetch

* Merge  
   + https://git-scm.com/docs/git-merge

* Status  
   + https://git-scm.com/docs/git-status

* Log  
   + https://git-scm.com/docs/git-log

* Checkout  
   + https://git-scm.com/docs/git-checkout

* Branch  
   + https://git-scm.com/docs/git-branch

* Tag  
   + https://git-scm.com/docs/git-tag