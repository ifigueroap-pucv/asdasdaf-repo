# Glosario
1. **Control de versiones (VC):** Es una herramienta que permite tener un control de los cambios que se realizan en uno o varios archivos que tiene un proyecto, pudiendo regresarlos a un estado anterior, ver quién modificó por última vez algo que puede estar causando un problema, quién introdujo un error y cuándo, etc.

>[Referencia](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
---
2. **Control de versiones distribuido (DVC):** Son herramientas que, a diferencia de los sistemas de control de versiones centralizados, replican completamente el repositorio, lo cual permite que en caso de caída del servidor, cualquier cliente puede copiar el repositorio en el server para restaurarlo.

>[Referencia](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
---
3. **Repositorio remoto y Repositorio local:** Un repositorio es un sitio centralizado donde se almacena y mantiene información digital. Hablamos de repositorio local cuando las versiones de tu proyecto están almacenadas en tu computador. En cambio, un repositorio remoto es cuando los archivos de tu proyecto están hospedados en algún sitio de Internet, lo cual favorece la colaboración con otras personas. 

>[Referencia 1](https://es.wikipedia.org/wiki/Repositorio#Software)
>[Referencia 2](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos)
---
4. **Copia de trabajo / Working Copy:** Son los archivos de un proyecto que han sido copiados desde un repositorio remoto hacia nuestro computador(mediante comando git clone).

>[Referencia 1](www.thomas-krenn.com/en/wiki/Git_Basic_Terms)
>[Referencia 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
---
5. **Área de Preparación / Staging Area:** Es un archivo que almacena información acerca de lo que va a ir en tu próximo commit. También es llamado índice o **index**.

>[Referencia](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
---
6. **Preparar Cambios / Stage Changes:** Es el paso previo a realizar un commit. Se preparan los archivos que seran añadidos en el próximo commit (mediante comando git add).

>[Referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)
---
7. **Confirmar cambios / Commit Changes:** Una vez preparados los archivos se puede realizar la confirmación de cambios. Esta confirmación se realiza mediante el comando "git commit" y se puede agregar un mensaje para especificar que se ha modificado. 

>[Referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)
---
8. **Commit:** Confirma los cambios que hemos hecho y los registra en el repositorio. Almacena el contenido actual del *Area de Preparación* en un nuevo *commit*, junto con un mensaje que describe los cambios realizados.

>[Referencia](https://git-scm.com/docs/git-commit)
---
9. **clone:** Clona un repositorio en un nuevo directorio. Descarga toda la información de ese repositorio y saca una copia de trabajo de la última versión.

>[Referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
---
10. **pull:** git pull es una abreviación de git fetch seguido de git merge. Lo que hace es bajar los cambios que se han realizado en el repositorio remoto a tu repositorio local.

>[Referencia 1](https://git-scm.com/docs/git-pull)
>[Referencia 2](https://es.stackoverflow.com/questions/245/cuál-es-la-diferencia-entre-pull-y-fetch-en-git)
---
11. **push:** Sube los cambios realizados en el repositorio local al repositorio remoto.

>[Referencia](https://git-scm.com/docs/git-push)
---
12. **fetch:** Descarga los cambios realizados en el repositorio remoto a la rama origin/master sin modificar tu repositorio local. Git fetch puede buscar desde un solo repositorio o URL, o desde varios repositorios a la vez.

>[Referencia 1](https://es.stackoverflow.com/questions/245/cuál-es-la-diferencia-entre-pull-y-fetch-en-git)
>[Referencia 2](https://git-scm.com/docs/git-fetch)
>[Referencia 3](https://es.wikipedia.org/wiki/Git)
---
13. **merge:** Fusiona el contenido descargado con un **fetch** para actualizar y modificar el repositorio local.

>[Referencia 1](https://es.wikipedia.org/wiki/Git)
>[Referencia 2](https://github.com/GarageGames/Torque2D/wiki/Cloning-the-repo-and-working-with-Git)
---
14. **status:** Muestra el estado del los archivos que se encuentran en la index. Este estado puede ser: no rastreado, modificado y staged (añadido al index).

>[Referencia](https://www.siteground.es/kb/tutorial-git-comandos/)
---
15. **log:** Muestra un listado de los **commits** que se han realizado con sus correspondientes detalles.

>[Referencia](https://git-scm.com/docs/git-log)
---
16. **checkout:**  Cambia ramas o restaura archivos del árbol de trabajo para que coincidan con la versión del index.

>[Referencia](https://git-scm.com/docs/git-checkout)
---
17. **Rama / Branch:** Muestra, crea o elimina branches

>[Referencia](https://git-scm.com/docs/git-branch)
---
18. **Etiqueta / Tag:** Es posible listar, crear, eliminar o buscar etiquetas creadas en puntos específicos del historial de proyecto.

>[Referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)
---
 
