
###**a. Cantidad y nombre de los branches**
git branch -a

> (* master)
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/megaramita1
  remotes/origin/ramita1
  remotes/origin/ramita2
  remotes/origin/taller1

* Existen 6 branches cuyos nombres son: *HEAD, master, megaramita1, ramita1, ramita2* y *taller1*
---
###**b. Cantidad y nombre de las etiquetas**

git tag

>asdasdaf-tag
final-final-ahora-si
super-release
tag1
todas-las-tags-al-mismo-commit


* Existen 5 etiquetas, cuyos nombres son: *asdasdaf-tag, final-final-ahora-si, super-release, tag1* y *todas-las-tags-al-mismo-commit*
---
###**c. Nombre, mensaje y c�digo hash de los �ltimos 3 commits. Use el comando �git log� para obtener esta informaci�n.**

>commit a8d1ad0182dfd18c64982dfd6ad88b6ef836a555
Author: Daniel <reptilio.q3@gmail.com>
Date:   Wed Aug 9 20:25:30 2017 -0400

    Se ha agregado el archivo ejercicio3.mdown
---
>commit 5423b43003755f2ff8b197e43ec3725f846b7fc9
Author: Daniel <reptilio.q3@gmail.com>
Date:   Wed Aug 9 20:24:51 2017 -0400

    Se ha agregado el archivo datos.mdown
---
>commit a11ffbb868e7def928f8fa63b73b5a7efc3b0efd
Author: Daniel <reptilio.q3@gmail.com>
Date:   Wed Aug 9 20:23:50 2017 -0400

    se ha eliminado archivo datos.mdown defectuoso
