##Glosario de Términos

 
a. **Control de versiones** (VC): El control de versiones es un sistema para registrar los cambios realizados en uno o varios archivos en el tiempo, para así poder acceder a versiones anteriores de nuestro código en caso de ser necesario. 
[https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones ] 

b. **Control de versiones distribuido** (DVC): El control de versiones distribuido cambia el foco cliente-servidor a uno peer-to-peer, lo que quiere decir que se ya no se accede solo a un repositorio central, si no que en cada peer habrá un repositorio completo, sincronizados entre si. De esta forma los cambios realizados a un mismo archivo se actualizaran en todos los repositorios según corresponda.
[ https://es.wikipedia.org/wiki/Control_de_versiones_distribuido ]

c. **Repositorio remoto y Repositorio local**: Un repositorio es una especie de "carpeta de proyecto " donde se guardan todos los documentos relacionados con el proyecto, incluida la documentación, ademas de tener cada versión de nuestro código.Un repositorio local es simplemente un repositorio alojado en nuestro equipo de trabajo, en cambio, un repositorio remoto es aquel repositorio que esta hosteado en otra parte, pero que de igual forma desde el local se podría acceder a el.

[ https://help.github.com/articles/github-glossary/ ]

d. **Copia de trabajo / Working Copy**: Una copia de trabajo es la información del proyecto que se encuentra dentro del repositorio, sobre la cual trabajamos, por lo tanto, como bien dice su nombre, es la "copia" donde realizamos todas las modificaciones que necesitemos para luego updatearlas al repositorio local mediante el staging area.
e. **Área de Preparación / Staging Area**: El staging area es el lugar previo por donde se prepara nuestra working copy para que en el siguiente commit los cambios se realicen en  versión "original" de nuestro trabajo.
[ https://www.git-tower.com/help/mac/working-copy/stage-changes ]
f. **Preparar Cambios / Stage Changes**:  Se proceden a revisar los cambios, para determinar que cambios estan en "stage"  y cuales no. Esto puede ser realizado de manera bastante prolija mirando los "checkbox" de cada archivo, el cual nos muestra su status actual.

[ https://www.git-tower.com/help/mac/working-copy/stage-changes ]

g. **Confirmar cambios / Commit Changes**: 
h. **Commit**: Comando de git que se usa para grabar los cambios en el repositorio

[ https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push ]
i. **clone**: Es un comando de git utilizado para clonar o copiar un  repositorio previamente seleccionado.
[ https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone ]

j.** pull**: Es un comando de git que tiene por funcion, incluir los cambios de un repositorio remoto en la branch actual.
 [https://git-scm.com/docs/git-pull ] 

k. **push**: Es un comando de git que actualiza las tags junto con objetos asociados.
 [https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push ]

l. **fetch**:  Es un comando de git que se utiliza para descargar objetos y tags desde otro repositorio.

[ https://git-scm.com/docs/git-fetch ]

m. **merge**: Es un comando de git que tiene por funcion unir 2 o mas "historias" en desarrollo

n. **status**: Es un comando de git que se utiliza para mostrar el estado del workiing copy y de la staging area.
[ https://www.atlassian.com/git/tutorials/inspecting-a-repository ]

o.** log**:  Es un comando de git que sirve para mostrar los "logs"(bitacora) de los commit realizados.
[ https://git-scm.com/docs/git-log ]

p. **checkout**:  Es un un comando de git que cambian al ultimo commit hecho en la branch dada.
[ https://stackoverflow.com/questions/15595778/github-what-does-checkout-do ]

q. **Rama / Branch**:  Es un puntero o referencia hacia un commit

[ https://stackoverflow.com/questions/25068543/what-exactly-do-we-mean-by-branch ]

r. **Etiqueta / Tag**: Crea,borra o verifica un objeto taggeado,registrado con GPG
