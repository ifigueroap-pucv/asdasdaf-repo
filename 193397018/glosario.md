a. Control de versiones (VC)
b. Control de versiones distribuido (DVC)
c. Repositorio remoto y Repositorio local
d. Copia de trabajo / Working Copy
e. �rea de Preparaci�n / Staging Area
f. Preparar Cambios / Stage Changes
g. Confirmar cambios / Commit Changes
h. Commit
i. clone
j. pull
k. push
l. fetch
m. merge
n. status
o. log
p. checkout
q. Rama / Branch
r. Etiqueta / Tag

	Desarrollo

a)Es un sistema que permite registrar los cambios que se van realizando sobre uno o mas archivos en una linea de tiempo, para asi poder tener un 
control sobre la versiones que necesites recuperar en un futuro, esto es aplicable a cualquier archivo que se encuentre dentro de tu ordenador.

https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

b)Es un sistema el cual permite a los usuarios tener un control de repositorio de manera que si ocurre algun accidente con el servidor que contiene
el repositorio los demas usuarios que estaban haciendo uso de el pueden hacer una copia del servidor para restaurarlo, es decir, que todos interactuan
entre si tanto el servidor con lso usuarios como entre los propios usuarios.

https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones

c)Los repositorios remotos son versiones de tu trabajo las cuales se encuentran en internet se pueden tener varios repositorios ya sea para solo leer
leer/escribir sirve para compartir archivos de manera en la cual teniendo los permisos necesarios se pueden hacer push o pull de estos archivos ; y un
repositorio local es la manera donde puedes guardar tus archivos de manera personal, en tu ordenador.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

d)Es una copia que se utiliza con el comando git clone para guardar un archivo ya existente.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

e)Se ubica en el directorio GIT, es un archivo que almacena lo que vendra en la siguiente version de tu proyecto.

https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git 

f)Es una accion que lleva a la preparacion de nuevos cambios que luego seran confirmados en el area de de preparacion.

https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git

g)Es la accion que sigue luego de la preparacion de datos solo confirma los datos cambiados para que estos luegos vayan al area de preparacion.

https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git 

h)Hace que los cambios realizados pasen a ser permanentes en el repositorio.

https://git-scm.com/docs/git-commit

i)Este comando permite copiar los datos del repositorio remoto en el local.

https://git-scm.com/docs/git-clone

j)Integra el archivo desde un repositorio remoto, es decir, de una rama remota a una local.

https://git-scm.com/docs/git-pull

k)actualiza las referencias remotas y mis objetos del ambiente de mi trabajo a una rama local mia.

https://git-scm.com/docs/git-push

l)Descarga las referencias y objetos desde un repositorio remoto y los deja en otra rama.

https://git-scm.com/docs/git-fetch

m)Su acccion es unir dos ramas.

https://git-scm.com/docs/git-merge

n)Es una accion que muesetra el estado en el cual se encuentra el directorio y su area de preparacion mostrando los caminos que tienen diferencia con su HEAD.

https://www.atlassian.com/git/tutorials/inspecting-a-repository

o)accion que muestra los registros del commit

https://git-scm.com/docs/git-log

p)Actualiza los archivos del carbol de trabajo para que se pueda cambiar de rama.

https://git-scm.com/docs/git-checkout

q)Al trabajar con el control de versiones este se asemeja a un arbol de trabajo el cual contiene su tronco la rama principal mientras que las otras
se llaman branch.

https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell

r)Se utiliza para etiquecar o darle un orden a las versiones.

https://git-scm.com/book/en/v2/Git-Basics-Tagging