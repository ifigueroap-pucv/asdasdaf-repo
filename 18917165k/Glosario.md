#Glosario
*1. Control de versiones (VC):* Es una combinación de tecnologias para controlar los cambios realizados en los ficheros de un determinado proyecto.
 
http://producingoss.com/es/vc.html

*2. Control de versiones distribuido (DVC):* Este permite que muchos desarrolladores de software trabajen en un proyecto comun, en lugar de un unico repositorio central en el cual los clientes se sincronizan, la copia local del codigo base de cada usuario es un repositorio completo.
 
https://es.wikipedia.org/wiki/Control_de_versiones_distribuido

*3. Repositorio remoto y Repositorio local:* Un repositorio remoto tiene relacion con aquellas versiones del proyecto que se encuentran alojadas en la red, este es el medio por el cual los desarrolladores trabajan colaborativamente en los DVC, en cambio el repositorio local es aquella copia o referencia de un repositorio remoto en particular, este se encuentra en el ordenador del usuario, por lo que es accesible sin necesidad de estar conectado a una red.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
 
*4. Copia de trabajo :* Corresponde a todos los trabajos involucrados, aquellos que son almacenados dentro de nuestro disco y en ello es posible realizar modificaciones a los archivos.

https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
 
*5. Área de Preparación / Staging Area:* Se almacena informacion relacionada con los cambios de los archivos del proyecto para modificar en un commit.

https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git

*6. Preparar Cambios / Stage Changes:* Momento en que los archivos se encuentran listos para realizar la modificacion.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

*7. Confirmar cambios / Commit Changes:* Estado en el que se confirma la modificacion de archivos encontrados en el Stage.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
 
*8. Commit:* Cambio realizado en el repositorio luego de almacenarlos en el area de preparacion

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

*9. clone:* Se realiza una copia de la ultima version de un repositorio ya existente

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git

*10. pull:* Este es un comando que permite obtener los cambios realizados en el repositorio remoto  actualizando el repositorio local uniendolos (merge).

https://git-scm.com/docs/gitglossary

*11. push:* Este comando permite actualizar el repositorio remoto segun lo modificado en el local.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

*12. fetch:* Este comando permite recuperar toda la informacion enviada al repositorio, obtiene los cambios realizados en un repositorio remoto.

https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

*13. merge:* Este comando permite fusionar o juntar dos ramas, actualizando los cambios de uno a otro.

https://git-scm.com/docs/gitglossary

*14. status:* Muestra la lista de los archivos que estan siendo modificados, agregados o cambiados.

https://git-scm.com/docs/git-status

*15. log:* Este comando enlista los commits realizados en una rama, junto a los detalles de quienes realizaron ese cambio.

https://git-scm.com/docs/gitglossary

*16. checkout:* Este comando es utilizado para crear una nueva rama o tambien para moverse de una rama a otra.

https://git-scm.com/docs/git-checkout 

*17. Rama / Branch:* Las ramas son utilizadas para manejar el control de versiones, cada rama es un historial de modificaciones del proyecto, el comando git branch sirve para listar todas las ramas en el repositorio.

https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F


*18. Etiqueta / Tag:* Son utilizados por git para marcar commits especificos dentro de la historia de un proyecto. 

https://git-scm.com/book/en/v2/Git-Basics-Tagging