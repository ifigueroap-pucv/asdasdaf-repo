##Glosario ##

* **Control de versiones (VC): ** Es un control de vesion que regista los cambio porducidos en un archivo, mediante se va avansando en él y permite recuperar versiones anteriores.
*  **Control de versiones distribuido (DVC):**Resuelve los problemas que mencionan con anterioridad.Los clientes solo pueden descargar la ultima version instantanea del archivo. Ademas si un servidor se cae y estos sistemas colaboran con el puede copiar copiarse hacia el servidor para poder restaurarlo.
*  **Repositorio remoto y Repositorio local:**la reposicion remota son versiones de tus proyecto que se encuentran en internet, se pueden encontrar tanto en modo lectura como lectura/escritura dependiendo de los permisos que tenga.
El repositorio local es la residencia completa del VC de un archivo o proyecto, en un equipo o servidor local.
*  **Copia de trabajo / Working Copy:**Es una carpeta o documento que es copia del archivo con el que se está trabajando en el VCS, donde se trabajan cambios.
*  **Área de Preparación / Staging Area:**Es un archivo, generalmente ubicado en el directorio Git, que almacena información acerca de lo que irá en la próxima versión, en el flujo de trabajo este se encuentra entre el directorio de trabajo y el repositorio.
*  **Preparar Cambios / Stage Changes:**Muestras lo cambios realizados en el repositorio locarl y que se encuentran listo para subir.
*  **Confirmar cambios / Commit Changes:** realizar algunos cambios y confirmar instantáneas de esos cambios en el repositorio cada vez que el proyecto alcance un estado que quieras conservar.
*  **Commit:**Envío de cambios locales al repositorio. Como resultado cambia la versión del archivo(s)/repositorio.
*  **clone:**Clona un directorio o repositorio a uno nuevo. Junto con esto crea las ramas y divisioes necesarias para la integracion de futuras modificciones.
*  **pull:**Toma parte de la informacion de un repositorio que se encuentra en linea y luego lo fusioona con el repositorio local.
*  **push:**Toma los cambios locaes realizados y estos los sube al repositotio en linea o al repositorio remoto.
*  **fetch:**Descarga el estado actual de un determinado repositotio remoto sin modificar los datos del repositorio local.
*  **merge:**Fusiona las modificaciones de otra rama con la rama de trabajo actual del repositorio.
*  **status:**Señala las rutas de los directorios que tienen diferencias entre el archivo indice y el commit HEAD Actual.
*  **log:**Muestra la modificaciones que se han han realizado en los directorios. Muestra los registros de confirmacion.
*  **checkout:**Cambia parte de los branches en caso que no coincida con la version indice o un determinado arbol.
*  **Rama / Branch**Es una forma de definir una confirmacion a un directorio especifico. Con lo anterior es posible poder acceder a directorios secundarios llegando a una organizacion y estructura de los directorios.
*  **Etiqueta / Tag:**Deja registro de los estados de cambios importantes realizados en los repositorios.

[Fuente](https://git-scm.com/book/en/v2)
