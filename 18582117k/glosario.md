# Glosario:
---
**a. Control de versiones (VC):** "Control de Versiones (VC)" es un sistema que guarda las modificaciones realizadas en un archivo o grupo de archivos a través del tiempo. De modo que uno pueda recuperar versiones específicas en un futuro. También te permite devolver archivos o el proyecto entero a una etapa pasada, comparar modificaciones lo largo del tiempo, observar quién hizo una modificación por última vez, en algo que puede estar causando fallos, quién introdujo un error o errores y cuándo, y otras cosas más. Usar un VCS también significa normalmente que si extravias archivos, puedes recuperarlos fácilmente. Además, obtienes todos estos beneficios a un coste muy bajo. Ademas "Control de Versiones (VC)" puede funcionar a través de tres distintos metodos (LVC, CVC y DVC) : 
* **1 Control de versiones Locales (LVC):** El cual es un metodo bastante usando al ser un enfoque muy simple, pero con un gran tendensia a errores, al ser facil de olvdiar en que directorio uno se encuentra o sobreescibir archivos.

![asdfasdf][imagen2]

*  **2 Control de versiones Centralizados (CVC):** Un metodo que fue hecho para solucionar el problema que tenia la gente para realizar sistemas cetralizados. Estos "Centralized Version Control Systems (CVS)" , tienen un único servidor que comprende todos los archivos, y diversa cantidad de clientes que descargan los archivos desde ese nucleo.

![asdfasdf][imagen3]

**b. Control de versiones Distribuidos (DVC):** Metodo que a diferencia del (CVC) anterior, no solo los usuarios (clientes) pueden descargar en forma instantánea los archivos, también pueden objetar el repositorio.

![asdfasdf][imagen1]

**c. Repositorio remoto y Repositorio local:** Un repositorio es el almacenamietno de un proyecto en forma virtual, el cual permite guardar versiones de su código, a las que se puede acceder cuando sea uno desee.

* **Repositorio Remoto:** Es la version/es de tu proyecto o trabajo el cual se localiza en algún lugar de la red. Se pueden tener varios, que puede ser de sólo lectura, o de lectura y escritura, según los permisos que se hayan dado. También compartir con otros implica gestionar estos repositorios remotos, y mandar (push), recibir (pull) datos de estos cuando se necesite intercambiar "objetos", saber cómo agregar repositorios nuevos, eliminar aquellos que ya no son protegidos, administrar ramas remotas y señalar si están en seguimiento o no.

* **Repositorio Local:** Es aquel proyecto trabajado en nuestro ordenador, donde realizamos cambios locales.

**d. Copia de trabajo / Working Copy:** Es un "clon" del comando "git clon" que representa un repositorio. Y crease una copia de trabajo, se utiliza el comando "git clone /path/to/repository"

![asdfasdf][imagen4]

**e. Área de Preparación / Staging Area:** Es un archivo, normalmente guardado en el directorio de Git, que almacena información sobre lo que se pasará al próximo repositorio. También se le conoce como "índice", pero es común referirse a él como área de clasificación.

![asdfasdf][imagen5]

**f. Preparar Cambios / Stage Changes:** Esto es tras haberse realizado cambio/s en el proyecto y estar listo para guerdarlo/s. Los cual se hacer con el comando "git add"

**g. Confirmar cambios / Commit Changes:** La confirmación de los cambios, esta conectada a los cambios preparados. Ya que tras haberse realizados los cambios, preparase con el comando "git add", se guardan en el historial de versiones del proyecto con el comando "git commit".

![asdfasdf][imagen6]

**h. Commit:** El comando commit o "git commit" guarda "staged snapshot" en el historial del proyecto. Las "staged snapshot" comprometidos pueden considerarse versiones "seguras" de un proyecto. Ademas nunca se cambiaran a menos que estrictamente se le pida. 

**i. Clone:** El comando "git clone", clona o copia un repositorio en un nuevo directorio.

**j. Pull:** Pull o "git pull", integra o obtiene con otro repositorio o una área local.

**k. Push:** Push o "git push", actualiza objetos remotos con objetos asociados o locales.

**i. Fetch:** Fetch o "git fetch", descarga objetos y los asocia con otro repositorio.

**m. Merge:** Merge o "git merge", une uno o mas historiales de desarrollo deversiones del proyecto en uno. 

**n. Status:** Statuts o "git status", muestra el estado del árbol de trabajo.

**o. Log:** Log o "git log", muestra los registro de confirmación.

**p. Checkout:** Checkout o "git checkout", restaura árboles de trabajo o intercambia las ramas. 

**q. Rama / Branch:** Branch o "git branch", crea, elimina o alista las ramas.

**r. Etiqueta / Tag:** Confirma, alista, crea o elimina un objeto etiquetado firmado con GPG. 

**Fuentes:**
 [Fuente 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones), [Fuente 2](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos), [Fuente 3](https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts), [Fuente 4](https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms), [Fuente 5](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics), [Fuente 6](https://www.atlassian.com/git/tutorials/saving-changes), [Fuente 7](https://git-scm.com/docs/git-clone), [Fuente 8](https://git-scm.com/docs/git-pull), [Fuente 9](https://git-scm.com/docs/git-push)
[Fuente 10](https://git-scm.com/docs/git-fetch), [Fuente 11](https://git-scm.com/docs/git-merge), [Fuente 12](https://git-scm.com/docs/git-status), [Fuente 14](https://git-scm.com/docs/git-log), [Fuente 15](https://git-scm.com/docs/git-checkout), [Fuente 16](https://git-scm.com/docs/git-branch), [Fuente 17](https://git-scm.com/docs/git-tag)
 
 
[imagen1]:https://git-scm.com/figures/18333fig0103-tn.png
[imagen2]:https://git-scm.com/figures/18333fig0101-tn.png
[imagen3]:https://git-scm.com/figures/18333fig0102-tn.png
[imagen4]:https://www.thomas-krenn.com/de/wikiDE/images/e/ee/Distritbuted-repo-en.png
[imagen5]:https://git-scm.com/book/en/v2/images/areas.png
[imagen6]:https://wac-cdn.atlassian.com/dam/jcr:0f27e004-f2f5-4890-921d-65fa77ba2774/01.svg?cdnVersion=fp
