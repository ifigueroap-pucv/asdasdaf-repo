a) Control de versiones: Permite almacenar los cambios que sufre un archivo o conjunto de archivos, con el fin de poder volver a alg�n punto especifico en cualquier momento.
	Fuente: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
b) Control de versiones distribuidos: Esta tecnologia permite trabajar a distintas personas en un mismo proyecto, sin necesidad de encontrarse en el mismo lugar fisico, de manera controlada y ordenada.
	Fuente: https://es.wikipedia.org/wiki/Control_de_versiones_distribuido
c) Repositorio remoto y local: Son versiones de un proyecto, que se pueden alojar ya sea en internet(remoto) o en el ordenador personal(local)
	Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
d) Copia de trabajo: Es el �rea donde estaremos trabajando localmente, para luego enviar los cambios hechos a que se almacenen.
	Fuente: http://rogerdudler.github.io/git-guide/index.es.html
e) �rea de preparaci�n: Es una zona intermedia entre el Working Copy y el Stage Changes.
	Fuente: http://rogerdudler.github.io/git-guide/index.es.html
f) Preparar cambios: Es la zona final que apunta al �ltimo commit realizado, antes de ser env�ados al repositorio remoto.
	Fuente: http://rogerdudler.github.io/git-guide/index.es.html
g) Confirmar cambios: 
h) Commit: Hacer un commit es generar una version del proyecto a la que podremos acceder posteriormente.
	Fuente: http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html#terminologia-importante
i) Clone: Sirve para crear una copia de un proyecto GIT ya existente
	Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git
j) Pull: Permite traer los ultimos cambios de otro repositorio.
	Fuente: http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html#trayendo-cambios-pull
k) Push: Permite transferir los cambios a un repositorio remoto.
	Fuente: http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html
l) Fetch: Git fetch trae los cambios al igual que pull, pero este los deja en otro branch.
	Fuente: https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git
m) Merge: Permite fusionar una rama del proyecto
	Fuente: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
n) Status: 
o) Log:
p) Checkout: 
q) Branch: Es una copia independiente del proyecto, que permite ramificar el proyecto para probar por ejemplo nuevas funcionalidades
	Fuente: http://blog.santiagobasulto.com.ar/programacion/2011/11/27/tutorial-de-git-en-espanol.html#branch-merge
r) Tag: Permite indicar en la historia del proyecto, informaci�n importante como la version por ejemplo
	Fuente: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
