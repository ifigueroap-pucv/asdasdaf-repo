#Glosario
**a. Control de versiones (VC)**
 : Corresponde a un Sistema que permite mantener un registro de los cambios realizados sobre un archivo o un conjunto de archivos a lo largo del tiempo, dando la posibilidad a los desarrolladores de recuperar versiones anteriores del mismo en cualquier momento que se requiera.
 [Referencia][1]
 
**b. Control de versiones distribuido (DVC)**
: Corresponde a aquellos Sistemas de control de versiones que permiten a varios desarrolladores trabajar en un proyecto en com�n sin necesidad que se encuentren en la misma red, debido a que cada uno de ellos realiza una copia del repositorio remoto en el cual se encuentran trabajando colaborativamente a su repositorio local. Esto permite que cualquiera de los colaboradores realice una recuperaci�n del proyecto en caso de que repositorio remoto muera.
[Referencia][1]
 
**c. Repositorio remoto y Repositorio local**
: Repositorio remoto corresponde a aquellas versiones de nuestros proyectos que se encuentran alojados en la red, es el medio por el cual los desarrolladores trabajan colaborativamente en los DVC S, se encuentran sujetos a permisos de lectura/escritura y a su vez son gestionados a trav�s del uso de una serie de comandos.

: Repositorio local es aquella copia o referencia de un repositorio remoto en particular. A diferencia del repositorio remoto el repositorio local se encuentra en nuestro ordenador, es decir, es accesible sin necesidad de estar conectado a una red.
[Referencia][2]
 
**d. Copia de trabajo / Working Copy**
: Corresponde a la copia que se encuentra en nuestro disco de la versi�n de un proyecto. En ella es posible realizar modificaciones a los archivos.
[Referencia][3]
 
**e. �rea de Preparaci�n / Staging Area**
: Es un archivo que almacena la informaci�n referente a lo que se va a modificar en la pr�xima confirmaci�n (*git commit*).
[Referencia][3]
 
**f. Preparar Cambios / Stage Changes**
: Estado en el cual los archivos se encuentran preparados para ser modificados, pero que sin embargo, a�n no han sido confirmada su modificaci�n.
[Referencia][12]

 
**g. Confirmar cambios / Commit Changes**
: Estado en el cual se confirma la modificaci�n de los archivos que se encuentran en el Stage.
[Referencia][12]

 
**h. Commit:**
: Comando que permite confirmar los cambios realizados en el repositorio, junto a un mensaje que detalla lo realizado en los cambios.
[Referencia][5]
 
**i. clone**
: Para obtener una copia de la �ltima versi�n de un proyecto existente se hace uso del comando *git clone url *, este comando descarga toda la informaci�n del repositorio al cual hace referencia la url ingresada.
[Referencia][4]
 
**j. pull**
: El comando *git pull* permite actualizar la informaci�n del proyecto clonado desde Git y lo intenta unir con el c�digo con el cual se est� trabajando localmente. Es equivalente a realizar *git fetch* seguido de un *git merge*.
[Referencia][2]
 
**k. push**
: El comando *git push* permite actualizar el repositorio remoto seg�n lo modificado en el repositorio local.
[Referencia][2]
 
**l. fetch**
: El comando *git fetch* recupera toda la informaci�n enviada al repositorio remoto desde que fue clonado o desde la �ltima vez que fue ejecutado el comando *git fetch*.
 [Referencia][2]
 
**m. merge**
: El comando *git merge* permite fusionar ramas del working tree.
[Referencia][8]
 
**n. status**
: El comando *git status* muestra el estado actual en el que se encuentran los archivos en los cuales se est� trabajando dentro del working tree. 
[Referencia][10]
 
**o. log**
: El comando *git log* enlista los commits realizados por los desarrolladores al repositorio remoto. 
[Referencia][6]
 
**p. checkout**
: El comando *git checkout* permite saltar de una rama a otra, o restaurar archivos del �rbol de trabajo.
[Referencia][7]
 
**q. Rama / Branch**
: Particularmente Git maneja el control de versiones de los proyectos mediante el uso de ramas independientes entre s�, las ramas corresponden al historial de modificaciones de los proyectos. El comando *git branch* permite listar, crear o eliminar ramas.
[Referencia][9]
 
**r. Etiqueta / Tag**
: Los tag son utilizados por Git para marcar puntos espec�ficos dentro de la historia de un proyecto, es posible manipularlas mediante el comando *git tag*
[Referencia][11]
 
[1]:https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones 
[2]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
[3]: https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git
[4]: https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obten
[5]: https://git-scm.com/docs/git-commit 
[6]: https://git-scm.com/docs/git-log 
[7]: https://git-scm.com/docs/git-checkout 
[8]: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar
[9]: https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F
[10]: https://git-scm.com/docs/git-status
[11]: https://git-scm.com/book/en/v2/Git-Basics-Tagging
[12]: https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository
