a. Control de versiones (VC): Permite mantener un historial de los cambios realizados en el codigo y que no se pierdan cambios hechos anteriormente
b. Control de versiones distribuido (DVC): Es un sistema donde los repositorios estan copiados en otros computadores de modo que si hay alguna perdida del servidor se pueda restaurar desde algun computador 
c. Repositorio remoto y Repositorio local: Repositorio remoto es un disco en la nube que contiene todos los archivos enviados desde un repositorio local, y repositorio local son los archivos almacenados en el computador en el que se trabaja
d. Copia de trabajo / Working Copy: Es una copia del trabajo hecho con su ultima version dentro del git
e. �rea de Preparaci�n / Staging Area: Es un archivo el cual contiene algo asi como los pasos a seguir o un tipo de indice acerca de la proxima confirmacion a realizar
f. Preparar Cambios / Stage Changes: Es lo que uno hace realizando un git add lo que provoca que se prepara el archivo para ser cambiado en el repositorio remoto
g. Confirmar cambios / Commit Changes: Se confirma el cambio preparado haciendo el git add ahora con un commit para confirmar estos cambios
h. Commit: Sirve para comentar y guardar cambios 
i. clone: Copia repositorio remoto hacia el local
j. pull: Actualiza ficheros que se tienen
k. push: Envia archivos desde el repositorio local al remoto
l. fetch: Baja los cambios desde el repositorio a una rama fantasma para que se pueda revisar que cambios hubo respecto a lo que se tiene
m. merge: Luego del fetch se le hace un git mergue para que este copie los archivos desde la rama fantasma al repositorio local
n. status: Es para ver los archivos listos para hacer commit 
o. log: Muestra los ultimos commit hechos
p. checkout: Actualiza los archivos en la rama para que estos coincidan entre si 
q. Rama / Branch: Es un puntero hacia una confirmacion de un cambio de archivos
r. Etiqueta / Tag: Revisa las etiquetas dentro del repositorio 