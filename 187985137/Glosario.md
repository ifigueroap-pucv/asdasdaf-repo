##Glosario Ing. Web 


**Control de versiones:**
Lo definiremos como un de sistema de control de versiones que nos permite guardar �copias� del estado de tu proyecto en ese 
instante del tiempo, d�ndote la capacidad de recuperar ese estado en cualquier momento. 
Es simple: tomas una de est�s �fotos�, trabajas en tu proyecto y si algo sale falla y no se puede reparar este error, puedes volver 
a atr�s, a alg�n punto donde todo estaba operando.
[link de referencia](https://hipertextual.com)

**Control de versiones distribuido:**
En programaci�n, el control de versiones distribuido nos da la facilidad  de trabajar a  muchos desarrolladores de software  
en un proyecto com�n sin necesidad de compartir una misma red.
 Las revisiones son guardadas en un sistema de control de versiones distribuido (DVCS, por sus siglas en ingl�s).
[link de referencia](https://es.wikipedia.org/wiki/Control_de_versiones_distribuido)

**Repositorio remoto y Repositorio local:**
Para poder trabajar en conjunto  en cualquier proyecto Git, necesitaremos  saber c�mo gestionar tus repositorios remotos. 
Los repositorios remotos son nuestras versiones de tu proyecto que se encuentran alojados en Internet o en alg�n punto de la red. 
Podremos tener varios, cada uno de los cuales puede ser de s�lo lectura, o de lectura/escritura, seg�n los permisos que tengas.
Colaborar con otros implica gestionar estos repositorios remotos, y mandar (push) y recibir (pull) datos de ellos cuando necesites compartir cosas
[link de referencia](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)

**Copia de trabajo:**
Primero que nada hay que tener en claro que tener una copia de trabajo o seguridad es de suma importancia pero �Porque?.
Disponer de copias de seguridad de nuestra web puede salvarnos de m�s de una situaci�n complicada.dado que son registros de nuestro trabajo  
A pesar de la importancia que tienen las copias de seguridad, (creo que) nunca hemos hablado de c�mo puedes hacerlas.
Existen diferentes m�todos para realizar una copia de seguridad. Como casi todo en WordPress, algunas soluciones las encontraremos en el ecosistema de plugins:
BackupBuddy. Un buen plugin crear y gestionar copias de seguridad, recomendado por uno de nuestros WProfesionales, Joan Boluda.
WPBackup. Otro plugin para planificar la creaci�n de copias de seguridad de nuestros ficheros y base de datos.
WordPress Backup to Dropbox. Este plugin es interesante porque la copia de seguridad de tu site se env�a autom�ticamente a tu cuenta de Dropbox, una soluci�n r�pida y c�moda para instalaciones de WordPress peque�as.
Recuerda que jam�s deber�as guardar la copia de seguridad de un sistema en el propio sistema, as� que hacerlo en Dropbox es una buena idea
[link de referencia](https://neliosoftware.com/es/blog/copias-de-seguridad-con-git/)

**Area de preparacion:**
 La area de preparacion o preparacion interactiva viene dentro de Git que trae incluidos unos cuantos scripts para facilitar algunas de las tareas en la l�nea de comandos. 
Se van a mostrar unos pocos comandos interactivos que suelen ser de gran utilidad a la hora de recoger en una confirmaci�n de cambios solo ciertas combinaciones y partes de archivos.
Estas herramientas son �tiles, por ejemplo, cuando se modifican unos cuantos archivos y luego se decide almacenar esos cambios en una serie de confirmaciones 
de cambio focalizadas en lugar de en una sola confirmaci�n de cambio entremezclada. 
[link de referencia](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Preparaci%C3%B3n-interactiva)

**Preparar cambios:**
Arreglar un archivo es simplemente prepararlo finamente para un commit. 
Git, con su �ndice le permite comprometer s�lo ciertas partes de los cambios que ha hecho desde el �ltimo commit.
Digamos que usted est� trabajando en dos caracter�sticas - una est� terminado, y uno todav�a necesita un poco de trabajo hecho.
Te gustar�a hacer un compromiso y volver a casa (�5, finalmente!), Pero no le gustar�a cometer las partes de la segunda caracter�stica, que no se ha hecho todav�a.
Escoge las partes que conoce pertenecen a la primera caracter�stica y conf�rmelas. 
Ahora su commit es su proyecto con la primera caracter�stica hecha, mientras que el segundo es todav�a en el trabajo en progreso en su directorio de trabajo
[link de referencia](https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git)

**Confirmar cambios:**
Para eliminar archivos de Git, debes eliminarlos de tus archivos rastreados (o mejor dicho, eliminarlos del �rea de preparaci�n) y luego confirmar. 
Para ello existe el comando git rm , que adem�s elimina el archivo de tu directorio de trabajo de manera que no aparezca la pr�xima vez como un archivo no rastreado.
[link de referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)

**Commit:**
Git-commit - Registra cambios en el repositorio 
Almacena el contenido actual del �ndice en un nuevo commit junto con un mensaje de registro del usuario que describe los cambios.
[link de referencia](https://git-scm.com/docs/git-commit)

**Clone:**
Git-clone - Clonar un repositorio en un nuevo directorio
Clona un repositorio en un directorio creado recientemente, crea ramas de seguimiento remoto para 
cada rama en el repositorio clonado (visible mediante git branch -r ) y crea y extrae una 
rama inicial que se bifurca de la rama actualmente activa del repositorio clonado.
[link de referencia](https://git-scm.com/docs/git-clone)

**Pull:**
Git-pull - Obtener de e integrar con otro repositorio o una sucursal local
Incorpora cambios desde un repositorio remoto a la sucursal actual. En su modo predeterminado, git pull es una abreviatura para git fetch seguido por git merge FETCH_HEAD .
M�s precisamente, git pull ejecuta git fetch con los par�metros dados y llama a git merge para combinar los cabezales de ramificaci�n recuperados en la rama actual. 
Con --rebase , ejecuta git rebase en lugar de git merge .
[link de referencia](https://git-scm.com/docs/git-pull)

**Push:**
Git-push - Actualizar refs remotos junto con objetos asociados
Actualiza las refs remotas usando refs locales, mientras env�a objetos necesarios para completar las refs dadas
[link de referencia](https://git-scm.com/docs/git-push)

**Fetch:**
Git-fetch - Descarga objetos y refs desde otro repositorio
Obtener ramas y / o etiquetas (colectivamente, "refs") de uno o m�s repositorios, junto con los objetos necesarios para completar sus historias. 
Las ramas de seguimiento remoto se actualizan (consulte la descripci�n de <refspec> a continuaci�n para saber c�mo controlar este comportamiento).
[link de referencia](https://git-scm.com/docs/git-fetch)

**Merge:**
Git-merge - �nete a dos o m�s historias de desarrollo juntas
Incorpora cambios de los compromisos nombrados (desde el momento en que sus historias divergieron de la rama actual) en la rama actual. 
Este comando es usado por git pull para incorporar cambios de otro repositorio y puede ser usado a mano para combinar los cambios de una rama a otra.
[link de referencia](https://git-scm.com/docs/git-merge)

**Status:**
Git-status - Muestra el estado del �rbol de trabajo
Muestra las rutas que tienen diferencias entre el archivo de �ndice y el commit HEAD actual, las rutas que tienen diferencias
entre el �rbol de trabajo y el archivo de �ndice y las rutas en el �rbol de trabajo que no son rastreadas por Git 
La primera es lo que se comprometer�a ejecutando git commit ; El segundo y el tercero son lo que podr�as cometer ejecutando git add antes de ejecutar git commit .
[link de referencia](https://git-scm.com/docs/git-status)

**Log:**
Git-log - Mostrar registros de confirmaci�n
Muestra los registros de confirmaci�n.
El comando toma las opciones aplicables al comando git rev-list para controlar lo que se muestra y c�mo y las opciones aplicables a los comandos git diff-* 
para controlar c�mo se muestran los cambios que introduce cada commit.
[link de referencia](https://git-scm.com/docs/git-log)

**Checkout:**
Git-checkout - Cambiar ramas o restaurar archivos de �rbol de trabajo
Actualiza los archivos del �rbol de trabajo para que coincidan con la versi�n del �ndice o del �rbol especificado. 
Si no se da ninguna ruta, git checkout tambi�n actualizar� HEAD para establecer la rama especificada como la rama actual.
[link de referencia](https://git-scm.com/docs/git-checkout)

**Rama:**
Supongamos que tienes una carpeta con tres archivos, que preparas (stage) todos ellos y los confirmas (commit).
Al preparar los archivos, Git realiza una suma de control de cada uno de ellos (un resumen SHA-1, tal y como se mencionaba en el cap�tulo 1), almacena una copia de 
cada uno en el repositorio (estas copias se denominan "blobs"), y guarda cada suma de control en el �rea de preparaci�n (staging area)
[link de referencia](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)

**Etiqueta:**
Como muchos VCS, Git tiene la posibilidad de etiquetar puntos espec�ficos del historial como importantes. 
Esta funcionalidad se usa t�picamente para marcar versiones de lanzamiento (v1.0, por ejemplo). 
En esta secci�n, aprender�s c�mo listar las etiquetas disponibles, c�mo crear nuevas etiquetas y cu�les son los distintos tipos de etiquetas.
[link de referencia](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)











