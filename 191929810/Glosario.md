
1.-complete un glosario con la definici�n de los siguientes conceptos o t�rminos
t�cnicos asociados a git. El glosario debe ser una lista itemizada, con los t�rminos y
sus definiciones.

a. Control de versiones (VC):
	El control de versiones es un sistema que registra los cambios realizados sobre un archivo 
	o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones espec�ficas m�s adelante
	(https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

b. Control de versiones distribuido (DVC):
	El constrol de versiones distribuido es un sistema los clientes no s�lo descargan la �ltima instant�nea de los archivos: replican 
	completamente el repositorio. As�, si un servidor muere, y estos sistemas estaban colaborando a trav�s de �l, cualquiera de los 
	repositorios de los clientes puede copiarse en el servidor para restaurarlo
	(https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

c. Repositorio remoto y Repositorio local :
	Los repositorios remotos son nuestras versiones de tu proyecto que se encuentran alojados en Internet o en alg�n punto de la red.

d. Copia de trabajo / Working Copy:
	La copia de trabajo es la copia local de los ficheros de un repositorio, en un momento del tiempo o revisi�n espec�ficos.
	Todo el trabajo realizado sobre los ficheros en un repositorio se realiza inicialmente sobre una copia de trabajo, de ah� su nombre

e. �rea de Preparaci�n / Staging Area:
	Es un archivo que representa el area intermedia entre el directorio que contiene los archivos a los que se aplica cambios,
	 y el repositorio, guarda la informaci�n sobre lo que se ha cambiado antes de ser confirmado y enviado al repositorio.

f. Preparar Cambios / Stage Changes:
	Son los cambios que se raelizan el repositorio antes de aplicar el comando "commit".

g. Confirmar cambios / Commit Changes:
	Los cambios en el repositorio son se hacen permanentes utilizando el comando "commit"

h. Commit:
	Un commit sucede cuando una copia de los cambios hechos a una copia local es escrita o integrada sobre el repositorio.

i. clone:
	Clona un repositorio en el directorio indicado

j. pull:
	A�ade los cambios realizados en el repositorio remoto dentro de la rama actual.
	
k. Push: Actualiza las referencias remotas y rodos los objetos asociados.
	(https://git-scm.com/docs/git-push)

l. Fetch: Descarga los objetos y las referencias desde otro repositor.
	(https://git-scm.com/docs/git-fetch)

m. Merge: Se usa para unir historias de desarrollo
	(https://git-scm.com/docs/git-merge)

n. Status: Muestra el area de preparaci�n en los que se esta trabajando.
	(https://www.atlassian.com/git/tutorials/inspecting-a-repository)

o.Log: Muestra los registros de los commits
	(https://git-scm.com/docs/git-log)

p. Checkout: se usa para cambiar crear una nueva rama o salatar a ella automaticamente 
	(https://git-scm.com/docs/git-checkout)

q. Rama/Branch: Es un puntero de bajo peso hacia un commit.
	(https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

r. Etiqueta/Tag: Es una rama que se usa para etiquetar versiones.
	(https://git-scm.com/book/en/v2/Git-Basics-Tagging)
