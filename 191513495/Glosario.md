* Glosario *

A. Control de versiones (VC): Control de versiones ayuda a tener el c�digo respaldado, guardando el c�digo en diferenctes ubicaciones, para as� evitar problemas con la p�rdida del c�digo

B. Control de versiones distribuido (DVC): El control de versiones permite hacer cambios de manera m�s f�cil, ya que fusiona facilmente los cambios hechos dentro de un equipo de trabajo usando comentarios.

C. Repositorio remoto y Repositorio local : Es una carpeta donde se encuentran los archivos de un proyecto (un historial del mismo), el cual tendr� un seguimiento de los cambios realizados a trav�s del tiempo.

D. Copia de trabajo / Working Copy: Es una representaci�n de la compilaci�n de un disco.

E. �rea de Preparaci�n / Staging Area: Es un �rea intermedia de almacenamiento de datos que es utilizada para el procesamiento de los mismos durante procesos de extracci�n, transformaci�n y carga. 

F. Preparar Cambios / Stage Changes: Es una preparaci�n de cambias que se efect�a al ejecutar el comando git add.

G. Confirmar cambios / Commit Changes: Esto tiene que ver con los archivos que fueron a�adidos o modificados y se debe ejecutar el comando git add para confirmar los cambios hechos.

H. Commit: Identifica los cambios realizados a una rama de trabajo, en el repositorio local.

I. Clone: Copia todo lo que est� en un repositorio remoto al repositorio local que uno tiene.

J. Pull: Comando para actualizar el repositorio local.

K. Push: Comando para actualizar los cambios contenidos en el repositorio local al repositorio remoto.

L. Fetch: Copia los cambios, pero dej�ndolos en otra rama.

M. Merge: Fusiona una o m�s ramas dentro de la rama que se tiene activa.

N. Status: Muestra los diferentes estados de los archivos en el directorio de trabajo. Muestra que archivos est�n modificados, con y sin seguimiento, pero no confirmados a�n.

O. Log: Muestra el historial registrado de un proyecto desde el guardado m�s reciente realizado.

P. Checkout: Comando para ir de una rama a otra rama.

Q. Rama / Branch: Herramienta para la gesti�n de ramas.

R. Etiqueta / Tag: Funciona como marcador a alg�n punto espec�fico del historial del c�digo fuente.