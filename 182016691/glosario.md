
Glosario de términos
===================

 1. **Control de versiones** (VC): El control de versiones es un sistema que registra los cambios realizados a nuestros archivos a lo largo del tiempo, gracias a esto podemos recuperar versiones anteriores de estos archivos cuando sea necesario. [ https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones ]
 2. **Control de versiones distribuido** (DVC):  el control de versiones distribuido consiste en que cada cliente tiene una copia completa del repositorio original con todos sus cambios actualizados por lo cual se podría decir que cada clon es un backup de todos los datos. [ https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones ]
 3. **Repositorio remoto y Repositorio local**: un repositorio remoto es un repositorio que esta alojado en internet o en una red , en cambio un repositorio local esta alojado en nuestro pc. [ https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes ]
 4. **Copia de trabajo / Working Copy**: es la copia local de un repositorio remoto , este nos permite realizar modificaciones en sus archivos de manera local para así al finalizar estas poder actualizar el repositorio remoto. [ https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms ]
 5. **Área de Preparación / Staging Area**: es un archivo que esta contenido en el directorio de git, el cual guarda información sobre lo que ira dentro de nuestro siguiente commit. [ https://git-scm.com/book/en/v2/Getting-Started-Git-Basics ] 
 6. **Preparar Cambios / Stage Changes**: es la acción de añadir los cambios de nuestros archivos para que así en el siguiente commit estos se actualizen en el repositorio. [ https://githowto.com/staging_changes ]
 7. **Confirmar cambios / Commit Changes**: es la acción de realizar un commit para añadir los cambios al repositorio, los cuales fueron preparados anteriormente. [ https://githowto.com/commiting_changes ]
 8. **Commit**: se refiere a un cambio individual en un archivo o archivos, al hacer commit git crea una ID única asociada a estos la cual permite tener un historial de los cambios hechos junto con sus autores. [ https://help.github.com/articles/github-glossary/ ]
 9. **Clone**: es una copia de un repositorio y reside en nuestro computador en vez de un servidor web o también se refiere a el hecho de hacer esta copia. Esto permite realizar cambios offline y sincronizarlos con nuestra versión online. [ https://help.github.com/articles/github-glossary/ ]
 10. **Pull**: permite obtener los últimos cambios realizados de un repositorio haciendo fetch y merge. [ https://help.github.com/articles/github-glossary/ ]
 11. **Push**: permite enviar los cambios realizados localmente a un repositorio remoto para que otros puedan acceder a estos. [ https://help.github.com/articles/github-glossary/ ]
 12. **Fetch**: permite obtener los últimos cambios realizados de un repositorio online sin hacer merge lo que nos permite hacer comparaciones entre nuestro código. 
       [https://help.github.com/articles/github-glossary/]
 13. **Merge**: toma los cambios de una rama y los aplica en otra. [ https://help.github.com/articles/github-glossary/ ]
 14. **Status**: nos muestra los distintos estados de nuestros archivos en nuestra working copy y en nuestra staging area. Qué archivos están modificados y sin seguimiento y cuáles con seguimiento pero no confirmados aún. [ https://git-scm.com/book/es/v2/Appendix-C%3A-Comandos-de-Git-Seguimiento-B%C3%A1sico]
 15. **Log**: muestra el historial de commits. [https://git-scm.com/docs/git-log ]
 16. **Checkout**: permite navegar entre ramas creadas con git branch, esto actualiza los archivos del directorio de trabajo para coincidir con la versión de los archivos contenidos en la rama seleccionada.[ https://git-scm.com/docs/git-checkout ]
 17. **Rama / Branch**: Una rama es una versión paralela de un repositorio pero contenida dentro de este la cual nos permite realizar cambios sin alterar la rama principal, al terminar nuestras modificaciones es posible realizar un merge con la rama principal. [ https://help.github.com/articles/github-glossary/ ]
 18. **Etiqueta / Tag**: son utilizados para marcar puntos importantes en la linea temporal de nuestro repositorio, se puede marcar cualquier commit realizado en git  [ https://www.genbetadev.com/sistemas-de-control-de-versiones/tags-con-git ]


----------