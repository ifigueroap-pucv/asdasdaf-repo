Glosario:
-Control de verciones: 
   Se le llama así a la gestión de los diversos cambions o configuraciones que se le puede hacer  a un software, ya que este puede estar trabajando con varias tecnologías en conjunto.
Fuente: 
https://es.stackoverflow.com/questions/tagged/control-de-versiones

-Control de verciones distribuido:
   esta encargado de gestionar el control del desarrollo de un software, pero coh la diferencioa que los desarrolladores pueden trabajar de manera remota.
Fuente:
https://es.stackoverflow.com/questions/tagged/git

-los repositorios remotos y locales:
   los repositoros remotos son aquellas versiones del proyecto que se encuentran en internet o alguna parte de la red, sus funciones dependen de los permisos. Los repositorios locales son aquellos que se encuentran guardados en la maquina.
Fuente:
https://es.stackoverflow.com/questions/91014/repositorio-remoto-y-repositorio-local  

-Repository: 
   El repositorio almacena todo el proyecto, el cual puede vivir tanto en local como en remoto. Al tener todo el proyecto, el repositorio guarda un historial de versiones y, más importante, de la relación de cada versión con la anterior para que pueda hacerse el árbol de versiones con las diferentes ramas.

-Fork: 
   Si en algún momento quisieras contribuir al proyecto de otra persona, o si deseas utilizar el proyecto de alguien como el punto de partida para tu propio proyecto. Esto se conoce como “fork”.

-Clone: 
   Una vez que decidiste hacer un fork , hasta ese momento sólo existe en GitHub. Para poder trabajar en el proyecto, tendrás que clonar el repositorio elegido a tu máquina local.

Branch: 
   Un branch o rama es una bifurcación del proyecto que se está realizando para añadir una nueva funcionalidad o corregir un bug. Al menos debería haber una rama estable que fuera la que estuviera descargada en producción, esto es el producto en una versión X. Una buena práctica sería, por ejemplo hacer una rama Y nueva de la rama X que estuviera en producción, hacer la mejora, corregir que no haya errores y, cuando estuviera todo hecho, reintegrar la rama a la rama X. Al reintegrarla a la rama X (o master) tendría esta nueva funcionalidad.

-Master: 
   La rama master es aquella rama donde se almacena la última versión estable del proyecto que se está realizando. En teoría la rama master es la que está en producción en cada momento  (o casi) y debería estar libre de bugs. Así, si esta rama está en producción, sirve como referente para hacer nuevas funcionalidades y/o arreglar bugs de última hora.

-Commit: 
   El commit consiste, a diferencia de otros repositorios como Subversion, en subir cosas a tu versión local del repositorio. De esta manera se puede trabajar en la rama de forma local sin tener que modificar ninguna versión en remoto ni tener que tener la última versión remota, cosa muy útil en grandes desarrollos trabajados por varias personas.

-Push: 
   Consiste en enviar todo lo que has confirmado con un commit al repositorio remoto. Aquí sí que es donde se une tu trabajo con el de los demás.

-Checkout: 
   El checkout es la acción de descargarse una rama del repositorio GIT local (sí, GIT tiene su propio repositorio en local para poder ir haciendo commits) o remoto.

-Fetch: 
   La acción fetch actualiza el repositorio local bajándose los datos del repositorio remoto a tu repositorio local sin actualizarlo, es decir, se guarda una copia del repositorio remoto en tu local.

-Merge: 
   La acción de merge es la continuación natural del fetch. El merge permite unir la copia del repositorio remoto con tu repositorio local, mezclando los diferentes códigos.

-Pull: 
   Un pull consiste en la unión del fetch y del merge, esto es, recoge la información del repositorio remoto y luego mezcla tu trabajo en local con esta.

-status: 
   Señala los caminos que de los directorios que tienen diferencias entre el archivo índice y el commit HEAD actual

-log:
    Muestra las configuraciones que se han han realizado en los directorios. Muestra los registros de confirmacion.
 
Fuente:https://desdecerolinux.wordpress.com/2014/03/28/terminos-de-git/
