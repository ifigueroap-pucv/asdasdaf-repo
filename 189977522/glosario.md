﻿## Glosario de Definiciones
+ **a. Control de versiones (VC):** Se define como un sistema el cual registra todos los cambios realizados sobre uno o varios archivos a lo largo del tiempo, de tal modo en el que puedas recuperar versiones específicas más adelante. Existen varios tipos de control de versiones, entre ellos estan el sistema de control de versiones locales, sistema de control de versiones centralizados y el sistema de control de versiones distribuidos.

[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)


+ **b. Control de versiones distribuido (DVC):** Es un sistema que ofrece soluciones para los problemas como son el colaborar con desarrolladores entre otros sistemas o errores comunes que ocurren a menudo como olvidar en qué directorio nos encontramos, guardar accidentalmente en el archivo equivocado o sobreescribir archivos que no queriamos. Se compone de un único servidor que contiene todos los archivos versionados, y varios clientes que descargan los archivos desde ese lugar central.

[Fuente](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)


+ **c. Repositorio remoto y Repositorio local:** El repositorio local es una version de un proyecto que se encuentra alojolado en la internet o en algun punto de la red. Puede ser de "sólo escritura" o de "lectura/escritura" dependiendo de los permisos que te tengan. En cuanto al repositorio local es una version de un proyecto que se encuentra alojado en nuestro ordenador y en este tambien realizaremos cambios locales.

[Fuente](https://stackoverflow.com/questions/13072111/gits-local-repository-and-remote-repository-confusing-concepts)


+ **d. Copia de trabajo / Working Copy:** Son los archivos locales modificados que aún no hemos añadido para preparar el commit.

[Fuente](https://mydefinitionofdone.com/2017/04/12/git/)


+ **e. Área de Preparación / Staging Area:** Es un archivo, generalmente contenido en el directorio de Git, que almacena información sobre lo que pasará al próximo commit. 

[Fuente](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)


+ **f. Preparar Cambios / Stage Changes:** Es un archivo que está en el "área de preparación" pero aun no se han confirmado los cambios.

[Fuente](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)


+ **g. Confirmar cambios / Commit Changes:** El git commit identifica y confirma todos los cambios hechos en dicho ambiente de trabajo. Trabaja con el repositorio local

[Fuente](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)


+ **h. Commit:** Un commit almacena el contenido actual del índice en un nuevo commit junto con un mensaje de registro del usuario que describe los cambios. Basicamente identifica los cambios en el repositorio local.

[Fuente](https://git-scm.com/docs/git-commit)


+ **i. clone:** El clone basicamente lo que hace es obtener una copia de un repositorio Git existente descargando toda la información del repositorio y saca una copia de trabajo de la ultima version. 

[Fuente](https://git-scm.com/docs/git-clone)


+ **j. pull:** El pull es una petición que el propietario de un "fork de un repositorio" (se crea un nuevo repositorio en nuestra cuenta ya sea de Github o Bitbucket) hace al propietario del repositorio original para que este último incorpore los commits que están en el fork.

[Fuente](http://aprendegit.com/que-es-un-pull-request)


+ **k. push:** Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo o equipo remota. Si no se hace un push de los cambios, estos cambios no se veran afectado en el repositorio remoto.

[Fuente](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)


+ **l. fetch:** Un fetch recupera todos los datos del proyecto remoto que no tengamos todavía. Cabe destacar que sólo recupera la información y la pone en el repositorio local pero no la une automáticamente con nuestro trabajo ni modifica aquello en lo que estemos trabajando.

[Fuente](https://git-scm.com/docs/git-fetch)

+ **m. merge:** Un merge hace una fusión a tres bandas entre las dos últimas instantáneas de cada rama y el ancestro común a ambas, creando un nuevo commit con los cambios mezclados.

[Fuente](https://www.solucionex.com/blog/git-merge-o-git-rebase)


+ **n. status:** Un status muestra el estado en el que se encuentra el árbol de trabajo.

[Fuente](https://git-scm.com/docs/git-status)


+ **o. log:** Un log muestra una lista con el registros de todos los commit que se han hecho en un repositorio en especifico.

[Fuente](https://git-scm.com/docs/git-log)


+ **p. checkout:** Un checkout actualiza los archivos del árbol de trabajo para que coincidan con la versión del índice o del árbol especificado.

[Fuente](https://git-scm.com/docs/git-checkout)