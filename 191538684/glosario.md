a. Control de versiones (VC): El control de versiones es un sistema que registra los cambios realizados sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones espec�ficas m�s adelante. (Fuente: https://es.wikipedia.org/wiki/Control_de_versiones)

b. Control de versiones distribuido (DVC): En un control de sistemas distribuido, los clientes no s�lo descargan la �ltima instant�nea de los archivos: replican completamente el repositorio. As�, si un servidor muere, y estos sistemas estaban colaborando a trav�s de �l, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo. Cada vez que se descarga una instant�nea, en realidad se hace una copia de seguridad completa de todos los datos (Fuente: https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

c. Repositorio remoto y Repositorio local: Un repositorio remoto son las versiones de tu proyecto que se encuentran en internet o en alg�n lugar de la red. Se pueden tener varios, d�nde cada uno de los cuales puede ser s�lo de lectura o de lectura/escritura seg�n los permisos que tengas. Colaborar con otros implica gestionar estos repositorios remotos, y mandar (push) y recibir (pull) datos de ellos cuando necesites compartir cosas. Un repositorio local es la copia local del repositorio. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

d. Copia de trabajo / Working Copy: es el branch seleccionado por el para trabajar en el, el cual es llamado copia actual.

e. �rea de Preparaci�n / Staging Area: es un archivo, generalmente contenido por el directorio de git, que almacena informacion sobre lo que pasar� por el proximo commit.

f. Preparar Cambios / Stage Changes: Esto quiere decr que se sabe sobre el cambio pero no es permanente en el repositorio. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

g. Confirmar cambios / Commit Changes: Se guardan los cambios realizados. https://git-scm.com/docs/git-commit

h. Commit: Registrar cambios en el repositorio. https://git-scm.com/docs/git-commit

i. clone: Clonar un repositorio en un nuevo directorio. https://git-scm.com/docs/git-clone

j. pull: Obtener e integrar con otro repositorio. https://git-scm.com/docs/git-pull

k. push: Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo.  https://try.github.io/levels/1/challenges/11

l. fetch: Recupera todos los datos del proyecto remoto que no tengas todavia. Despues de esto se podr�n tener referencias a todas las ramas del repositorio remoto, que puedes unir o inspeccionar en cualquier momento. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos

m. merge: Sirve para fucionar una rama. https://git-scm.com/docs/git-merge

n. status: Sirve para comprobar el estado de tus archivos. https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio

o. log: Sirve para examinar la secuencia de commits (confirmaciones) que hemos realizado en la historia de un proyecto, para saber d�nde estamos y qu� estados hemos tenido a lo largo de la vida del repositorio. https://www.desarrolloweb.com/articulos/git-log.html

p. checkout: Sirve para saltar de una rama a otra con el comando  git checkout. https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar

q. Rama / Branch:  Git almacena un punto de control que conserva: un apuntador a la copia puntual de los contenidos preparados (staged), unos metadatos con el autor y el mensaje explicativo, y uno o varios apuntadores a las confirmaciones (commit) que sean padres directos de esta (un padre en los casos de confirmaci�n normal, y m�ltiples padres en los casos de estar confirmando una fusi�n (merge) de dos o mas ramas). https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar

r. Etiqueta / Tag: Sirven para marcar puntos donde se ha lanzado alguna version (v1.0 y asi sucesivamente) https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas