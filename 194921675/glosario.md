# Glosario de Ejemplo

1- Control de versiones (VC): 				El control de versiones es un sistema que registra los cambios realizados sobre
	un archivo o conjunto de archivos a lo largo del tiempo. permitiendo recuperar una version anterior.

2- Control de versiones distribuido (DVC): 	En un DVC todas las versiones del archivo estan en todos los clientes (desarrolladores) 
	del servidor. permitiendo la recuperacion de la BD en una posible caida del servidor. 

3- Repositorio remoto y Repositorio local:	Los repositorios remotos son versiones de tu proyecto que se encuentran alojados 
	en algun servidor en Internet. y los repositorios locales estan ubicados en la computadora local.

4- Copia de trabajo / Working Copy: 			La copia de trabajo se realiza con el comando git clone [url], el cual copia todo 
	el repositorio sacando la �ltima versi�n.

5- �rea de Preparaci�n / Staging Area:		El �rea de preparaci�n es un sencillo archivo, generalmente contenido en tu directorio de Git,
	que almacena informaci�n acerca de lo que va a ir en tu pr�xima confirmaci�n. A veces se le denomina �ndice.

6- Preparar Cambios / Stage Changes:			La preparacion se cambios se hace alejecutar el comondo git add.	

7- Confirmar cambios / Commit Changes:		Cualquier archivo que hayas creado o modificado, y sobre el que hayas ejecutado 
	git add desde su �ltima edici�n	se incluir� en esta confirmaci�n.

	
8- Commit:  		Si haces git commit, la versi�n del archivo que se incluir� en la confirmaci�n ser� la que fuese cuando ejecutaste 
	el comando git add, no la versi�n que est�s viendo ahora en tu directorio de trabajo.

9- clone:		Este comando se usa con el prop�sito de revisar repertorios.

10- pull: 		Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando,
	el comando que se usa es: git pull.
	
11- push:		Un simple push env�a los cambios que se han hecho en la rama principal de los repertorios remotos que est�n asociados 
	con el directorio que est� trabajando. 

12- fetch:		Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside 
	en el directorio local que est� trabajando.

13- merge:		Este comando se usa para fusionar una rama con otra rama activa: git merge <branch-name>.

14- status:		Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que 
	est�n por ser a�adidos o comprometidos.

15- log:			Ejecutar este comando muestra una lista de commits en una rama junto con todos los detalles.

16- checkout:	El comando checkout se puede usar para crear ramas o cambiar entre ellas. Por ejemplo, el siguiente
	comando crea una nueva y se cambia a ella:	command git checkout -b <banch-name>
	Para cambiar de una rama a otra solo usa:	git checkout <branch-name>

17- Rama / Branch:	Este comando se usa para listar, crear o borrar ramas. 
	Para listar todas las ramas se usa: 	git branch
	para borrar la rama:					git branch -d <branch-name>

18- Etiqueta / Tag:	Etiquetar se usa para marcar commits espec�ficos con asas simples.


linkografia:
	http://rogerdudler.github.io/git-guide/index.es.html
	https://git-scm.com/book/es/v1
	https://es.wikipedia.org/wiki/Git
	https://www.hostinger.es/tutoriales/comandos-de-git