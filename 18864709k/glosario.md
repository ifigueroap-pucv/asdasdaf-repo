# Glosario de Conceptos

a. **Control de versiones (VC)**              : Es un sistema el cual ayuda a tener un mejor control de los cambios que se efect�an ya sea en
				   		archivos o c�digos fuentes a lo largo del tiempo, con la ventaja de poder manejar de manera m�s f�cil
				   		las versiones anteriores y tener especificado los cambios que se le han efectuado al c�digo.
						(https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

b. **Control de versiones distribuido (DVC)** : Este control de versiones resuelve un problema que tiene el *control de versiones centralizado*
						ya que, a diferencia de este otro, se puede interactuar con otros clientes, entonces si se cae
						el sistema en el servidor, pueden seguir interactuando y colaborando entre ellos.
						Esto sucede ya que el repositorio se duplica en cada cliente haciendo una copia de seguridad en 
						cada cliente.(https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

c. **Repositorio Remoto y Repositorio Local** : Los repositorios remotos son versiones de tu proyecto que se encuentran en internet o en algun punto de red
						se puede tener varios puntos de lectura o lectura/escritura, por lo tanto esto colaborar implica tambien trabajar 
						en repositorios de remotos, mandar y recibir informacion.
						(https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos) 

d. **Copia de Trabajo** / *working copy*      :	Es una copia de el repositorio principal el cual se puede editar a tu gusto y trabajar sin arriesgar el proyecto principal.
						(https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)

e. **Area de Preparacion** / *Staging Area*   : Es un archivo, generalmente ubicado en el directorio Git que almacena informaci�n acerca de lo que ir� en la pr�xima versi�n, 
					       en el flujo de trabajo este se encuentra entre el directorio de trabajo y el repositorio.
					       (https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

f. **Preparar Cambios** / *Stay Change*       : Antes de confirmar, se preparan los cambios de un archivo antes de realizarlos.
						(https://git-scm.com/about/staging-area)

g. **Confirmar Cambios** / *Commit Changes*   : Es la confirmaci�n de cambios realizados anteriormente, para que sean enviados al repositorio.
						(https://git-scm.com/docs/git-commit)

h. *Commit* 				      :	Confirma los cambios hechos anteriormente, almacenandolos en el repositorio del proyecto principal.
						(https://git-scm.com/docs/git-commit)

i. *clone* 				      : Copia un repositorio en un nuevo directorio.
						(https://git-scm.com/docs/git-clone)

j. *pull* 				      :	Esta accion en git obtione archivos desde un repositorio remoto y los traslada al repositorio actual para actualizarlo.
						(https://git-scm.com/docs/git-pull)

k. *push* 				      :	Actualiza referencias remotas con respecto a las del repositorio local.
						(https://git-scm.com/docs/git-push)

l. *fetch* 				      :	Descargar archivos desde un repositorio.
						(https://git-scm.com/docs/git-fetch)

m. *merge* 				      :	Une 2 o m�s versiones de un archivo.
						(https://git-scm.com/docs/git-merge)

n. *status* 				      :	Muestra el estado del directorio actual.
					        (https://git-scm.com/docs/git-status)

o. *log* 				      :	Muestra el registro de cambios hechos anteriormente.
						(https://git-scm.com/docs/git-log)

p. *checkout*				      :	Permite cambiar de rama o restaurar una de una versi�n anterior en el repositorio.
					        (https://git-scm.com/docs/git-checkout)

q. **Rama** / *Branch* 			      : Directorios o sub-directorios del "working area" con referencia a los commits.
						(https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

r. **etiqueta** / *Tag* 		      : Identificaci�n  de las versiones.
						(https://git-scm.com/docs/git-tag)



